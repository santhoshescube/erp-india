﻿namespace MyBooksERP
{
    partial class FrmRptPendingDelivery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptPendingDelivery));
            this.expnlRptPdtGroup = new DevComponents.DotNetBar.ExpandablePanel();
            this.cboStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ALL = new DevComponents.Editors.ComboItem();
            this.Pending = new DevComponents.Editors.ComboItem();
            this.Completed = new DevComponents.Editors.ComboItem();
            this.lblStatus = new DevComponents.DotNetBar.LabelX();
            this.cboItemIssueNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblItemIssueNo = new DevComponents.DotNetBar.LabelX();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblToDate = new DevComponents.DotNetBar.LabelX();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.cboCustomer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCustomer = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.rvDelivery = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ErrRptDelivery = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmrDelivery = new System.Windows.Forms.Timer(this.components);
            this.expnlRptPdtGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrRptDelivery)).BeginInit();
            this.SuspendLayout();
            // 
            // expnlRptPdtGroup
            // 
            this.expnlRptPdtGroup.CanvasColor = System.Drawing.SystemColors.Control;
            this.expnlRptPdtGroup.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expnlRptPdtGroup.Controls.Add(this.cboStatus);
            this.expnlRptPdtGroup.Controls.Add(this.lblStatus);
            this.expnlRptPdtGroup.Controls.Add(this.cboItemIssueNo);
            this.expnlRptPdtGroup.Controls.Add(this.lblItemIssueNo);
            this.expnlRptPdtGroup.Controls.Add(this.btnShow);
            this.expnlRptPdtGroup.Controls.Add(this.lblToDate);
            this.expnlRptPdtGroup.Controls.Add(this.lblFromDate);
            this.expnlRptPdtGroup.Controls.Add(this.dtpFromDate);
            this.expnlRptPdtGroup.Controls.Add(this.dtpToDate);
            this.expnlRptPdtGroup.Controls.Add(this.cboCustomer);
            this.expnlRptPdtGroup.Controls.Add(this.lblCustomer);
            this.expnlRptPdtGroup.Controls.Add(this.cboCompany);
            this.expnlRptPdtGroup.Controls.Add(this.lblCompany);
            this.expnlRptPdtGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.expnlRptPdtGroup.Location = new System.Drawing.Point(0, 0);
            this.expnlRptPdtGroup.Name = "expnlRptPdtGroup";
            this.expnlRptPdtGroup.Size = new System.Drawing.Size(1042, 91);
            this.expnlRptPdtGroup.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlRptPdtGroup.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlRptPdtGroup.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlRptPdtGroup.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expnlRptPdtGroup.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expnlRptPdtGroup.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlRptPdtGroup.Style.GradientAngle = 90;
            this.expnlRptPdtGroup.TabIndex = 1;
            this.expnlRptPdtGroup.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlRptPdtGroup.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlRptPdtGroup.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlRptPdtGroup.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expnlRptPdtGroup.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlRptPdtGroup.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expnlRptPdtGroup.TitleStyle.GradientAngle = 90;
            this.expnlRptPdtGroup.TitleText = "Filter By";
            // 
            // cboStatus
            // 
            this.cboStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboStatus.DisplayMember = "Text";
            this.cboStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboStatus.DropDownHeight = 134;
            this.cboStatus.DropDownWidth = 184;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.IntegralHeight = false;
            this.cboStatus.ItemHeight = 14;
            this.cboStatus.Items.AddRange(new object[] {
            this.ALL,
            this.Pending,
            this.Completed});
            this.cboStatus.Location = new System.Drawing.Point(363, 57);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(184, 20);
            this.cboStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboStatus.TabIndex = 30;
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.cboStatus_SelectedIndexChanged);
            this.cboStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboStatus_KeyPress);
            this.cboStatus.SelectedValueChanged += new System.EventHandler(this.cboStatus_SelectedValueChanged);
            // 
            // ALL
            // 
            this.ALL.Text = "ALL";
            // 
            // Pending
            // 
            this.Pending.Text = "Pending";
            // 
            // Completed
            // 
            this.Completed.Text = "Completed";
            // 
            // lblStatus
            // 
            // 
            // 
            // 
            this.lblStatus.BackgroundStyle.Class = "";
            this.lblStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblStatus.Location = new System.Drawing.Point(287, 57);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(66, 20);
            this.lblStatus.TabIndex = 29;
            this.lblStatus.Text = "Status";
            // 
            // cboItemIssueNo
            // 
            this.cboItemIssueNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboItemIssueNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboItemIssueNo.DisplayMember = "Text";
            this.cboItemIssueNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboItemIssueNo.DropDownHeight = 134;
            this.cboItemIssueNo.DropDownWidth = 184;
            this.cboItemIssueNo.FormattingEnabled = true;
            this.cboItemIssueNo.IntegralHeight = false;
            this.cboItemIssueNo.ItemHeight = 14;
            this.cboItemIssueNo.Location = new System.Drawing.Point(363, 32);
            this.cboItemIssueNo.Name = "cboItemIssueNo";
            this.cboItemIssueNo.Size = new System.Drawing.Size(184, 20);
            this.cboItemIssueNo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboItemIssueNo.TabIndex = 28;
            this.cboItemIssueNo.SelectedIndexChanged += new System.EventHandler(this.cboItemIssueNo_SelectedIndexChanged);
            this.cboItemIssueNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboItemIssueNo_KeyPress);
            this.cboItemIssueNo.SelectedValueChanged += new System.EventHandler(this.cboItemIssueNo_SelectedValueChanged);
            // 
            // lblItemIssueNo
            // 
            // 
            // 
            // 
            this.lblItemIssueNo.BackgroundStyle.Class = "";
            this.lblItemIssueNo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblItemIssueNo.Location = new System.Drawing.Point(287, 32);
            this.lblItemIssueNo.Name = "lblItemIssueNo";
            this.lblItemIssueNo.Size = new System.Drawing.Size(66, 20);
            this.lblItemIssueNo.TabIndex = 27;
            this.lblItemIssueNo.Text = "ItemIssue No";
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(759, 58);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 20);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 26;
            this.btnShow.Text = "Show";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // lblToDate
            // 
            // 
            // 
            // 
            this.lblToDate.BackgroundStyle.Class = "";
            this.lblToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblToDate.Location = new System.Drawing.Point(567, 58);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(37, 19);
            this.lblToDate.TabIndex = 25;
            this.lblToDate.Text = "To";
            // 
            // lblFromDate
            // 
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(567, 32);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(37, 20);
            this.lblFromDate.TabIndex = 24;
            this.lblFromDate.Text = "From";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd MMM yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(609, 32);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.ShowCheckBox = true;
            this.dtpFromDate.Size = new System.Drawing.Size(121, 20);
            this.dtpFromDate.TabIndex = 23;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd MMM  yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(609, 58);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.ShowCheckBox = true;
            this.dtpToDate.Size = new System.Drawing.Size(121, 20);
            this.dtpToDate.TabIndex = 21;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
            // 
            // cboCustomer
            // 
            this.cboCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCustomer.DisplayMember = "Text";
            this.cboCustomer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCustomer.DropDownHeight = 134;
            this.cboCustomer.DropDownWidth = 184;
            this.cboCustomer.FormattingEnabled = true;
            this.cboCustomer.IntegralHeight = false;
            this.cboCustomer.ItemHeight = 14;
            this.cboCustomer.Location = new System.Drawing.Point(85, 58);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(184, 20);
            this.cboCustomer.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCustomer.TabIndex = 4;
            this.cboCustomer.SelectedIndexChanged += new System.EventHandler(this.cboCustomer_SelectedIndexChanged);
            this.cboCustomer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCustomer_KeyPress);
            this.cboCustomer.SelectedValueChanged += new System.EventHandler(this.cboCustomer_SelectedValueChanged);
            // 
            // lblCustomer
            // 
            // 
            // 
            // 
            this.lblCustomer.BackgroundStyle.Class = "";
            this.lblCustomer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCustomer.Location = new System.Drawing.Point(21, 58);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(48, 20);
            this.lblCustomer.TabIndex = 3;
            this.lblCustomer.Text = "Customer";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.DropDownWidth = 184;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(85, 32);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(184, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 2;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            this.cboCompany.SelectedValueChanged += new System.EventHandler(this.cboCompany_SelectedValueChanged);
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(21, 32);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(48, 20);
            this.lblCompany.TabIndex = 1;
            this.lblCompany.Text = "Company";
            // 
            // rvDelivery
            // 
            this.rvDelivery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rvDelivery.Location = new System.Drawing.Point(0, 91);
            this.rvDelivery.Name = "rvDelivery";
            this.rvDelivery.Size = new System.Drawing.Size(1042, 362);
            this.rvDelivery.TabIndex = 2;
            // 
            // ErrRptDelivery
            // 
            this.ErrRptDelivery.ContainerControl = this;
            // 
            // TmrDelivery
            // 
            this.TmrDelivery.Tick += new System.EventHandler(this.TmrDelivery_Tick);
            // 
            // FrmRptPendingDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 453);
            this.Controls.Add(this.rvDelivery);
            this.Controls.Add(this.expnlRptPdtGroup);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptPendingDelivery";
            this.Text = "FrmRptPendingDelivery";
            this.Load += new System.EventHandler(this.FrmRptPendingDelivery_Load);
            this.expnlRptPdtGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrRptDelivery)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expnlRptPdtGroup;
        private DevComponents.DotNetBar.ButtonX btnShow;
        private DevComponents.DotNetBar.LabelX lblToDate;
        private DevComponents.DotNetBar.LabelX lblFromDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCustomer;
        private DevComponents.DotNetBar.LabelX lblCustomer;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboStatus;
        private DevComponents.DotNetBar.LabelX lblStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboItemIssueNo;
        private DevComponents.DotNetBar.LabelX lblItemIssueNo;
        private DevComponents.Editors.ComboItem ALL;
        private DevComponents.Editors.ComboItem Pending;
        private DevComponents.Editors.ComboItem Completed;
        private Microsoft.Reporting.WinForms.ReportViewer rvDelivery;
        private System.Windows.Forms.ErrorProvider ErrRptDelivery;
        private System.Windows.Forms.Timer TmrDelivery;
    }
}