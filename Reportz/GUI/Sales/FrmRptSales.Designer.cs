﻿
namespace MyBooksERP
{
    partial class frmRptSales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRptSales));
            this.companyHeaderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.lblCustomer = new DevComponents.DotNetBar.LabelX();
            this.CboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.CboCustomer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.lblDateType = new DevComponents.DotNetBar.LabelX();
            this.CboDateType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboExecutive = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblExcecutive = new DevComponents.DotNetBar.LabelX();
            this.cboStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblStatus = new DevComponents.DotNetBar.LabelX();
            this.lblNo = new DevComponents.DotNetBar.LabelX();
            this.cboOrder = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblTo = new DevComponents.DotNetBar.LabelX();
            this.lblFrom = new DevComponents.DotNetBar.LabelX();
            this.DtpToDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.DtpFromDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.salesInvoiceExpenseDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.salesQuotationDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.salesOrderExpenseDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.salesInvoiceMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.salesOrderMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.salesQuotationMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.salesOrderDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.salesInvoicenDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ErpSalesReport = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.companyHeaderBindingSource)).BeginInit();
            this.expandablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesInvoiceExpenseDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesQuotationDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesOrderExpenseDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesInvoiceMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesOrderMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesQuotationMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesOrderDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesInvoicenDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErpSalesReport)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(797, 36);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 20);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 9;
            this.BtnShow.Text = "Show";
            // 
            // lblType
            // 
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(13, 87);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(53, 23);
            this.lblType.TabIndex = 2;
            this.lblType.Text = "Type";
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(13, 36);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(53, 23);
            this.lblCompany.TabIndex = 0;
            this.lblCompany.Text = "Company";
            // 
            // lblCustomer
            // 
            // 
            // 
            // 
            this.lblCustomer.BackgroundStyle.Class = "";
            this.lblCustomer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCustomer.Location = new System.Drawing.Point(289, 62);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(62, 23);
            this.lblCustomer.TabIndex = 1;
            this.lblCustomer.Text = "Customer";
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.DisplayMember = "Text";
            this.CboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCompany.DropDownHeight = 75;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.IntegralHeight = false;
            this.CboCompany.ItemHeight = 14;
            this.CboCompany.Location = new System.Drawing.Point(72, 39);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(200, 20);
            this.CboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCompany.TabIndex = 0;
            this.CboCompany.SelectionChangeCommitted += new System.EventHandler(this.CboCompany_SelectionChangeCommitted);
            this.CboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // CboType
            // 
            this.CboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboType.DisplayMember = "Text";
            this.CboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboType.DropDownHeight = 75;
            this.CboType.FormattingEnabled = true;
            this.CboType.IntegralHeight = false;
            this.CboType.ItemHeight = 14;
            this.CboType.Location = new System.Drawing.Point(72, 89);
            this.CboType.Name = "CboType";
            this.CboType.Size = new System.Drawing.Size(200, 20);
            this.CboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboType.TabIndex = 2;
            this.CboType.SelectionChangeCommitted += new System.EventHandler(this.CboType_SelectionChangeCommitted);
            this.CboType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // ReportViewer1
            // 
            this.ReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DatasetSales_STSalesReturnMaster";
            reportDataSource1.Value = null;
            reportDataSource2.Name = "DatasetSales_STSalesReturnDetails";
            reportDataSource2.Value = null;
            reportDataSource3.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource3.Value = null;
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource3);
            this.ReportViewer1.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptSalesReturn.rdlc";
            this.ReportViewer1.Location = new System.Drawing.Point(0, 126);
            this.ReportViewer1.Name = "ReportViewer1";
            this.ReportViewer1.Size = new System.Drawing.Size(1042, 327);
            this.ReportViewer1.TabIndex = 5;
            this.ReportViewer1.RenderingComplete += new Microsoft.Reporting.WinForms.RenderingCompleteEventHandler(this.ReportViewer1_RenderingComplete);
            this.ReportViewer1.RenderingBegin += new System.ComponentModel.CancelEventHandler(this.ReportViewer1_RenderingBegin);
            // 
            // CboCustomer
            // 
            this.CboCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCustomer.DisplayMember = "Text";
            this.CboCustomer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCustomer.DropDownHeight = 75;
            this.CboCustomer.FormattingEnabled = true;
            this.CboCustomer.IntegralHeight = false;
            this.CboCustomer.ItemHeight = 14;
            this.CboCustomer.Location = new System.Drawing.Point(358, 65);
            this.CboCustomer.Name = "CboCustomer";
            this.CboCustomer.Size = new System.Drawing.Size(199, 20);
            this.CboCustomer.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCustomer.TabIndex = 4;
            this.CboCustomer.SelectionChangeCommitted += new System.EventHandler(this.CboCustomer_SelectionChangeCommitted);
            this.CboCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.lblDateType);
            this.expandablePanel1.Controls.Add(this.CboDateType);
            this.expandablePanel1.Controls.Add(this.CboExecutive);
            this.expandablePanel1.Controls.Add(this.lblExcecutive);
            this.expandablePanel1.Controls.Add(this.cboStatus);
            this.expandablePanel1.Controls.Add(this.lblStatus);
            this.expandablePanel1.Controls.Add(this.lblNo);
            this.expandablePanel1.Controls.Add(this.cboOrder);
            this.expandablePanel1.Controls.Add(this.lblTo);
            this.expandablePanel1.Controls.Add(this.lblFrom);
            this.expandablePanel1.Controls.Add(this.DtpToDate);
            this.expandablePanel1.Controls.Add(this.DtpFromDate);
            this.expandablePanel1.Controls.Add(this.BtnShow);
            this.expandablePanel1.Controls.Add(this.lblType);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Controls.Add(this.lblCustomer);
            this.expandablePanel1.Controls.Add(this.CboType);
            this.expandablePanel1.Controls.Add(this.CboCompany);
            this.expandablePanel1.Controls.Add(this.CboCustomer);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(1042, 126);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 2;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            // 
            // lblDateType
            // 
            // 
            // 
            // 
            this.lblDateType.BackgroundStyle.Class = "";
            this.lblDateType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDateType.Location = new System.Drawing.Point(580, 36);
            this.lblDateType.Name = "lblDateType";
            this.lblDateType.Size = new System.Drawing.Size(63, 23);
            this.lblDateType.TabIndex = 13;
            this.lblDateType.Text = "Date Type";
            // 
            // CboDateType
            // 
            this.CboDateType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboDateType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDateType.DisplayMember = "Text";
            this.CboDateType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboDateType.DropDownHeight = 75;
            this.CboDateType.FormattingEnabled = true;
            this.CboDateType.IntegralHeight = false;
            this.CboDateType.ItemHeight = 14;
            this.CboDateType.Location = new System.Drawing.Point(649, 36);
            this.CboDateType.Name = "CboDateType";
            this.CboDateType.Size = new System.Drawing.Size(121, 20);
            this.CboDateType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboDateType.TabIndex = 6;
            this.CboDateType.SelectedIndexChanged += new System.EventHandler(this.CboDateType_SelectedIndexChanged);
            this.CboDateType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // CboExecutive
            // 
            this.CboExecutive.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboExecutive.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboExecutive.DisplayMember = "Text";
            this.CboExecutive.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboExecutive.DropDownHeight = 75;
            this.CboExecutive.FormattingEnabled = true;
            this.CboExecutive.IntegralHeight = false;
            this.CboExecutive.ItemHeight = 14;
            this.CboExecutive.Location = new System.Drawing.Point(72, 65);
            this.CboExecutive.Name = "CboExecutive";
            this.CboExecutive.Size = new System.Drawing.Size(200, 20);
            this.CboExecutive.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboExecutive.TabIndex = 1;
            this.CboExecutive.SelectionChangeCommitted += new System.EventHandler(this.CboExecutive_SelectionChangeCommitted);
            this.CboExecutive.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblExcecutive
            // 
            // 
            // 
            // 
            this.lblExcecutive.BackgroundStyle.Class = "";
            this.lblExcecutive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblExcecutive.Location = new System.Drawing.Point(13, 62);
            this.lblExcecutive.Name = "lblExcecutive";
            this.lblExcecutive.Size = new System.Drawing.Size(53, 23);
            this.lblExcecutive.TabIndex = 10;
            this.lblExcecutive.Text = "Employee";
            // 
            // cboStatus
            // 
            this.cboStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboStatus.DisplayMember = "Text";
            this.cboStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboStatus.DropDownHeight = 75;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.IntegralHeight = false;
            this.cboStatus.ItemHeight = 14;
            this.cboStatus.Location = new System.Drawing.Point(358, 39);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(200, 20);
            this.cboStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboStatus.TabIndex = 3;
            this.cboStatus.SelectionChangeCommitted += new System.EventHandler(this.cboStatus_SelectionChangeCommitted);
            this.cboStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblStatus
            // 
            // 
            // 
            // 
            this.lblStatus.BackgroundStyle.Class = "";
            this.lblStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblStatus.Location = new System.Drawing.Point(289, 36);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(53, 23);
            this.lblStatus.TabIndex = 3;
            this.lblStatus.Text = "Status";
            // 
            // lblNo
            // 
            // 
            // 
            // 
            this.lblNo.BackgroundStyle.Class = "";
            this.lblNo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblNo.Location = new System.Drawing.Point(289, 87);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(16, 23);
            this.lblNo.TabIndex = 4;
            this.lblNo.Text = "No";
            // 
            // cboOrder
            // 
            this.cboOrder.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOrder.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOrder.DisplayMember = "Text";
            this.cboOrder.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOrder.DropDownHeight = 75;
            this.cboOrder.FormattingEnabled = true;
            this.cboOrder.IntegralHeight = false;
            this.cboOrder.ItemHeight = 14;
            this.cboOrder.Location = new System.Drawing.Point(358, 91);
            this.cboOrder.Name = "cboOrder";
            this.cboOrder.Size = new System.Drawing.Size(199, 20);
            this.cboOrder.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboOrder.TabIndex = 5;
            this.cboOrder.SelectionChangeCommitted += new System.EventHandler(this.cboOrder_SelectionChangeCommitted);
            this.cboOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblTo
            // 
            // 
            // 
            // 
            this.lblTo.BackgroundStyle.Class = "";
            this.lblTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTo.Location = new System.Drawing.Point(580, 89);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 20);
            this.lblTo.TabIndex = 7;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            // 
            // 
            // 
            this.lblFrom.BackgroundStyle.Class = "";
            this.lblFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFrom.Location = new System.Drawing.Point(580, 62);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(27, 23);
            this.lblFrom.TabIndex = 6;
            this.lblFrom.Text = "From";
            // 
            // DtpToDate
            // 
            // 
            // 
            // 
            this.DtpToDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpToDate.ButtonDropDown.Visible = true;
            this.DtpToDate.CustomFormat = "dd MMM yyyy";
            this.DtpToDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpToDate.IsPopupCalendarOpen = false;
            this.DtpToDate.Location = new System.Drawing.Point(649, 91);
            this.DtpToDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpToDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpToDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpToDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpToDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.ShowCheckBox = true;
            this.DtpToDate.Size = new System.Drawing.Size(121, 20);
            this.DtpToDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpToDate.TabIndex = 8;
            this.DtpToDate.ValueChanged += new System.EventHandler(this.DtpToDate_ValueChanged);
            // 
            // DtpFromDate
            // 
            // 
            // 
            // 
            this.DtpFromDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpFromDate.ButtonDropDown.Visible = true;
            this.DtpFromDate.CustomFormat = "dd MMM yyyy";
            this.DtpFromDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpFromDate.IsPopupCalendarOpen = false;
            this.DtpFromDate.Location = new System.Drawing.Point(649, 65);
            this.DtpFromDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpFromDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpFromDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpFromDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpFromDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.ShowCheckBox = true;
            this.DtpFromDate.Size = new System.Drawing.Size(121, 20);
            this.DtpFromDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpFromDate.TabIndex = 7;
            this.DtpFromDate.ValueChanged += new System.EventHandler(this.DtpFromDate_ValueChanged);
            // 
            // Timer
            // 
            this.Timer.Interval = 1000;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // ErpSalesReport
            // 
            this.ErpSalesReport.ContainerControl = this;
            // 
            // frmRptSales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 453);
            this.Controls.Add(this.ReportViewer1);
            this.Controls.Add(this.expandablePanel1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRptSales";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sales ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRptSales_Load);
            ((System.ComponentModel.ISupportInitialize)(this.companyHeaderBindingSource)).EndInit();
            this.expandablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesInvoiceExpenseDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesQuotationDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesOrderExpenseDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesInvoiceMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesOrderMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesQuotationMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesOrderDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesInvoicenDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErpSalesReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.LabelX lblType;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.LabelX lblCustomer;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboType;
        private Microsoft.Reporting.WinForms.ReportViewer ReportViewer1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCustomer;
        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpFromDate;
        private DevComponents.DotNetBar.LabelX lblTo;
        private DevComponents.DotNetBar.LabelX lblFrom;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpToDate;
        private System.Windows.Forms.BindingSource salesQuotationMasterBindingSource;
        //private DatasetSales  datasetSales;

        private System.Windows.Forms.BindingSource salesOrderMasterBindingSource;
        private System.Windows.Forms.BindingSource salesInvoiceMasterBindingSource;
        private DevComponents.DotNetBar.LabelX lblNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboOrder;
        private DevComponents.DotNetBar.LabelX lblStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboStatus;
        private System.Windows.Forms.BindingSource salesQuotationDetailsBindingSource;
        private System.Windows.Forms.BindingSource salesInvoiceExpenseDetailsBindingSource;
        private System.Windows.Forms.BindingSource salesOrderExpenseDetailsBindingSource;
        private System.Windows.Forms.BindingSource salesOrderDetailsBindingSource;
        private System.Windows.Forms.BindingSource salesInvoicenDetailsBindingSource;
        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.BindingSource companyHeaderBindingSource;
        
        //private System.Windows.Forms.BindingSource STSalesReturnMasterBindingSource;
        //private System.Windows.Forms.BindingSource STSalesReturnDetailsBindingSource;
        private DevComponents.DotNetBar.LabelX lblExcecutive;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboExecutive;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboDateType;
        private DevComponents.DotNetBar.LabelX lblDateType;
        private System.Windows.Forms.ErrorProvider ErpSalesReport;
    }
}