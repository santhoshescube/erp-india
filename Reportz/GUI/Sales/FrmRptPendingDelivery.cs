﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Microsoft.Reporting.WinForms;

namespace MyBooksERP
{
    public partial class FrmRptPendingDelivery : Form
    {
        clsBLLRptPendingDelivery MobjclsBLLRptPendingDelivery;

        private string MsReportPath;

        private ArrayList MaMessageArr;   // Error Message display
        private ArrayList MaStatusMessage;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;

        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;

        public FrmRptPendingDelivery()
        {
            InitializeComponent();
            MobjclsBLLRptPendingDelivery = new clsBLLRptPendingDelivery();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification(); 
        }

        private void FrmRptPendingDelivery_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
            LoadMessage();
            cboStatus.SelectedIndex = 1;
        }

        private void LoadCombos(int intType)
        {
            DataTable datCombos = null;

            string strCondition = "";


            if (intType == 0)
            {
                datCombos = MobjclsBLLRptPendingDelivery.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID=" +ClsCommonSettings.LoginCompanyID});
                BindCombo(cboCompany, "CompanyName", "CompanyID", datCombos, false);

                datCombos = MobjclsBLLRptPendingDelivery.FillCombos(new string[] { "distinct V.VendorID,V.VendorName+'['+V.VendorCode+']' AS VendorName", "InvVendorInformation V INNER JOIN InvVendorCompanyDetails VC ON V.VendorID = VC.VendorID", "VendorTypeID = 1 AND VC.CompanyID="+ClsCommonSettings.LoginCompanyID });
                BindCombo(cboCustomer, "VendorName", "VendorID", datCombos, true);
            }
            if(intType == 1)
            {
                strCondition = strCondition + " OrderTypeID = "+(int)OperationOrderType.DNOTDirect+" AND CompanyID = " + cboCompany.SelectedValue.ToInt32(); 
                if (cboCustomer.SelectedValue.ToInt32() > 0)
                    strCondition = strCondition + " AND VendorID = " + cboCustomer.SelectedValue.ToInt32();

                datCombos = MobjclsBLLRptPendingDelivery.FillCombos(new string[] { "ItemIssueID,ItemIssueNo", "InvItemIssueMaster", strCondition});
                BindCombo(cboItemIssueNo, "ItemIssueNo", "ItemIssueID", datCombos, true);
            }
        }

        private void BindCombo(ComboBox cboCombo, string strTextField, string strValueField, DataTable datCombos, bool blnInsertRow)
        {
            if (blnInsertRow)
            {
                DataRow dr = datCombos.NewRow();
                dr[strValueField] = 0;
                dr[strTextField] = "ALL";
                datCombos.Rows.InsertAt(dr, 0);
            }
            cboCombo.DataSource = datCombos;
            cboCombo.DisplayMember = strTextField;
            cboCombo.ValueMember = strValueField;
        }

        /// <summary>
        /// Loads all message
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.PendingDeliveryReport, ClsCommonSettings.ProductID);
        }

        private void ClearReport()
        {
            ErrRptDelivery.Clear();
            rvDelivery.Clear();
            rvDelivery.Reset();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
            LoadCombos(1);
        }

        private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
            LoadCombos(1);
        }

        private void cboCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCustomer.DroppedDown = false;
        }

        private void cboItemIssueNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboItemIssueNo.DroppedDown = false;
        }

        private void cboItemIssueNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();

            if (cboItemIssueNo.SelectedValue.ToInt32() > 0)
            {
                cboStatus.SelectedIndex = 0;
                dtpFromDate.Checked = false;
                dtpToDate.Checked = false;

                cboStatus.Enabled = false;
                dtpFromDate.Enabled = false;
                dtpToDate.Enabled = false;
            }
            else
            {
                cboStatus.Enabled = true;
                dtpFromDate.Enabled = true;
                dtpToDate.Enabled = true;
            }
        }

        private void cboStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboStatus.DroppedDown = false;
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                btnShow.Enabled = false;
                if (ValidateReport())
                {
                    ShowReport();
                }
                TmrDelivery.Enabled = true;
            }
            catch
            {
                TmrDelivery.Enabled = true;
            }
        }

        private void ShowReport()
        {
            ClearReport();

            int intCompanyID = cboCompany.SelectedValue.ToInt32();
            int intVendorID = cboCustomer.SelectedValue.ToInt32();
            long lngItemIssueID = cboItemIssueNo.SelectedValue.ToInt32();
            int intStatusID = cboStatus.SelectedIndex;

            int intchkFrmTo = 0;

            DateTime dtFromDate = dtpFromDate.Value;
            DateTime dtToDate = dtpToDate.Value;

            string PsReportFooter = ClsCommonSettings.ReportFooter;


            if (dtpFromDate.Checked && dtpToDate.Checked)
            {
                intchkFrmTo = 1;
            }
            else if (dtpFromDate.Checked && !dtpToDate.Checked)
            {
                intchkFrmTo = 2;
            }
            else if (!dtpFromDate.Checked && dtpToDate.Checked)
            {
                intchkFrmTo = 3;
            }

            DataTable datCompanyHeader = MobjclsBLLRptPendingDelivery.GetCompanyHeader(intCompanyID);


            DataSet dsReport = MobjclsBLLRptPendingDelivery.GetReport(intCompanyID, intVendorID, lngItemIssueID, intStatusID, dtFromDate, dtToDate, intchkFrmTo);


            if (dsReport.Tables[0].Rows.Count == 0)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9783, out MmessageIcon);//No data found
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            }
            else
            {
                MsReportPath = Application.StartupPath + "\\MainReports\\RptPendingDelivery.rdlc";
                rvDelivery.LocalReport.ReportPath = MsReportPath;

                ReportParameter[] ReportParameter = new ReportParameter[7];
                ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                ReportParameter[1] = new ReportParameter("Company", Convert.ToString(cboCompany.Text.Trim()), false);
                ReportParameter[2] = new ReportParameter("Customer", Convert.ToString(cboCustomer.Text.Trim()), false);
                ReportParameter[3] = new ReportParameter("ItemIssueNo", Convert.ToString(cboItemIssueNo.Text.Trim()), false);
                ReportParameter[4] = new ReportParameter("Status", Convert.ToString(cboStatus.Text.Trim()), false);
                ReportParameter[5] = new ReportParameter("FromDate", dtpFromDate.Checked ? dtpFromDate.Value.ToString("dd MMM yyyy") : "", false);
                ReportParameter[6] = new ReportParameter("ToDate", dtpToDate.Checked ? dtpToDate.Value.ToString("dd MMM yyyy") : "", false);

                rvDelivery.LocalReport.SetParameters(ReportParameter);
                rvDelivery.LocalReport.DataSources.Clear();
                rvDelivery.LocalReport.DataSources.Add(new ReportDataSource("DtSetPendingDelivery_dtCompanyHeader", datCompanyHeader));
                rvDelivery.LocalReport.DataSources.Add(new ReportDataSource("DtSetPendingDelivery_dtDelivery", dsReport.Tables[0]));
                rvDelivery.LocalReport.DataSources.Add(new ReportDataSource("DtSetPendingDelivery_dtItemDetails", dsReport.Tables[1]));
                rvDelivery.SetDisplayMode(DisplayMode.PrintLayout);
                rvDelivery.ZoomMode = ZoomMode.Percent;
                this.Cursor = Cursors.Default;
            }
        }

        private bool ValidateReport()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                ShowErrorMessage(9778, cboCompany);
                return false;
            }
            else if (cboCustomer.SelectedIndex == -1)
            {
                ShowErrorMessage(9779, cboCustomer);
                return false;
            }
            else if (cboItemIssueNo.SelectedIndex == -1)
            {
                ShowErrorMessage(9780, cboItemIssueNo);
                return false;
            }
            else if (cboStatus.SelectedIndex == -1)
            {
                ShowErrorMessage(9781, cboStatus);
                return false;
            }
            else if (dtpFromDate.Checked && dtpToDate.Checked && (dtpFromDate.Value > dtpToDate.Value))
            {
                ShowErrorMessage(9782, dtpFromDate);
                return false;
            }
            return true;
        }

        /// <summary>
        /// shows error message
        /// </summary>
        /// <param name="num">error number</param>
        /// <param name="cntrl">control</param>
        private void ShowErrorMessage(int num, Control cntrl)
        {
            MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, num, out MmessageIcon);
            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            ErrRptDelivery.SetError(cntrl, MsMessageCommon.Replace("#", "").Trim());
            TmrDelivery.Enabled = true;
            cntrl.Focus();
        }

        private void TmrDelivery_Tick(object sender, EventArgs e)
        {
            TmrDelivery.Enabled = false;
            btnShow.Enabled = true;
        }

        private void cboCompany_SelectedValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboCustomer_SelectedValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboItemIssueNo_SelectedValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }


    }
}
