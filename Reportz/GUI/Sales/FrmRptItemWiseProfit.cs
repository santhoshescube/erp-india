﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;

namespace MyBooksERP
{
    public partial class FrmRptItemWiseProfit : Form
    {
        clsBLLRptItemwiseProfit MobjBLLRptItemwiseProfit;


        private ArrayList MaMessageArr;   // Error Message display
        private ArrayList MaStatusMessage;
        ClsNotification MObjClsNotification;
        private string MsMessageCaption = "MyBooksERP";
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;

        public FrmRptItemWiseProfit()
        {
            InitializeComponent();
            MobjBLLRptItemwiseProfit = new clsBLLRptItemwiseProfit();
            MObjClsNotification = new ClsNotification();
        }

        private void FrmRptItemWiseProfit_Load(object sender, EventArgs e)
        {
            LoadMessage();
            LoadCombos(0);
            DtpFromDate.Text = ClsCommonSettings.GetServerDate().ToString();
            DtpToDate.Text = ClsCommonSettings.GetServerDate().ToString();
            if (ClsCommonSettings.CompanyID > 0)
                CboCompany.SelectedValue = ClsCommonSettings.CompanyID;
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.ItemwiseProfitReport, (int)ClsCommonSettings.ProductID);
        }

        private void LoadCombos(int intType)
        {
            int intCompanyID = CboCompany.SelectedValue.ToInt32();
            int intCategoryID = cboCategory.SelectedValue.ToInt32();
            int intSubCategoryID = cboSubCategory.SelectedValue.ToInt32();

            DataSet dsCombo = MobjBLLRptItemwiseProfit.LoadCombos(intCompanyID, intCategoryID, intSubCategoryID);

            if (dsCombo != null && dsCombo.Tables.Count > 0)
            {
                if (intType == 0)
                {
                    if (dsCombo.Tables[0] != null && dsCombo.Tables[0].Rows.Count > 0)
                    {
                        CboCompany.DataSource = dsCombo.Tables[0];
                        CboCompany.DisplayMember = "CompanyName";
                        CboCompany.ValueMember = "CompanyID";
                    }
                    if (dsCombo.Tables[1] != null && dsCombo.Tables[1].Rows.Count > 0)
                    {
                        cboCategory.DataSource = dsCombo.Tables[1];
                        cboCategory.DisplayMember = "CategoryName";
                        cboCategory.ValueMember = "CategoryID";
                    }
                    if (dsCombo.Tables[4] != null && dsCombo.Tables[4].Rows.Count > 0)
                    {
                        cboType.DataSource = dsCombo.Tables[4];
                        cboType.DisplayMember = "OperationType";
                        cboType.ValueMember = "OperationTypeID";
                    }
                }

                if (intType == 1)
                {
                    if (dsCombo.Tables[3] != null && dsCombo.Tables[3].Rows.Count > 0)
                    {
                        cboItem.DataSource = dsCombo.Tables[3];
                        cboItem.DisplayMember = "ItemName";
                        cboItem.ValueMember = "ItemID";
                    }
                }
                if (intType == 2)
                {
                    if (dsCombo.Tables[2] != null && dsCombo.Tables[2].Rows.Count > 0)
                    {
                        cboSubCategory.DataSource = dsCombo.Tables[2];
                        cboSubCategory.DisplayMember = "SubCategoryName";
                        cboSubCategory.ValueMember = "SubCategoryID";
                    }
                }
            }
        }

        private void CboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            errReport.Clear();
            LoadCombos(1);
        }

        private void cboCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            errReport.Clear();
            LoadCombos(1);
            LoadCombos(2);
        }

        private void cboSubCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            errReport.Clear();
            LoadCombos(1);
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            if (ValidateReport())
            {
                ShowReport();
            }
        }

        private void ShowReport()
        {
            bool IsGroup = false;
            string[] str = Convert.ToString(cboItem.SelectedValue).Split('@');
            int ItemID = Convert.ToInt32(str[0]);
            if(ItemID > 0)
            IsGroup = Convert.ToBoolean(Convert.ToInt32(str[1]));
            int CompanyID = CboCompany.SelectedValue.ToInt32();
            int CategoryID = cboCategory.SelectedValue.ToInt32();
            int SubCategoryID = cboSubCategory.SelectedValue.ToInt32();
            int intTypeID = cboType.SelectedValue.ToInt32();
            string PsReportFooter = ClsCommonSettings.ReportFooter;
            DateTime dtFromDate = Convert.ToDateTime(DtpFromDate.Value);
            DateTime dtToDate = Convert.ToDateTime(DtpToDate.Value);
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            string MsReportPath = Application.StartupPath + "\\MainReports\\RptItemWiseProfit.rdlc";
            ReportViewer1.LocalReport.ReportPath = MsReportPath;
            DataSet dsReport = this.MobjBLLRptItemwiseProfit.GetReport(ItemID, CompanyID, CategoryID, SubCategoryID, intTypeID, dtFromDate, dtToDate, IsGroup);
            if (dsReport != null && dsReport.Tables != null)
            {
                if (dsReport.Tables[1] != null && dsReport.Tables[1].Rows.Count > 0)
                {
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportParameter[] ReportParameters = new ReportParameter[8];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportParameters[1] = new ReportParameter("Company", CboCompany.Text.Trim(), false);
                    ReportParameters[2] = new ReportParameter("Category", cboCategory.Text.Trim(), false);
                    ReportParameters[3] = new ReportParameter("SubCategory", cboSubCategory.Text.Trim(), false);
                    ReportParameters[4] = new ReportParameter("FromDate", Convert.ToDateTime(DtpFromDate.Text).ToString("MMM dd yyyy"), false);
                    ReportParameters[5] = new ReportParameter("ToDate", Convert.ToDateTime(DtpToDate.Text).ToString("MMM dd yyyy"), false);
                    ReportParameters[6] = new ReportParameter("Type", cboType.Text.Trim(), false);
                    ReportParameters[7] = new ReportParameter("Item", cboItem.Text.Trim(), false);

                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetItemWiseProfit_dtCompany", dsReport.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetItemWiseProfit_dtItemwiseProfit", dsReport.Tables[1]));
                    ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                    ReportViewer1.ZoomMode = ZoomMode.Percent;
                    this.Cursor = Cursors.Default;
                }
                else
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9773, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
            }
        }

        private bool ValidateReport()
        {

            if (CboCompany.DataSource == null || CboCompany.SelectedValue == null || CboCompany.SelectedIndex == -1)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9767, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errReport.SetError(CboCompany, MsMessageCommon.Replace("#", "").Trim());
                CboCompany.Focus();
                return false;
            }
            if (cboType.DataSource == null || cboType.SelectedValue == null || cboType.SelectedIndex == -1)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9768, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errReport.SetError(cboType, MsMessageCommon.Replace("#", "").Trim());
                cboType.Focus();
                return false;
            }
            if (cboCategory.DataSource == null || cboCategory.SelectedValue == null || cboCategory.SelectedIndex == -1)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9769, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errReport.SetError(cboCategory, MsMessageCommon.Replace("#", "").Trim());
                cboCategory.Focus();
                return false;
            }
            if (cboSubCategory.DataSource == null || cboSubCategory.SelectedValue == null || cboSubCategory.SelectedIndex == -1)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9770, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errReport.SetError(cboSubCategory, MsMessageCommon.Replace("#", "").Trim());
                cboSubCategory.Focus();
                return false;
            }
            if (cboItem.DataSource == null || cboItem.SelectedValue == null || cboItem.SelectedIndex == -1)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9771, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errReport.SetError(cboItem, MsMessageCommon.Replace("#", "").Trim());
                cboItem.Focus();
                return false;
            }
            if (DtpFromDate.Value.Date > DtpToDate.Value.Date)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9772, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errReport.SetError(DtpFromDate, MsMessageCommon.Replace("#", "").Trim());
                DtpFromDate.Focus();
                return false;
            }
            return true;
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            errReport.Clear();
        }

        private void cboItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            errReport.Clear();
        }

        private void DtpFromDate_TextChanged(object sender, EventArgs e)
        {
            errReport.Clear();
        }

        private void DtpToDate_TextChanged(object sender, EventArgs e)
        {
            errReport.Clear();
        }

        private void CboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboCompany.DroppedDown = false;
        }

        private void cboType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboType.DroppedDown = false;
        }

        private void cboCategory_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCategory.DroppedDown = false;
        }

        private void cboSubCategory_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboSubCategory.DroppedDown = false;
        }

        private void cboItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboItem.DroppedDown = false;
        }
    }
}
