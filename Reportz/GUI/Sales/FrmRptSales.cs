﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;
using System.Data.SqlClient;

using System.IO;
using System.Drawing.Imaging;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,2 May 2011>
Description:	<Description,, Sales Report Form>
================================================
*/

namespace MyBooksERP 
{
    public partial class frmRptSales : DevComponents.DotNetBar.Office2007Form 
    {
        #region  Variable Declaration

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        private string MsReportPath;
        string PsReportFooter;
        private string MsMessageCaption ="MyBooksERP";
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private bool MbViewPermission = true;
        private bool MblnPrintEmailPermission = false;//To Set View Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private int chk;
        private int chkFrom;
        private int chkTo;
        private string strFrom;
        private string strTo;
        private string strCondition = "";
        private string customerCondition = "";
        private int Company;
        private DataTable DTCompany;
        private int SalesOrderid;

        clsBLLSales MobjclsBLLSales;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;

        #endregion //Variables Declaration

        #region Constructor
        public frmRptSales()
        {
            InitializeComponent();
            PsReportFooter = ClsCommonSettings.ReportFooter;
            MobjclsBLLSales = new clsBLLSales();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();           
            BtnShow.Click += new EventHandler(BtnShow_Click);

        }
        #endregion //constructor

        private void frmRptSales_Load(object sender, EventArgs e)
        {
            LoadMessage();
            LoadCombos(0);
         //   SetPermissions();
            DtpFromDate.Value = ClsCommonSettings.GetServerDate();
            DtpToDate.Value = ClsCommonSettings.GetServerDate();
            if (Convert.ToInt32(CboCompany.SelectedValue) == 0 && Convert.ToInt32(CboType.SelectedValue) == 0)
            {
                lblStatus.Enabled = false;
                cboStatus.Enabled = false;
                lblNo.Enabled = false;
                cboOrder.Enabled = false;
                lblCustomer.Enabled = false;
                CboCustomer.Enabled = false;
            }

        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.SalesInvoice, (int)ClsCommonSettings.ProductID);
          //  MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.SalesInvoice, 4);
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettingsReports objClsBLLPermissionSettings = new clsBLLPermissionSettingsReports();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.SalesReports, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            MbViewPermission = (MblnAddPermission || MblnPrintEmailPermission || MblnUpdatePermission || MblnDeletePermission);
            expandablePanel1.Enabled = MbViewPermission;
        }

        private bool LoadCombos(int intType)
        {          
            try
            {
                DataTable datCombos = new DataTable();
                DataRow datrow;
                clsBLLPermissionSettingsReports objClsBLLPermissionSettings = new clsBLLPermissionSettingsReports();
                string strPermittedCompany;
                DataTable DTPermComp = new DataTable();

                if (intType == 0 || intType == 1)
                {
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                        datCombos = MobjclsBLLSales.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", " CompanyID="+ClsCommonSettings.LoginCompanyID });
                       
                        CboCompany.ValueMember = "CompanyID";
                        CboCompany.DisplayMember = "CompanyName";
                        CboCompany.DataSource = datCombos;
                        CboCompany.SelectedValue = ClsCommonSettings.LoginCompanyID;
                        CboCompany_SelectionChangeCommitted(null, null);
                    //}
                    //else
                    //{
                    //    datCombos = MobjclsBLLSales.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptSales);
                    //    if (datCombos.Rows.Count <= 0)
                    //        CboCompany.DataSource = null;
                    //    else
                    //    {
                    //        datrow = datCombos.NewRow();
                    //        datrow["CompanyID"] = 0;
                    //        datrow["CompanyName"] = "ALL";
                    //        datCombos.Rows.InsertAt(datrow, 0);
                    //        CboCompany.ValueMember = "CompanyID";
                    //        CboCompany.DisplayMember = "CompanyName";
                    //        CboCompany.DataSource = datCombos;
                    //    }
                    //}
                }
                if (intType == 2)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "VendorID,VendorName", "InvVendorInformation", "VendorTypeID="+(int)VendorType.Customer+"" });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboCustomer.ValueMember = "VendorID";
                    CboCustomer.DisplayMember = "VendorName";
                    CboCustomer.DataSource = datCombos;
                }
                if (CboCompany.DataSource != null)
                {
                    if (intType == 0 || intType == 3)
                    {
                        datCombos = MobjclsBLLSales.FillCombos(new string[] { "OperationTypeID,OperationType", "OperationTypeReference", "ModuleID =2 and OperationTypeID in ("+ (int) OperationType.SalesQuotation +","+ (int) OperationType.SalesOrder +","+ (int) OperationType.SalesInvoice +","+ (int) OperationType.CreditNote + ")"});
                        datrow = datCombos.NewRow();
                        datrow["OperationTypeID"] = 0;
                        datrow["OperationType"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        CboType.ValueMember = "OperationTypeID";
                        CboType.DisplayMember = "OperationType";
                        CboType.DataSource = datCombos;
                    }
                }
                //if (intType == 0 || intType == 4)
                //{
                //    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SaleTypeID,Description", "STSaleTypeReference", "Type =1" });
                //    datrow = datCombos.NewRow();
                //    datrow["SaleTypeID"] = 0;
                //    datrow["Description"] = "ALL";
                //    datCombos.Rows.InsertAt(datrow, 0);                    
                //    CboSalesOrderType.ValueMember = "SaleTypeID";
                //    CboSalesOrderType.DisplayMember = "Description";
                //    CboSalesOrderType.DataSource = datCombos;
                //}
                if (intType == 5)
                {
                  
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                       // strCondition = "";                        
                    }
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesQuotationID,SalesQuotationNo", "InvSalesQuotationMaster", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesQuotationID"] = 0;
                    datrow["SalesQuotationNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesQuotationID";
                    cboOrder.DisplayMember = "SalesQuotationNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 6)
                {
                   
                    if (Convert.ToInt32(CboExecutive.SelectedValue) == 0)
                    {
                        strCondition = "";
                    }
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesOrderID,SalesOrderNo", "InvSalesOrderMaster", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesOrderID"] = 0;
                    datrow["SalesOrderNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesOrderID";
                    cboOrder.DisplayMember = "SalesOrderNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 7)
                {
                  
                    if (Convert.ToInt32(CboExecutive.SelectedValue) == 0)
                    {
                        strCondition = "";
                    }
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesInvoiceID,SalesInvoiceNo", "InvSalesInvoiceMaster SIM Inner Join UserMaster UM on SIM.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesInvoiceID"] = 0;
                    datrow["SalesInvoiceNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesInvoiceID";
                    cboOrder.DisplayMember = "SalesInvoiceNo";
                    cboOrder.DataSource = datCombos;
                }
               
                if (intType == 8)

                {

                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "StatusID,Status", "CommonStatusReference", "OperationTypeID=" + (int)OperationType.SalesQuotation + "and StatusID not in (" + (int)OperationStatusType.SQuotationDenied + "," + (int)OperationStatusType.SQuotationSuggested + ")" });

                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.DataSource = null;
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }
                if (intType == 9)
                {


                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "StatusID,Status", " CommonStatusReference", "OperationTypeID =" + (int)OperationType.SalesOrder + " and StatusID not in (" + (int)OperationStatusType.SOrderDenied + "," + (int)OperationStatusType.SOrderSuggested + ")" });
                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.DataSource = null;
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }
                if (intType == 10)
                {
                    //datCombos = MobjclsBLLSales.FillCombos(new string[] { "StatusID,Description", "STCommonStatusReference", "StatusTypeID=" + (int)StatusType.POS + " and StatusID in (" + (int)OperationStatusType.SPOSDeliverd + "," + (int)OperationStatusType.SPOSCancelled + ")" });
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "StatusID,Status", " CommonStatusReference", "OperationTypeID =" + (int)OperationType.SalesInvoice + " and StatusID in (" + (int)OperationStatusType.SInvoiceOpen + "," + (int)OperationStatusType.SInvoiceCancelled + "," + (int)OperationStatusType.SDelivered + ")" });
                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.DataSource = null;
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }
                if (intType == 11)
                {
                                                                                                                            
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesQuotationID,SalesQuotationNo", "InvSalesQuotationMaster SQM Inner Join UserMaster UM on SQM.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesQuotationID"] = 0;
                    datrow["SalesQuotationNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesQuotationID";
                    cboOrder.DisplayMember = "SalesQuotationNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 12)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesOrderID,SalesOrderNo", "InvSalesOrderMaster SOM Inner Join UserMaster UM on SOM.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesOrderID"] = 0;
                    datrow["SalesOrderNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesOrderID";
                    cboOrder.DisplayMember = "SalesOrderNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 13)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesInvoiceID,SalesInvoiceNo", "InvSalesInvoiceMaster SIM Inner Join UserMaster UM on SIM.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesInvoiceID"] = 0;
                    datrow["SalesInvoiceNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesInvoiceID";
                    cboOrder.DisplayMember = "SalesInvoiceNo";
                    cboOrder.DataSource = datCombos;
                }

                if (intType == 14)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "StatusID,Status", " CommonStatusReference", "OperationTypeID =" + (int)OperationType.CreditNote + "" });
                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.DataSource = null;
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }

                if (intType ==15)
                {
                 
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        strCondition = "";
                    }
                    //SalesReturnID,SalesReturnNo,STSalesReturnMaster
                   
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesReturnID,SalesReturnNo", "InvSalesReturnMaster SRM Inner Join UserMaster UM on SRM.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesReturnID"] = 0;
                    datrow["SalesReturnNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesReturnID";
                    cboOrder.DisplayMember = "SalesReturnNo";
                    cboOrder.DataSource = datCombos;
                }
              
                if (intType == 16)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesReturnID,SalesReturnNo", "InvSalesReturnMaster SRM Inner Join UserMaster UM on SRM.CreatedBy=UM.UserID Inner Join InvSalesInvoiceMaster SIM on SIM.SalesInvoiceID=SRM.SalesInvoiceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesReturnID"] = 0;
                    datrow["SalesReturnNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesReturnID";
                    cboOrder.DisplayMember = "SalesReturnNo";
                    cboOrder.DataSource = datCombos;
                }
                //salesquotation 
                if (intType == 17)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "distinct  VI.VendorID,VI.VendorName", "InvVendorInformation VI left join InvSalesQuotationMaster SQ on VI.VendorID=SQ.VendorID inner join UserMaster UM on SQ.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboCustomer.DataSource = null;
                    CboCustomer.ValueMember = "VendorID";
                    CboCustomer.DisplayMember = "VendorName";
                    CboCustomer.DataSource = datCombos;
                }

                if (intType == 18)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "distinct  VI.VendorID,VI.VendorName", "InvVendorInformation VI left join InvSalesOrderMaster SO on VI.VendorID=SO.VendorID inner join UserMaster UM on SO.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboCustomer.DataSource = null;
                    CboCustomer.ValueMember = "VendorID";
                    CboCustomer.DisplayMember = "VendorName";
                    CboCustomer.DataSource = datCombos;
                }
                if (intType == 19)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "distinct  VI.VendorID,VI.VendorName", "InvVendorInformation VI left join InvSalesInvoiceMaster SI on VI.VendorID=SI.VendorID Inner Join UserMaster UM on SI.CreatedBy=UM.UserID ", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboCustomer.DataSource = null;
                    CboCustomer.ValueMember = "VendorID";
                    CboCustomer.DisplayMember = "VendorName";
                    CboCustomer.DataSource = datCombos;
                }
                if (intType == 20)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "distinct  VI.VendorID,VI.VendorName", "InvVendorInformation VI left join InvSalesInvoiceMaster SI on VI.VendorID=SI.VendorID inner join InvSalesReturnMaster SR on SI.SalesInvoiceID=SR.SalesInvoiceID Inner Join UserMaster UM on SR.CreatedBy=UM.UserID ", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboCustomer.DataSource = null;
                    CboCustomer.ValueMember = "VendorID";
                    CboCustomer.DisplayMember = "VendorName";
                    CboCustomer.DataSource = datCombos;
                }
                if (CboCompany.DataSource != null)
                {
                    if (intType == 0 || intType == 21)
                    {
                        strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue);

                        if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                        {
                        //    if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                        //    {
                                strPermittedCompany = null;
                                strCondition = "";
                            //}
                            //else
                            //{
                            //    DTPermComp = objClsBLLPermissionSettings.GetPermittedCompany(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptSales);
                            //    if (DTPermComp.Rows.Count <= 0)
                            //    {
                            //        strPermittedCompany = null;
                            //        strCondition = "";
                            //    }
                            //    else
                            //    {
                            //        strPermittedCompany = DTPermComp.Rows[0]["IsEnabled"].ToString();
                            //        if (strPermittedCompany != null) strCondition = " CompanyID in (" + strPermittedCompany + ")";
                            //    }
                            //}
                        }
                        datCombos = MobjclsBLLSales.FillCombos(new string[] { "EmployeeID,FirstName", "EmployeeMaster", strCondition });
                        datrow = datCombos.NewRow();
                        datrow["EmployeeID"] = 0;
                        datrow["FirstName"] = "ALL";
                        datCombos.Rows.InsertAt(datrow, 0);
                        CboExecutive.DataSource = null;
                        CboExecutive.ValueMember = "EmployeeID";
                        CboExecutive.DisplayMember = "FirstName";
                        CboExecutive.DataSource = datCombos;
                    }
                }
                if (intType == 22)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesReturnID,SalesReturnNo", "InvSalesReturnMaster SRM Inner Join UserMaster UM on SRM.CreatedBy=UM.UserID Inner Join InvSalesInvoiceMaster SIM on SIM.SalesInvoiceID=SRM.SalesInvoiceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesReturnID"] = 0;
                    datrow["SalesReturnNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesReturnID";
                    cboOrder.DisplayMember = "SalesReturnNo";
                    cboOrder.DataSource = datCombos;
                }
                if (intType == 0 || intType == 23)
                {
                    CboDateType.Items.Clear();
                    CboDateType.Items.Add("ALL");
                    CboDateType.Items.Add("Date");
                    CboDateType.Items.Add("Created Date");
                    CboDateType.Items.Add("Due Date");
                    CboDateType.SelectedIndex = 0;
                }
                if (intType == 24)
                {
                    CboDateType.Items.Clear();
                    CboDateType.Items.Add("ALL");
                    CboDateType.Items.Add("Date");
                    CboDateType.Items.Add("Create Date");
                    CboDateType.SelectedIndex = 0;
                }
                if (intType == 25)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesQuotationID,SalesQuotationNo", "InvSalesQuotationMaster SQM Inner join UserMaster UM on SQM.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesQuotationID"] = 0;
                    datrow["SalesQuotationNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesQuotationID";
                    cboOrder.DisplayMember = "SalesQuotationNo";
                    cboOrder.DataSource = datCombos;
                }

                if (intType == 26)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesOrderID,SalesOrderNo", "InvSalesOrderMaster SIM Inner join UserMaster UM on SIM.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesOrderID"] = 0;
                    datrow["SalesOrderNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesOrderID";
                    cboOrder.DisplayMember = "SalesOrderNo";
                    cboOrder.DataSource = datCombos;
                }

                if (intType == 27)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesInvoiceID,SalesInvoiceNo", "InvSalesInvoiceMaster SIM Inner join UserMaster UM on SIM.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesInvoiceID"] = 0;
                    datrow["SalesInvoiceNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesInvoiceID";
                    cboOrder.DisplayMember = "SalesInvoiceNo";
                    cboOrder.DataSource = datCombos;

                }

                if (intType == 28)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "SalesReturnID,SalesReturnNo", "InvSalesReturnMaster SRM Inner Join UserMaster UM on SRM.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesReturnID"] = 0;
                    datrow["SalesReturnNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "SalesReturnID";
                    cboOrder.DisplayMember = "SalesReturnNo";
                    cboOrder.DataSource = datCombos;
                }

                

                if (intType == 29)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "StatusID,Status", " CommonStatusReference", "OperationTypeID=" + (int)OperationType.POS + " and StatusID in (" + (int)OperationStatusType.SPOSDeliverd + "," + (int)OperationStatusType.SPOSCancelled + ")" });

                    datrow = datCombos.NewRow();
                    datrow["StatusID"] = 0;
                    datrow["Status"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboStatus.DataSource = null;
                    cboStatus.ValueMember = "StatusID";
                    cboStatus.DisplayMember = "Status";
                    cboStatus.DataSource = datCombos;
                }

                if (intType == 30)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "distinct  VI.VendorID,VI.VendorName", "InvVendorInformation VI left join InvPOSMaster POS on VI.VendorID=POS.VendorID inner join UserMaster UM on POS.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);

                    if(MobjclsBLLSales.GenearlCustomerCheck())
                    {
                        datrow = datCombos.NewRow();
                        datrow["VendorID"] = -1;
                        datrow["VendorName"] = "General Customer";
                        datCombos.Rows.InsertAt(datrow, 1);       

                    }

                    CboCustomer.DataSource = null;
                    CboCustomer.ValueMember = "VendorID";
                    CboCustomer.DisplayMember = "VendorName";
                    CboCustomer.DataSource = datCombos;
                }

                if (intType == 31)
                {
                    datCombos = MobjclsBLLSales.FillCombos(new string[] { "POSID,POSNo", "InvPOSMaster POS Inner join UserMaster UM on POS.CreatedBy=UM.UserID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["POSID"] = 0;
                    datrow["POSNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    cboOrder.DataSource = null;
                    cboOrder.ValueMember = "POSID";
                    cboOrder.DisplayMember = "POSNo";
                    cboOrder.DataSource = datCombos;
                }

                if (intType == 32)
                {
                    CboDateType.Items.Clear();
                    CboDateType.Items.Add("ALL");
                    CboDateType.Items.Add("Date");
                    CboDateType.Items.Add("Create Date");
                    CboDateType.SelectedIndex = 0;
                }


                MObjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void AllReport()
        {
            MsReportPath = Application.StartupPath + "\\MainReports\\RptMISSales.rdlc";
            ReportViewer1.LocalReport.ReportPath = MsReportPath;
            ReportParameter[] ReportParameter = new ReportParameter[12];
            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);         
            ReportParameter[1] = new ReportParameter("Company", Convert.ToString(CboCompany.Text.Trim()), false);          
            ReportParameter[2] = new ReportParameter("Customer", Convert.ToString(CboCustomer.Text.Trim()), false);
            ReportParameter[3] = new ReportParameter("Type", Convert.ToString(CboType.Text.Trim()), false);

            if (chkFrom == 1 & chkTo == 1)
            {
                ReportParameter[4] = new ReportParameter("FromDate", strFrom, false);
                ReportParameter[5] = new ReportParameter("ToDate", strTo, false);
            }
            else if (chkFrom == 1)
            {
                
                string strTo1=""; 

                strTo1 = "ALL";
                ReportParameter[4] = new ReportParameter("FromDate", strFrom, false);
                ReportParameter[5] = new ReportParameter("ToDate", strTo1, false);
            }
            else if (chkTo == 1)

            {
                string strFrom1 = ""; 
                strFrom1 = "ALL";
                ReportParameter[4] = new ReportParameter("FromDate", strFrom1, false);
                ReportParameter[5] = new ReportParameter("ToDate", strTo, false);
            }
            
            else
            {
                strFrom = "ALL";
                strTo = "ALL";
                ReportParameter[4] = new ReportParameter("FromDate", strFrom, false);
                ReportParameter[5] = new ReportParameter("ToDate", strTo, false);
            }
            


            ReportParameter[6] = new ReportParameter("checkbox", Convert.ToString(chk), false);
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesOrder)
            {
                ReportParameter[7] = new ReportParameter("SalesorderType", "Blank", false);
            }
            else
            {
                string sales = string.Empty;
                ReportParameter[7] = new ReportParameter("SalesorderType", "Blank", false);
            }
            ReportParameter[8] = new ReportParameter("Status", Convert.ToString(cboStatus.Text.Trim()), false);
            ReportParameter[9] = new ReportParameter("No", Convert.ToString(cboOrder.Text.Trim()), false);
            ReportParameter[10] = new ReportParameter("Executive", Convert.ToString(CboExecutive.Text.Trim()), false);

            if (chkFrom == 1 || chkTo == 1)
            {
                ReportParameter[11] = new ReportParameter("DateType", Convert.ToString(CboDateType.Text.Trim()), false);
            }
            else
            {
                string strDateType1 = "ALL";
                ReportParameter[11] = new ReportParameter("DateType", strDateType1, false);
            }

            ReportViewer1.LocalReport.SetParameters(ReportParameter);
            ReportViewer1.LocalReport.DataSources.Clear();
            DTCompany = new DataTable();
            if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
            {
                DTCompany = MobjclsBLLSales.DisplayALLCompanyHeaderReport();
            }
            else
            {
                DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(Company);
            }
            ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);

            //ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);

        }
        public void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
        }
      
        private void ShowReport()
        {
            Company = Convert.ToInt32(CboCompany.SelectedValue);
            int Customer = Convert.ToInt32(CboCustomer.SelectedValue);
            int Type = Convert.ToInt32(CboType.SelectedValue);
            strFrom = Microsoft.VisualBasic.Strings.Format(DtpFromDate.Value.Date, "dd MMM yyyy");
            strTo = Microsoft.VisualBasic.Strings.Format(DtpToDate.Value.Date, "dd MMM yyyy");
           // int intSaleType = Convert.ToInt32(CboSalesOrderType.SelectedValue);
            int intstatus = Convert.ToInt32(cboStatus.SelectedValue);
            int intNo = Convert.ToInt32(cboOrder.SelectedValue);
            int intExecutive = Convert.ToInt32(CboExecutive.SelectedValue);
            int intDateType = CboDateType.SelectedIndex;
            //if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesInvoice && Convert.ToInt32(cboOrder.SelectedValue) > 0)
            //{
            //    DataTable SalesOrder = MobjclsBLLSales.DisplayExpenseOrderId(intNo);
            //    SalesOrderid = Convert.ToInt32(SalesOrder.Rows[0]["SalesOrderID"]);
            //}
            if (DtpFromDate.LockUpdateChecked == true)
            {
                chkFrom = 1;
            }
            else
            {
                chkFrom = 0;
            }

            if (DtpToDate.LockUpdateChecked == true)
            {
                chkTo = 1;
            }
            else
            {
                chkTo = 0;
            }
            //clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            string strPermittedCompany;
            //DataTable DTPermComp = new DataTable();

            //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            //{
            strPermittedCompany = null;
            //}
            //else
            //{
            //    DTPermComp = objClsBLLPermissionSettings.GetPermittedCompany(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptSales);
            //    if (DTPermComp.Rows.Count <= 0)
            //        strPermittedCompany = null;
            //    else
            //        strPermittedCompany = DTPermComp.Rows[0]["IsEnabled"].ToString();
            //}
            if (Type == 7)
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptSalesQuotation.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }
            if (Type == 8)
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptSalesOrder.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }
            if (Type == 9)
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptSaleInvoice.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }

            if (Type == 11)
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptSalesReturn.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }

            if (Type == 10) //POS
            {
                if (intNo > 0)
                {
                    MsReportPath = Application.StartupPath + "\\MainReports\\RptPOS.rdlc";
                    ReportViewer1.LocalReport.ReportPath = MsReportPath;
                    ReportParameter[] ReportParameters = new ReportParameter[1];
                    ReportParameters[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    ReportViewer1.LocalReport.SetParameters(ReportParameters);
                    ReportViewer1.LocalReport.DataSources.Clear();
                }
            }

            if (Type == 0)
            {
                DTCompany = new DataTable();
                if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                {

                    DTCompany = MobjclsBLLSales.DisplayALLCompanyHeaderReport();
                }
                else
                {

                    DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(Company);
                }
                AllReport();


                DataTable DTSalesQuotation = MobjclsBLLSales.DisplaySalesQuotationReport(Company, Customer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);
                DataTable DTSalesOrder = MobjclsBLLSales.DisplaySalesOrderReport(Company, Customer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);
                DataTable DTSalesInvoice = MobjclsBLLSales.DisplaySalesInvoiceReport(Company, Customer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);
                DataTable DTSalesReturn = MobjclsBLLSales.DisplaySalesReturnReport(Company, Customer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);
                DataTable DTPOS = MobjclsBLLSales.DisplayPOSReport(Company, Customer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);


                if (DTSalesQuotation.Rows.Count <= 0 && DTSalesOrder.Rows.Count <= 0 && DTSalesOrder.Rows.Count <= 0 && DTSalesReturn.Rows.Count <= 0 && DTPOS.Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2035, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }

                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", DTSalesQuotation));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", DTSalesOrder));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", DTSalesInvoice));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", DTSalesInvoice));
                if(intDateType!=3)  // Due Date Checking
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", DTSalesReturn));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", DTPOS));
                }
                else
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster",""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster",""));
                }

                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", ""));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", ""));

            }
            if (Type == 7)
            {
                DataSet dtsetSalesQuotation = MobjclsBLLSales.DisplaySalesQuotation(Company, Customer, strFrom, strTo, Type, intstatus, intNo, chkFrom, chkTo, intDateType, strPermittedCompany);
                if (dtsetSalesQuotation.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2035, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (intNo > 0)
                {

                    DataSet dtSet = MobjclsBLLSales.DisplaySalesQuotation(Company, Customer, strFrom, strTo, Type, intstatus, intNo, chkFrom, chkTo, intDateType, strPermittedCompany);
                    int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                    DataTable DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(companyid);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", dtSet.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", dtSet.Tables[1]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", dtSet.Tables[2]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", dtSet.Tables[3]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", ""));

                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSDetails", ""));

                }
                else
                {
                    DTCompany = new DataTable();
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {

                        DTCompany = MobjclsBLLSales.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {

                        DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();

                    DataTable DTSalesQuotation = MobjclsBLLSales.DisplaySalesQuotationReport(Company, Customer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);

                    if (DTSalesQuotation.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2035, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", DTSalesQuotation));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", ""));

                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSDetails", ""));
                }
            }

            if (Type == 10) //POS
            {
                DataSet DTPOS1 = MobjclsBLLSales.DisplayPOS1(Company, Customer, strFrom, strTo, Type, intstatus, intNo, chkFrom, chkTo, intDateType, strPermittedCompany);

                if (DTPOS1.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2035, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }

                if (intNo > 0)
                {
                    DataSet dtSet = MobjclsBLLSales.DisplayPOS1(Company, Customer, strFrom, strTo, Type, intstatus, intNo, chkFrom, chkTo, intDateType, strPermittedCompany);
                    int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                    DataTable DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(companyid);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", DTPOS1.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSDetails", DTPOS1.Tables[1]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", DTPOS1.Tables[2]));

                }
                else
                {
                    
                    DTCompany = new DataTable();
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {

                        DTCompany = MobjclsBLLSales.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {

                        DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();

                    DataTable DTPOS = MobjclsBLLSales.DisplayPOSReport(Company, Customer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);

                    if (DTPOS.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2035, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }

                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", DTPOS));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", ""));
                }

            }

            if (Type == 8)
            {
                DataSet dtsetSalesOrder = MobjclsBLLSales.DisplaySalesOrder(Company, Customer, strFrom, strTo, Type, intstatus, intNo, chkFrom, chkTo, intDateType, strPermittedCompany);

                if (dtsetSalesOrder.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2035, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (intNo > 0)
                {
                    DataSet dtSet = MobjclsBLLSales.DisplaySalesOrder(Company, Customer, strFrom, strTo, Type, intstatus, intNo, chkFrom, chkTo, intDateType, strPermittedCompany);
                    int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                    DataTable DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(companyid);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));

                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", dtSet.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", dtSet.Tables[1]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", dtSet.Tables[2]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", dtSet.Tables[3]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", ""));

                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSDetails", ""));


                }
                else
                {
                    DTCompany = new DataTable();
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {

                        DTCompany = MobjclsBLLSales.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {

                        DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();

                    DataTable DTSalesOrder = MobjclsBLLSales.DisplaySalesOrderReport(Company, Customer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);
                    if (DTSalesOrder.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2035, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", DTSalesOrder));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", ""));

                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", ""));
                 

                }
            }

            if (Type == 9)
            {
                DataSet dtsetSalesInvoice = MobjclsBLLSales.DisplaySalesInvoice(Company, Customer, strFrom, strTo, Type, intstatus, intNo, SalesOrderid, chkFrom, chkTo, intDateType, strPermittedCompany);
                if (dtsetSalesInvoice.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2035, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (intNo > 0)
                {
                    DataSet dtSet = MobjclsBLLSales.DisplaySalesInvoice(Company, Customer, strFrom, strTo, Type, intstatus, intNo, SalesOrderid, chkFrom, chkTo, intDateType, strPermittedCompany);
                    int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                    DataTable DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(companyid);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", dtSet.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", dtSet.Tables[1]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", dtSet.Tables[2]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", dtSet.Tables[3]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", ""));

                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSDetails", ""));

                }
                else
                {
                    DTCompany = new DataTable();
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {

                        DTCompany = MobjclsBLLSales.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {

                        DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();

                    DataTable DTSalesInvoice = MobjclsBLLSales.DisplaySalesInvoiceReport(Company, Customer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);
                    if (DTSalesInvoice.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2035, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));

                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", DTSalesInvoice));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", ""));

                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSDetails", ""));
                }
            }

            if (Type == 11)
            {
                DataSet dtsetSalesReturn = MobjclsBLLSales.DisplaySalesReturn(Company, Customer, strFrom, strTo, intstatus, intNo, chkFrom, chkTo, intDateType, intExecutive, strPermittedCompany);

                if (dtsetSalesReturn.Tables[0].Rows.Count <= 0)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2035, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return;
                }
                if (intNo > 0)
                {
                    DataSet dtSet = MobjclsBLLSales.DisplaySalesReturn(Company, Customer, strFrom, strTo, intstatus, intNo, chkFrom, chkTo, intDateType, intExecutive, strPermittedCompany);
                    int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                    DataTable DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(companyid);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", dtSet.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", dtSet.Tables[1]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", dtSet.Tables[2]));

                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSDetails", ""));
                   


                }
                else
                {
                    DTCompany = new DataTable();
                    if (CboCompany.Text.Trim() == "ALL")
                    {

                        DTCompany = MobjclsBLLSales.DisplayALLCompanyHeaderReport();
                    }
                    else
                    {

                        DTCompany = MobjclsBLLSales.DisplayCompanyHeaderReport(Company);
                    }
                    AllReport();

                    DataTable DTSalesReturn = MobjclsBLLSales.DisplaySalesReturnReport(Company, Customer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);
                    if (DTSalesReturn.Rows.Count <= 0)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2035, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return;
                    }
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));

                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnMaster", DTSalesReturn));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STSalesReturnDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderMaster",""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesQuotationExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesOrderExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoicenDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_SalesInvoiceExpenseDetails", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_STTermsAndConditions", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSMaster", ""));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DatasetSales_POSDetails", ""));
                }
            }

            ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
            ReportViewer1.ZoomMode = ZoomMode.Percent;
            this.Cursor = Cursors.Default;
        }
       
        private void BtnShow_Click(object sender, EventArgs e)
        {              
            try
            {
                ReportViewer1.Clear();
                ReportViewer1.Reset();
                if (SalesReportValidation())
                {
                    BtnShow.Click -= new EventHandler(BtnShow_Click);
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                        BtnShow.Enabled = false;
                        ShowReport();
                    //}
                    //else
                    //{
                    //    if (ReportValidation())
                    //    {
                    //        BtnShow.Enabled = false;
                    //        ShowReport();
                    //    }
                    //}
                    Timer.Enabled = true;
                }
            }
            catch (Exception Ex)
            {
                Timer.Enabled = true;
            }            
        }
        private bool ReportValidation()
        {
            if (CboCompany.DataSource == null)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9014, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                CboCompany.Focus();
                return false;
            }
            return true;
        }
        private bool SalesReportValidation()
        {
            if (CboCompany.Enabled == true)
            {
                if (CboCompany.DataSource == null || CboCompany.SelectedValue == null || CboCompany.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9100, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpSalesReport.SetError(CboCompany, MsMessageCommon.Replace("#", "").Trim());
                    CboCompany.Focus();
                    return false;
                }
            }
            if (CboExecutive.Enabled == true)
            {
                if (CboExecutive.DataSource == null || CboExecutive.SelectedValue == null || CboExecutive.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9101, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpSalesReport.SetError(CboExecutive, MsMessageCommon.Replace("#", "").Trim());
                    CboExecutive.Focus();
                    return false;
                }
            }
            if (CboType.Enabled == true)
            {
                if (CboType.DataSource == null || CboType.SelectedValue == null || CboType.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9102, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpSalesReport.SetError(CboType, MsMessageCommon.Replace("#", "").Trim());
                    CboType.Focus();
                    return false;
                }
            }
            if (cboStatus.Enabled == true)
            {
                if (cboStatus.DataSource == null || cboStatus.SelectedValue == null || cboStatus.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9103, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpSalesReport.SetError(cboStatus, MsMessageCommon.Replace("#", "").Trim());
                    cboStatus.Focus();
                    return false;
                }
            }
            if (CboCustomer.Enabled == true)
            {
                if (CboCustomer.DataSource == null || CboCustomer.SelectedValue == null || CboCustomer.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9115, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpSalesReport.SetError(CboCustomer, MsMessageCommon.Replace("#", "").Trim());
                    CboCustomer.Focus();
                    return false;
                }
            }
            if (cboOrder.Enabled == true)
            {
                if (cboOrder.DataSource == null || cboOrder.SelectedValue == null || cboOrder.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9106, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpSalesReport.SetError(cboOrder, MsMessageCommon.Replace("#", "").Trim());
                    cboOrder.Focus();
                    return false;
                }
            }
            if (CboDateType.Enabled == true)
            {
                if (CboDateType.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9108, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpSalesReport.SetError(CboDateType, MsMessageCommon.Replace("#", "").Trim());
                    CboDateType.Focus();
                    return false;
                }
            }
            return true;
        }
        private void DtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
        }

        private void DtpToDate_ValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
        }        
 
        private void Timer_Tick(object sender, EventArgs e)
        {
            Timer.Enabled = false;           
            BtnShow.Click += new EventHandler(BtnShow_Click);
            BtnShow.Enabled = true;
            ErpSalesReport.Clear();
        }

       
        private void CboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpSalesReport.Clear();
        
            LoadCombos(3);
            LoadCombos(21);
            lblStatus.Enabled = false;
            lblNo.Enabled = false;
            lblCustomer.Enabled = false;
            cboStatus.SelectedValue = 0;
            cboOrder.SelectedValue = 0;
            CboCustomer.SelectedValue = 0;
            cboStatus.Enabled = false;
            cboOrder.Enabled = false;
            CboCustomer.Enabled = false;


            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesOrder)
            {
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(9);
                LoadCombos(6); 
            }
            else
            {
                //LoadCombos(21);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesQuotation)
            {
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(8);
                LoadCombos(5);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesInvoice)
            {
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(10);
                LoadCombos(7);
            }

            if (Convert.ToInt32(CboType.SelectedValue) ==  (int)OperationType.CreditNote)
            {
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                LoadCombos(14);
                LoadCombos(15);
            }      


        }

        private void CboType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpSalesReport.Clear();

            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            int intExecutive = Convert.ToInt32(CboExecutive.SelectedValue);
            int intStatusID = Convert.ToInt32(cboStatus.SelectedValue);

            //if (Convert.ToInt32(CboType.SelectedValue) == 11)
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.CreditNote)
            {
                LoadCombos(24);
            }
            else
            {
                LoadCombos(23);
            }


            if (Convert.ToInt32(CboType.SelectedValue) == 0)
            {
                //cboOrder.DataSource = null;
                //cboOrder.SelectedText = "ALL";
                cboOrder.SelectedValue= 0;
      
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesOrder) //sales Order
            {
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                lblCustomer.Enabled = true;
                CboCustomer.Enabled = true;

                strCondition = "";

                if (intCompany != 0) strCondition = " SIM.CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                LoadCombos(9);
                LoadCombos(26);

                strCondition = "";
                if (intCompany != 0) strCondition = " SO.CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
               // if (intStatusID != 0) strCondition = strCondition + " SO.StatusID =" + intStatusID + " AND";
                strCondition = strCondition + " VI.VendorTypeID = "+(int)VendorType.Customer+"";
                LoadCombos(18);
  
            }
            else
            {
              
             
            }
                if (Convert.ToInt32(CboType.SelectedValue) == 0)//all
            {

                cboStatus.SelectedValue = 0;
                lblStatus.Enabled = false;
                cboStatus.Enabled = false;
                lblNo.Enabled = false;
                cboOrder.Enabled = false;
                lblCustomer.Enabled = false;
                CboCustomer.Enabled = false;
                LoadCombos(2);


            }
                if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesQuotation)// Quotation
            {
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                lblCustomer.Enabled = true;
                CboCustomer.Enabled = true;

                LoadCombos(8);
                strCondition = "";

                if (intCompany != 0) strCondition = " SQM.CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);
                LoadCombos(25);

                strCondition = "";
                if (intCompany != 0) strCondition = " SQ.CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
             //   if (intStatusID != 0) strCondition = strCondition + " SQ.StatusID =" + intStatusID + " AND";
                strCondition = strCondition + " VI.VendorTypeID = "+(int)VendorType.Customer+"";
                LoadCombos(17);

            }
                if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesInvoice)//invoice
            {
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                lblCustomer.Enabled = true;
                CboCustomer.Enabled = true;

                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                LoadCombos(10);
                LoadCombos(27);

                strCondition = "";
                if (intCompany != 0) strCondition = " SI.CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
               // if (intStatusID != 0) strCondition = strCondition + " SI.StatusID =" + intStatusID + " AND";
                strCondition = strCondition + " VI.VendorTypeID = "+(int)VendorType.Customer+"";
                LoadCombos(19);

            }

                if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.CreditNote)//credit note
            {
                lblStatus.Enabled = true;
                cboStatus.Enabled = true;
                lblNo.Enabled = true;
                cboOrder.Enabled = true;
                lblCustomer.Enabled = true;
                CboCustomer.Enabled = true;

                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                LoadCombos(14);
                LoadCombos(28);

                strCondition = "";
                if (intCompany != 0) strCondition = " SR.CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
               // if (intStatusID != 0) strCondition = strCondition + " SR.StatusID =" + intStatusID + " AND";
                strCondition = strCondition + " VI.VendorTypeID = "+(int)VendorType.Customer+"";
                LoadCombos(20);


            }
                if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.POS)//POS
                {
                    lblStatus.Enabled = true;
                    cboStatus.Enabled = true;
                    lblNo.Enabled = true;
                    cboOrder.Enabled = true;
                    lblCustomer.Enabled = true;
                    CboCustomer.Enabled = true;
                    LoadCombos(32);
                    LoadCombos(29);// For status Filling

                    strCondition = "";
                    if (intCompany != 0) strCondition = " POS.CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                    //   if (intStatusID != 0) strCondition = strCondition + " SQ.StatusID =" + intStatusID + " AND";
                    strCondition = strCondition + " VI.VendorTypeID = "+(int)VendorType.Customer+"";
                    LoadCombos(30); //For Customer Filling

                    strCondition = "";

                    if (intCompany != 0) strCondition = " POS.CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                    LoadCombos(31); // For POS No Filling

                }

                  

        }

        private void cboStatus_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpSalesReport.Clear();

            strCondition = "";

            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            int intCustomer = Convert.ToInt32(CboCustomer.SelectedValue);
            int Type = Convert.ToInt32(CboType.SelectedValue);
            int intstatus = Convert.ToInt32(cboStatus.SelectedValue);
            int intNo = Convert.ToInt32(cboOrder.SelectedValue);
            int intExecutive = Convert.ToInt32(CboExecutive.SelectedValue);

     
            if (Convert.ToInt32(cboStatus.SelectedValue) == 0)
            {
               
            }
            
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesQuotation) 
            {

                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intCustomer != 0) strCondition = strCondition + " VendorID =" + intCustomer + " AND";
                if (intstatus != 0) strCondition = strCondition + " StatusID =" + intstatus + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);
                LoadCombos(11);

                    strCondition = "";
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                    if (intstatus != 0) strCondition = strCondition + " SQ.StatusID =" + intstatus + " AND";
                    strCondition = strCondition + " VI.VendorTypeID="+(int)VendorType.Customer+"";
                    LoadCombos(17);
                }
         if (Convert.ToInt32(cboStatus.SelectedValue) == 0)
                {
                   // LoadCombos(5);
                }
            
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesOrder) 
            {
                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intCustomer != 0) strCondition = strCondition + " VendorID =" + intCustomer + " AND";
                if (intstatus != 0) strCondition = strCondition + " StatusID =" + intstatus + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);
                LoadCombos(12);


                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (intstatus != 0) strCondition = strCondition + " SO.StatusID =" + intstatus + " AND";
                strCondition = strCondition + " VI.VendorTypeID="+(int)VendorType.Customer+"";
                LoadCombos(18);
            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesInvoice) 
            {
                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intstatus != 0) strCondition = strCondition + " StatusID =" + intstatus + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (intCustomer != 0) strCondition = strCondition + " SIM.VendorID =" + intCustomer + " AND";
                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);
                LoadCombos(13);

                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (intstatus != 0) strCondition = strCondition + " SI.StatusID =" + intstatus + " AND";
                strCondition = strCondition + " VI.VendorTypeID=" + (int)VendorType.Customer + "";
                LoadCombos(19); //For customer combo Filling
            }
            if (Convert.ToInt32(CboType.SelectedValue) ==  (int)OperationType.CreditNote) 
            {
                strCondition = "";
                if (intCompany != 0) strCondition = " SRM.CompanyID=" + intCompany + " AND";
                if (intstatus != 0) strCondition = strCondition + " SRM.StatusID =" + intstatus + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (intCustomer != 0) strCondition = strCondition + " SIM.VendorID =" + intCustomer + " AND";
                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);
                LoadCombos(16);

                strCondition = "";
                if (intCompany != 0) strCondition = " SR.CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (intstatus != 0) strCondition = strCondition + " SR.StatusID =" + intstatus + " AND";
                strCondition = strCondition + " VI.VendorTypeID=" + (int)VendorType.Customer + "";
                LoadCombos(20);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.POS)
            {

                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (intstatus != 0) strCondition = strCondition + " POS.StatusID =" + intstatus + " AND";
                strCondition = strCondition + " VI.VendorTypeID=" + (int)VendorType.Customer + "";

                LoadCombos(30);   // For Loading Customer


                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intCustomer != 0) strCondition = strCondition + " VendorID =" + intCustomer + " AND";
                if (intstatus != 0) strCondition = strCondition + " StatusID =" + intstatus + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                LoadCombos(31);  // For Loading POS No

            }
        }

        private void CboCustomer_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpSalesReport.Clear();

            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            int intCustomer = Convert.ToInt32(CboCustomer.SelectedValue);
            int Type = Convert.ToInt32(CboType.SelectedValue);
            int intstatus = Convert.ToInt32(cboStatus.SelectedValue);
            int intNo = Convert.ToInt32(cboOrder.SelectedValue);
            int intExecutive = Convert.ToInt32(CboExecutive.SelectedValue);

          
            if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
            {
                strCondition = "";
            }
            if (Convert.ToInt32(cboStatus.SelectedValue) == 0)
            {
                strCondition = "";
            }
            if (Convert.ToInt32(CboCustomer.SelectedValue) == 0)
            {
                strCondition = "";
            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesQuotation) 
            {
               
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intstatus != 0) strCondition = strCondition + " StatusID =" + intstatus + " AND";
                if (intExecutive != 0) strCondition = strCondition + " EmployeeID =" + intExecutive + " AND";
                if (intCustomer != 0) strCondition = strCondition + " VendorID =" + intCustomer + " AND";

                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);


                    LoadCombos(11);

            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesOrder) 
            {

                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intstatus != 0) strCondition = strCondition + " StatusID =" + intstatus + " AND";
                if (intExecutive != 0) strCondition = strCondition + " EmployeeID =" + intExecutive + " AND";
                if (intCustomer != 0) strCondition = strCondition + " VendorID =" + intCustomer + " AND";

                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);


                LoadCombos(12);

            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesInvoice) 

            {
                
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intstatus != 0) strCondition = strCondition + " StatusID =" + intstatus + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (intCustomer != 0) strCondition = strCondition + " VendorID =" + intCustomer + " AND";

                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                LoadCombos(13);

            }
            if (Convert.ToInt32(CboType.SelectedValue) ==  (int)OperationType.CreditNote) 
            {
                

                if (intCompany != 0) strCondition = " SRM.CompanyID=" + intCompany + " AND";
                if (intstatus != 0) strCondition = strCondition + " SRM.StatusID =" + intstatus + " AND";
                if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";
                if (intCustomer != 0) strCondition = strCondition + " SIM.VendorID =" + intCustomer + " AND";

                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);


                LoadCombos(22);

            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.POS)  // POS
            {

                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                if (intstatus != 0) strCondition = strCondition + " StatusID =" + intstatus + " AND";
                if (intExecutive != 0) strCondition = strCondition + " EmployeeID =" + intExecutive + " AND";
                if (intCustomer != 0 & intCustomer!=-1) strCondition = strCondition + " VendorID =" + intCustomer + " AND";
                if (intCustomer == -1) strCondition = strCondition + " isnull(VendorID,0) =0 AND";

                if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                LoadCombos(31);  //For Loading POS No

            }

        }
        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }
      

        private void cboOrder_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpSalesReport.Clear();
        }

        private void CboExecutive_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ErpSalesReport.Clear();

            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            int intExecutive = Convert.ToInt32(CboExecutive.SelectedValue);

           // if (Convert.ToInt32(CboExecutive.SelectedValue) == 0)
           // {
               
                lblStatus.Enabled = false;
                cboStatus.SelectedValue = 0;
                cboStatus.Enabled = false;
                lblCustomer.Enabled = false;
                CboCustomer.SelectedValue = 0;
                CboCustomer.Enabled = false;
                cboOrder.SelectedValue = 0;
                lblNo.Enabled = false;
                cboOrder.Enabled = false;

                LoadCombos(3);
           // }
           /* else
            {
                LoadCombos(2);
                if (Convert.ToInt32(CboType.SelectedValue) == 7)
                {
                  
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " EmployeeID =" + intExecutive + " AND";

                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                    LoadCombos(5); 
                }

                else if (Convert.ToInt32(CboType.SelectedValue) == 8)
                {

                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " EmployeeID =" + intExecutive + " AND";

                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                    LoadCombos(6);

                }
                else if (Convert.ToInt32(CboType.SelectedValue) == 9)
                {

                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";

                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                    LoadCombos(7);
                }

                else if (Convert.ToInt32(CboType.SelectedValue) == 11)
                {
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intExecutive != 0) strCondition = strCondition + " UM.EmployeeID =" + intExecutive + " AND";

                    if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

                    LoadCombos(15);
                }
            }*/
               
        }

        private void CboDateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpSalesReport.Clear();

            if (CboDateType.SelectedIndex == 0)
            {
                lblFrom.Enabled = false;
                lblTo.Enabled = false;
                DtpFromDate.Enabled = false;
                DtpToDate.Enabled = false;
                DtpFromDate.LockUpdateChecked = false;
                DtpToDate.LockUpdateChecked = false;
            }
            else
            {
                lblFrom.Enabled = true;
                lblTo.Enabled = true;
                DtpFromDate.Enabled = true;
                DtpToDate.Enabled = true;
                DtpFromDate.LockUpdateChecked = true;
            }
        }

        private void ReportViewer1_RenderingBegin(object sender, CancelEventArgs e)
        {

        }

        private void ReportViewer1_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {

        }
      
    }
}
