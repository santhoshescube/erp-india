﻿namespace MyBooksERP
{
    partial class FrmRptItemWiseProfit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptItemWiseProfit));
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.cboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblTo = new DevComponents.DotNetBar.LabelX();
            this.lblFrom = new DevComponents.DotNetBar.LabelX();
            this.DtpToDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.DtpFromDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.cboItem = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblItem = new DevComponents.DotNetBar.LabelX();
            this.lblCategory = new DevComponents.DotNetBar.LabelX();
            this.cboCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.lblSubCategory = new DevComponents.DotNetBar.LabelX();
            this.CboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboSubCategory = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.errReport = new System.Windows.Forms.ErrorProvider(this.components);
            this.expandablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errReport)).BeginInit();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.lblType);
            this.expandablePanel1.Controls.Add(this.cboType);
            this.expandablePanel1.Controls.Add(this.lblTo);
            this.expandablePanel1.Controls.Add(this.lblFrom);
            this.expandablePanel1.Controls.Add(this.DtpToDate);
            this.expandablePanel1.Controls.Add(this.DtpFromDate);
            this.expandablePanel1.Controls.Add(this.BtnShow);
            this.expandablePanel1.Controls.Add(this.cboItem);
            this.expandablePanel1.Controls.Add(this.lblItem);
            this.expandablePanel1.Controls.Add(this.lblCategory);
            this.expandablePanel1.Controls.Add(this.cboCategory);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Controls.Add(this.lblSubCategory);
            this.expandablePanel1.Controls.Add(this.CboCompany);
            this.expandablePanel1.Controls.Add(this.cboSubCategory);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(1028, 92);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 5;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            // 
            // lblType
            // 
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(14, 59);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(52, 23);
            this.lblType.TabIndex = 23;
            this.lblType.Text = "Type";
            // 
            // cboType
            // 
            this.cboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboType.DisplayMember = "Text";
            this.cboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboType.DropDownHeight = 75;
            this.cboType.FormattingEnabled = true;
            this.cboType.IntegralHeight = false;
            this.cboType.ItemHeight = 14;
            this.cboType.Location = new System.Drawing.Point(73, 62);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(194, 20);
            this.cboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboType.TabIndex = 22;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            this.cboType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboType_KeyPress);
            // 
            // lblTo
            // 
            // 
            // 
            // 
            this.lblTo.BackgroundStyle.Class = "";
            this.lblTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTo.Location = new System.Drawing.Point(734, 59);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 23);
            this.lblTo.TabIndex = 20;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            // 
            // 
            // 
            this.lblFrom.BackgroundStyle.Class = "";
            this.lblFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFrom.Location = new System.Drawing.Point(556, 59);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(33, 23);
            this.lblFrom.TabIndex = 18;
            this.lblFrom.Text = "From";
            // 
            // DtpToDate
            // 
            // 
            // 
            // 
            this.DtpToDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpToDate.ButtonDropDown.Visible = true;
            this.DtpToDate.CustomFormat = "dd MMM yyyy";
            this.DtpToDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpToDate.IsPopupCalendarOpen = false;
            this.DtpToDate.Location = new System.Drawing.Point(760, 62);
            this.DtpToDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpToDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpToDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpToDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpToDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.Size = new System.Drawing.Size(121, 20);
            this.DtpToDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpToDate.TabIndex = 21;
            this.DtpToDate.TextChanged += new System.EventHandler(this.DtpToDate_TextChanged);
            // 
            // DtpFromDate
            // 
            // 
            // 
            // 
            this.DtpFromDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpFromDate.ButtonDropDown.Visible = true;
            this.DtpFromDate.CustomFormat = "dd MMM yyyy";
            this.DtpFromDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpFromDate.IsPopupCalendarOpen = false;
            this.DtpFromDate.Location = new System.Drawing.Point(595, 62);
            this.DtpFromDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpFromDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpFromDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpFromDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpFromDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.Size = new System.Drawing.Size(121, 20);
            this.DtpFromDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpFromDate.TabIndex = 19;
            this.DtpFromDate.TextChanged += new System.EventHandler(this.DtpFromDate_TextChanged);
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(909, 62);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 20);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 17;
            this.BtnShow.Text = "Show";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // cboItem
            // 
            this.cboItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboItem.DisplayMember = "Text";
            this.cboItem.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboItem.DropDownHeight = 75;
            this.cboItem.FormattingEnabled = true;
            this.cboItem.IntegralHeight = false;
            this.cboItem.ItemHeight = 14;
            this.cboItem.Location = new System.Drawing.Point(595, 35);
            this.cboItem.Name = "cboItem";
            this.cboItem.Size = new System.Drawing.Size(167, 20);
            this.cboItem.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboItem.TabIndex = 16;
            this.cboItem.SelectedIndexChanged += new System.EventHandler(this.cboItem_SelectedIndexChanged);
            this.cboItem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboItem_KeyPress);
            // 
            // lblItem
            // 
            // 
            // 
            // 
            this.lblItem.BackgroundStyle.Class = "";
            this.lblItem.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblItem.Location = new System.Drawing.Point(556, 35);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(33, 23);
            this.lblItem.TabIndex = 15;
            this.lblItem.Text = "Item";
            // 
            // lblCategory
            // 
            // 
            // 
            // 
            this.lblCategory.BackgroundStyle.Class = "";
            this.lblCategory.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCategory.Location = new System.Drawing.Point(286, 35);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(79, 23);
            this.lblCategory.TabIndex = 5;
            this.lblCategory.Text = "Category";
            // 
            // cboCategory
            // 
            this.cboCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCategory.DisplayMember = "Text";
            this.cboCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCategory.DropDownHeight = 75;
            this.cboCategory.FormattingEnabled = true;
            this.cboCategory.IntegralHeight = false;
            this.cboCategory.ItemHeight = 14;
            this.cboCategory.Location = new System.Drawing.Point(371, 35);
            this.cboCategory.Name = "cboCategory";
            this.cboCategory.Size = new System.Drawing.Size(167, 20);
            this.cboCategory.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCategory.TabIndex = 5;
            this.cboCategory.SelectedIndexChanged += new System.EventHandler(this.cboCategory_SelectedIndexChanged);
            this.cboCategory.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCategory_KeyPress);
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(14, 35);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(52, 23);
            this.lblCompany.TabIndex = 0;
            this.lblCompany.Text = "Company";
            // 
            // lblSubCategory
            // 
            // 
            // 
            // 
            this.lblSubCategory.BackgroundStyle.Class = "";
            this.lblSubCategory.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSubCategory.Location = new System.Drawing.Point(286, 59);
            this.lblSubCategory.Name = "lblSubCategory";
            this.lblSubCategory.Size = new System.Drawing.Size(79, 23);
            this.lblSubCategory.TabIndex = 0;
            this.lblSubCategory.Text = "Sub Category";
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.DisplayMember = "Text";
            this.CboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCompany.DropDownHeight = 75;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.IntegralHeight = false;
            this.CboCompany.ItemHeight = 14;
            this.CboCompany.Location = new System.Drawing.Point(73, 35);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(194, 20);
            this.CboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCompany.TabIndex = 0;
            this.CboCompany.SelectedIndexChanged += new System.EventHandler(this.CboCompany_SelectedIndexChanged);
            this.CboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboCompany_KeyPress);
            // 
            // cboSubCategory
            // 
            this.cboSubCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSubCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSubCategory.DisplayMember = "Text";
            this.cboSubCategory.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboSubCategory.DropDownHeight = 134;
            this.cboSubCategory.FormattingEnabled = true;
            this.cboSubCategory.IntegralHeight = false;
            this.cboSubCategory.ItemHeight = 14;
            this.cboSubCategory.Location = new System.Drawing.Point(371, 62);
            this.cboSubCategory.Name = "cboSubCategory";
            this.cboSubCategory.Size = new System.Drawing.Size(167, 20);
            this.cboSubCategory.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboSubCategory.TabIndex = 1;
            this.cboSubCategory.SelectedIndexChanged += new System.EventHandler(this.cboSubCategory_SelectedIndexChanged);
            this.cboSubCategory.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboSubCategory_KeyPress);
            // 
            // ReportViewer1
            // 
            this.ReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Value = null;
            reportDataSource2.Value = null;
            reportDataSource3.Value = null;
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource3);
            this.ReportViewer1.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptRFQForm.rdlc";
            this.ReportViewer1.Location = new System.Drawing.Point(0, 92);
            this.ReportViewer1.Name = "ReportViewer1";
            this.ReportViewer1.Size = new System.Drawing.Size(1028, 306);
            this.ReportViewer1.TabIndex = 6;
            // 
            // errReport
            // 
            this.errReport.ContainerControl = this;
            // 
            // FrmRptItemWiseProfit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 398);
            this.Controls.Add(this.ReportViewer1);
            this.Controls.Add(this.expandablePanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptItemWiseProfit";
            this.Text = "FrmRptItemWiseProfit";
            this.Load += new System.EventHandler(this.FrmRptItemWiseProfit_Load);
            this.expandablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboItem;
        private DevComponents.DotNetBar.LabelX lblItem;
        private DevComponents.DotNetBar.LabelX lblCategory;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCategory;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.LabelX lblSubCategory;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboSubCategory;
        private Microsoft.Reporting.WinForms.ReportViewer ReportViewer1;
        private DevComponents.DotNetBar.LabelX lblTo;
        private DevComponents.DotNetBar.LabelX lblFrom;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpToDate;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpFromDate;
        private DevComponents.DotNetBar.LabelX lblType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboType;
        private System.Windows.Forms.ErrorProvider errReport;
    }
}