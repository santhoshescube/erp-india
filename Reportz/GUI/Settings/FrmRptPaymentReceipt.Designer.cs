﻿namespace MyBooksERP
{
    partial class FrmRptPaymentReceipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptPaymentReceipt));
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.lblDateType = new DevComponents.DotNetBar.LabelX();
            this.cboDateType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblReferenceNo = new DevComponents.DotNetBar.LabelX();
            this.CboReferenceNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblPaymentNo = new DevComponents.DotNetBar.LabelX();
            this.CboPaymentNo = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblTransactionType = new DevComponents.DotNetBar.LabelX();
            this.CboTransactionType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.lblTo = new DevComponents.DotNetBar.LabelX();
            this.lblFrom = new DevComponents.DotNetBar.LabelX();
            this.DtpToDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.DtpFromDate = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.lblSupplier = new DevComponents.DotNetBar.LabelX();
            this.CboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboSupplier = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCustomer = new DevComponents.DotNetBar.LabelX();
            this.CboCustomer = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.ErpPaymentReport = new System.Windows.Forms.ErrorProvider(this.components);
            this.expandablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErpPaymentReport)).BeginInit();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.lblDateType);
            this.expandablePanel1.Controls.Add(this.cboDateType);
            this.expandablePanel1.Controls.Add(this.lblReferenceNo);
            this.expandablePanel1.Controls.Add(this.CboReferenceNo);
            this.expandablePanel1.Controls.Add(this.lblPaymentNo);
            this.expandablePanel1.Controls.Add(this.CboPaymentNo);
            this.expandablePanel1.Controls.Add(this.lblTransactionType);
            this.expandablePanel1.Controls.Add(this.CboTransactionType);
            this.expandablePanel1.Controls.Add(this.lblTo);
            this.expandablePanel1.Controls.Add(this.lblFrom);
            this.expandablePanel1.Controls.Add(this.DtpToDate);
            this.expandablePanel1.Controls.Add(this.DtpFromDate);
            this.expandablePanel1.Controls.Add(this.BtnShow);
            this.expandablePanel1.Controls.Add(this.lblType);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Controls.Add(this.lblSupplier);
            this.expandablePanel1.Controls.Add(this.CboType);
            this.expandablePanel1.Controls.Add(this.CboCompany);
            this.expandablePanel1.Controls.Add(this.CboSupplier);
            this.expandablePanel1.Controls.Add(this.lblCustomer);
            this.expandablePanel1.Controls.Add(this.CboCustomer);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(908, 127);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 3;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            // 
            // lblDateType
            // 
            // 
            // 
            // 
            this.lblDateType.BackgroundStyle.Class = "";
            this.lblDateType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDateType.Location = new System.Drawing.Point(629, 42);
            this.lblDateType.Name = "lblDateType";
            this.lblDateType.Size = new System.Drawing.Size(51, 23);
            this.lblDateType.TabIndex = 23;
            this.lblDateType.Text = "Date Type";
            // 
            // cboDateType
            // 
            this.cboDateType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDateType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDateType.DisplayMember = "Text";
            this.cboDateType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDateType.DropDownHeight = 75;
            this.cboDateType.FormattingEnabled = true;
            this.cboDateType.IntegralHeight = false;
            this.cboDateType.ItemHeight = 14;
            this.cboDateType.Location = new System.Drawing.Point(686, 42);
            this.cboDateType.Name = "cboDateType";
            this.cboDateType.Size = new System.Drawing.Size(121, 20);
            this.cboDateType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboDateType.TabIndex = 7;
            this.cboDateType.SelectedIndexChanged += new System.EventHandler(this.cboDateType_SelectedIndexChanged);
            // 
            // lblReferenceNo
            // 
            // 
            // 
            // 
            this.lblReferenceNo.BackgroundStyle.Class = "";
            this.lblReferenceNo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblReferenceNo.Location = new System.Drawing.Point(344, 68);
            this.lblReferenceNo.Name = "lblReferenceNo";
            this.lblReferenceNo.Size = new System.Drawing.Size(53, 23);
            this.lblReferenceNo.TabIndex = 16;
            this.lblReferenceNo.Text = "No.";
            // 
            // CboReferenceNo
            // 
            this.CboReferenceNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboReferenceNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboReferenceNo.DisplayMember = "Text";
            this.CboReferenceNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboReferenceNo.DropDownHeight = 75;
            this.CboReferenceNo.FormattingEnabled = true;
            this.CboReferenceNo.IntegralHeight = false;
            this.CboReferenceNo.ItemHeight = 14;
            this.CboReferenceNo.Location = new System.Drawing.Point(415, 68);
            this.CboReferenceNo.Name = "CboReferenceNo";
            this.CboReferenceNo.Size = new System.Drawing.Size(199, 20);
            this.CboReferenceNo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboReferenceNo.TabIndex = 5;
            this.CboReferenceNo.SelectionChangeCommitted += new System.EventHandler(this.CboReferenceNo_SelectionChangeCommitted);
            this.CboReferenceNo.SelectedIndexChanged += new System.EventHandler(this.CboReferenceNo_SelectedIndexChanged);
            this.CboReferenceNo.SelectedValueChanged += new System.EventHandler(this.CboReferenceNo_SelectedValueChanged);
            this.CboReferenceNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblPaymentNo
            // 
            // 
            // 
            // 
            this.lblPaymentNo.BackgroundStyle.Class = "";
            this.lblPaymentNo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPaymentNo.Location = new System.Drawing.Point(343, 94);
            this.lblPaymentNo.Name = "lblPaymentNo";
            this.lblPaymentNo.Size = new System.Drawing.Size(63, 23);
            this.lblPaymentNo.TabIndex = 4;
            this.lblPaymentNo.Text = "Payment No";
            // 
            // CboPaymentNo
            // 
            this.CboPaymentNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboPaymentNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboPaymentNo.DisplayMember = "Text";
            this.CboPaymentNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboPaymentNo.DropDownHeight = 75;
            this.CboPaymentNo.FormattingEnabled = true;
            this.CboPaymentNo.IntegralHeight = false;
            this.CboPaymentNo.ItemHeight = 14;
            this.CboPaymentNo.Location = new System.Drawing.Point(415, 94);
            this.CboPaymentNo.Name = "CboPaymentNo";
            this.CboPaymentNo.Size = new System.Drawing.Size(199, 20);
            this.CboPaymentNo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboPaymentNo.TabIndex = 6;
            this.CboPaymentNo.SelectionChangeCommitted += new System.EventHandler(this.CboPaymentNo_SelectionChangeCommitted);
            this.CboPaymentNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblTransactionType
            // 
            // 
            // 
            // 
            this.lblTransactionType.BackgroundStyle.Class = "";
            this.lblTransactionType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTransactionType.Location = new System.Drawing.Point(23, 68);
            this.lblTransactionType.Name = "lblTransactionType";
            this.lblTransactionType.Size = new System.Drawing.Size(90, 23);
            this.lblTransactionType.TabIndex = 5;
            this.lblTransactionType.Text = "Transaction Type";
            // 
            // CboTransactionType
            // 
            this.CboTransactionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboTransactionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboTransactionType.DisplayMember = "Text";
            this.CboTransactionType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboTransactionType.DropDownHeight = 75;
            this.CboTransactionType.FormattingEnabled = true;
            this.CboTransactionType.IntegralHeight = false;
            this.CboTransactionType.ItemHeight = 14;
            this.CboTransactionType.Items.AddRange(new object[] {
            this.comboItem2,
            this.comboItem3});
            this.CboTransactionType.Location = new System.Drawing.Point(121, 68);
            this.CboTransactionType.Name = "CboTransactionType";
            this.CboTransactionType.Size = new System.Drawing.Size(199, 20);
            this.CboTransactionType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboTransactionType.TabIndex = 1;
            this.CboTransactionType.SelectionChangeCommitted += new System.EventHandler(this.CboTransactionType_SelectionChangeCommitted);
            this.CboTransactionType.SelectedIndexChanged += new System.EventHandler(this.CboTransactionType_SelectedIndexChanged);
            this.CboTransactionType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Payment";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "Receipt";
            // 
            // lblTo
            // 
            // 
            // 
            // 
            this.lblTo.BackgroundStyle.Class = "";
            this.lblTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTo.Location = new System.Drawing.Point(629, 96);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 20);
            this.lblTo.TabIndex = 7;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            // 
            // 
            // 
            this.lblFrom.BackgroundStyle.Class = "";
            this.lblFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFrom.Location = new System.Drawing.Point(629, 68);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(27, 23);
            this.lblFrom.TabIndex = 6;
            this.lblFrom.Text = "From";
            // 
            // DtpToDate
            // 
            // 
            // 
            // 
            this.DtpToDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpToDate.ButtonDropDown.Visible = true;
            this.DtpToDate.CustomFormat = "dd MMM yyyy";
            this.DtpToDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpToDate.IsPopupCalendarOpen = false;
            this.DtpToDate.Location = new System.Drawing.Point(686, 94);
            this.DtpToDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpToDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpToDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpToDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpToDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpToDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpToDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.ShowCheckBox = true;
            this.DtpToDate.Size = new System.Drawing.Size(121, 20);
            this.DtpToDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpToDate.TabIndex = 9;
            this.DtpToDate.ValueChanged += new System.EventHandler(this.DtpToDate_ValueChanged);
            // 
            // DtpFromDate
            // 
            // 
            // 
            // 
            this.DtpFromDate.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DtpFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DtpFromDate.ButtonDropDown.Visible = true;
            this.DtpFromDate.CustomFormat = "dd MMM yyyy";
            this.DtpFromDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.DtpFromDate.IsPopupCalendarOpen = false;
            this.DtpFromDate.Location = new System.Drawing.Point(686, 68);
            this.DtpFromDate.LockUpdateChecked = false;
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.DtpFromDate.MonthCalendar.BackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.DisplayMonth = new System.DateTime(2011, 4, 1, 0, 0, 0, 0);
            this.DtpFromDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DtpFromDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.DtpFromDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DtpFromDate.MonthCalendar.TodayButtonVisible = true;
            this.DtpFromDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.ShowCheckBox = true;
            this.DtpFromDate.Size = new System.Drawing.Size(121, 20);
            this.DtpFromDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DtpFromDate.TabIndex = 8;
            this.DtpFromDate.ValueChanged += new System.EventHandler(this.DtpFromDate_ValueChanged);
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(821, 94);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(75, 20);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 10;
            this.BtnShow.Text = "Show";
            // 
            // lblType
            // 
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(23, 94);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(90, 23);
            this.lblType.TabIndex = 2;
            this.lblType.Text = "Operation Type";
            // 
            // lblCompany
            // 
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(23, 42);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(53, 23);
            this.lblCompany.TabIndex = 0;
            this.lblCompany.Text = "Company";
            // 
            // lblSupplier
            // 
            // 
            // 
            // 
            this.lblSupplier.BackgroundStyle.Class = "";
            this.lblSupplier.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSupplier.Location = new System.Drawing.Point(343, 39);
            this.lblSupplier.Name = "lblSupplier";
            this.lblSupplier.Size = new System.Drawing.Size(62, 23);
            this.lblSupplier.TabIndex = 1;
            this.lblSupplier.Text = "Supplier";
            // 
            // CboType
            // 
            this.CboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboType.DisplayMember = "Text";
            this.CboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboType.DropDownHeight = 75;
            this.CboType.FormattingEnabled = true;
            this.CboType.IntegralHeight = false;
            this.CboType.ItemHeight = 14;
            this.CboType.Location = new System.Drawing.Point(121, 94);
            this.CboType.Name = "CboType";
            this.CboType.Size = new System.Drawing.Size(199, 20);
            this.CboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboType.TabIndex = 2;
            this.CboType.SelectionChangeCommitted += new System.EventHandler(this.CboType_SelectionChangeCommitted);
            this.CboType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.DisplayMember = "Text";
            this.CboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCompany.DropDownHeight = 75;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.IntegralHeight = false;
            this.CboCompany.ItemHeight = 14;
            this.CboCompany.Location = new System.Drawing.Point(121, 42);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(199, 20);
            this.CboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCompany.TabIndex = 0;
            this.CboCompany.SelectionChangeCommitted += new System.EventHandler(this.CboCompany_SelectionChangeCommitted);
            this.CboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // CboSupplier
            // 
            this.CboSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSupplier.DisplayMember = "Text";
            this.CboSupplier.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboSupplier.DropDownHeight = 75;
            this.CboSupplier.FormattingEnabled = true;
            this.CboSupplier.IntegralHeight = false;
            this.CboSupplier.ItemHeight = 14;
            this.CboSupplier.Location = new System.Drawing.Point(415, 42);
            this.CboSupplier.Name = "CboSupplier";
            this.CboSupplier.Size = new System.Drawing.Size(199, 20);
            this.CboSupplier.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboSupplier.TabIndex = 3;
            this.CboSupplier.SelectionChangeCommitted += new System.EventHandler(this.CboSupplier_SelectionChangeCommitted);
            this.CboSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCustomer
            // 
            // 
            // 
            // 
            this.lblCustomer.BackgroundStyle.Class = "";
            this.lblCustomer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCustomer.Location = new System.Drawing.Point(343, 39);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(53, 23);
            this.lblCustomer.TabIndex = 3;
            this.lblCustomer.Text = "Customer";
            // 
            // CboCustomer
            // 
            this.CboCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCustomer.DisplayMember = "Text";
            this.CboCustomer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboCustomer.DropDownHeight = 75;
            this.CboCustomer.FormattingEnabled = true;
            this.CboCustomer.IntegralHeight = false;
            this.CboCustomer.ItemHeight = 14;
            this.CboCustomer.Location = new System.Drawing.Point(415, 42);
            this.CboCustomer.Name = "CboCustomer";
            this.CboCustomer.Size = new System.Drawing.Size(200, 20);
            this.CboCustomer.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboCustomer.TabIndex = 4;
            this.CboCustomer.SelectionChangeCommitted += new System.EventHandler(this.CboCustomer_SelectionChangeCommitted);
            this.CboCustomer.SelectedValueChanged += new System.EventHandler(this.CboCustomer_SelectedValueChanged);
            // 
            // ReportViewer1
            // 
            this.ReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetSettingsFormReport_STPayment";
            reportDataSource1.Value = null;
            reportDataSource2.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource2.Value = null;
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.ReportViewer1.LocalReport.ReportEmbeddedResource = "MyBooksERP.bin.Debug.MainReports.RptMISPaymentReceipt.rdlc";
            this.ReportViewer1.Location = new System.Drawing.Point(0, 127);
            this.ReportViewer1.Name = "ReportViewer1";
            this.ReportViewer1.Size = new System.Drawing.Size(908, 326);
            this.ReportViewer1.TabIndex = 6;
            // 
            // Timer
            // 
            this.Timer.Interval = 1000;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // ErpPaymentReport
            // 
            this.ErpPaymentReport.ContainerControl = this;
            // 
            // FrmRptPaymentReceipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 453);
            this.Controls.Add(this.ReportViewer1);
            this.Controls.Add(this.expandablePanel1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptPaymentReceipt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Payment/Receipt";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmRptPaymentReceipt_Load);
            this.expandablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtpToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtpFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErpPaymentReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.LabelX lblCustomer;
        private DevComponents.DotNetBar.LabelX lblPaymentNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboPaymentNo;
        private DevComponents.DotNetBar.LabelX lblTransactionType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboTransactionType;
        private DevComponents.DotNetBar.LabelX lblTo;
        private DevComponents.DotNetBar.LabelX lblFrom;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpToDate;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput DtpFromDate;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.LabelX lblType;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.LabelX lblSupplier;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboSupplier;
        private DevComponents.DotNetBar.LabelX lblReferenceNo;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboReferenceNo;
        private Microsoft.Reporting.WinForms.ReportViewer ReportViewer1;
        private DevComponents.Editors.ComboItem comboItem2;
        private System.Windows.Forms.Timer Timer;
        //private DtSetSettingsFormReport dtSetSettingsFormReport;
        //private DtSetCompanyFormReport dtSetCompanyFormReport;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CboCustomer;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.DotNetBar.LabelX lblDateType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDateType;
        private System.Windows.Forms.ErrorProvider ErpPaymentReport;
    }
}