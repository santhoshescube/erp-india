﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;
using System.Data.SqlClient;

using System.IO;
using System.Drawing.Imaging;
/* 
============================================================
Author:		    <Author,,Sawmya>
Create date:    <Create Date,11 July 2011>
Description:	<Description,, Payment/Receipt Report Form>
============================================================
*/
namespace MyBooksERP
{
    public partial class FrmRptPaymentReceipt : DevComponents.DotNetBar.Office2007Form
    {
        #region  Variable Declaration

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        private string MsReportPath;
        private string name;
        private string PsReportFooter;
        private string MsMessageCaption = "";
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private bool MbViewPermission = true;
        private bool MblnPrintEmailPermission = false;//To Set View Permission
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private int chk;
        private int chkFrom;
        private int chkTo;
        private string strFrom;
        private string strTo;
        private string strCondition = "";
        private int Company;
        private DataTable DTCompany;
        private int TransactionType;
        private int Vendor;
        private int DateType;

        ClsMISBLLPaymentReceipt MobjClsMISBLLPaymentReceipt;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;

        #endregion //Variables Declaration

        #region Constructor
        public FrmRptPaymentReceipt()
        {
            InitializeComponent();

            PsReportFooter = ClsCommonSettings.ReportFooter;
            MobjClsMISBLLPaymentReceipt = new ClsMISBLLPaymentReceipt();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();
            BtnShow.Click += new EventHandler(BtnShow_Click);

        }
        #endregion Constructor

        private void FrmRptPaymentReceipt_Load(object sender, EventArgs e)
        {
            int Type = Convert.ToInt32(CboType.SelectedValue);
            LoadMessage();
            LoadCombos(0);
        //    SetPermissions();
            DtpFromDate.Value = ClsCommonSettings.GetServerDate();
            DtpToDate.Value = ClsCommonSettings.GetServerDate();
            CboTransactionType.SelectedIndex = 0;  //Payment         
            lblSupplier.Visible = true;
            CboSupplier.Visible = true;
        }
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.Payments, (int)ClsCommonSettings.ProductID);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.Payments, 4);
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettingsReports objClsBLLPermissionSettings = new clsBLLPermissionSettingsReports();
            if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.PaymentReceiptsReports, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            MbViewPermission = (MblnAddPermission || MblnPrintEmailPermission || MblnUpdatePermission || MblnDeletePermission);
            expandablePanel1.Enabled = MbViewPermission;
        }

        private bool LoadCombos(int intType)
        {
            try
            {
                DataTable datCombos = new DataTable();
                DataRow datrow;

                if (intType == 0 || intType == 1)
                {
                    //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    //{
                        datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID="+ClsCommonSettings.LoginCompanyID });
                       
                        CboCompany.ValueMember = "CompanyID";
                        CboCompany.DisplayMember = "CompanyName";
                        CboCompany.DataSource = datCombos;
                    //}
                    //else
                    //{
                    //    datCombos = MobjClsMISBLLPaymentReceipt.GetCompanyByPermission(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptPayments);
                    //    if (datCombos.Rows.Count <= 0)
                    //        CboCompany.DataSource = null;
                    //    else
                    //    {
                    //        datrow = datCombos.NewRow();
                    //        datrow["CompanyID"] = 0;
                    //        datrow["Name"] = "ALL";
                    //        datCombos.Rows.InsertAt(datrow, 0);
                    //        CboCompany.ValueMember = "CompanyID";
                    //        CboCompany.DisplayMember = "Name";
                    //        CboCompany.DataSource = datCombos;
                    //    }
                    //}
                }
                if (intType == 0 || intType == 2)
                {
                    strCondition = "OperationTypeID In (" + (int)OperationType.PurchaseOrder + "," + (int)OperationType.PurchaseInvoice + "," + (int)OperationType.StockTransfer + ")";
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "OperationTypeID, OperationType", "OperationTypeReference", strCondition });
                    //CboType.DataSource = null;
                    CboType.DisplayMember = "OperationType";
                    CboType.ValueMember = "OperationTypeID";
                    CboType.DataSource = datCombos;

                    //strCondition = "TypeID In (" + (int)OperationType.PurchaseOrder + "," + (int)OperationType.PurchaseInvoice + "," + (int)OperationType.SalesOrder + "," + (int)OperationType.SalesInvoice + "," + (int)OperationType.POS + "," + (int)OperationType.Complaints + ")";
                }
                if (intType == 0 || intType == 3)
                {
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        strCondition = "PO.PurchaseOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.PurchaseOrder + ")";
                    }
                    else
                    {
                        strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " And PO.PurchaseOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.PurchaseOrder + ")";
                    }
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "distinct VI.VendorID,VI.VendorName ", "dbo.InvVendorInformation VI Inner JOIN dbo.InvPurchaseOrderMaster PO ON VI.VendorID = PO.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboSupplier.DataSource = null;
                    CboSupplier.ValueMember = "VendorID";
                    CboSupplier.DisplayMember = "VendorName";
                    CboSupplier.DataSource = datCombos;
                }
                if (intType == 0 || intType == 4) //Purchase Order
                {
                    if (intType == 0)
                    {
                        strCondition = "PurchaseOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.PurchaseOrder + ")";
                    }
                    //else
                    //{

                    //}
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { " PurchaseOrderID,PurchaseOrderNo", " InvPurchaseOrderMaster", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["PurchaseOrderID"] = 0;
                    datrow["PurchaseOrderNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "PurchaseOrderID";
                    CboReferenceNo.DisplayMember = "PurchaseOrderNo";
                    CboReferenceNo.DataSource = datCombos;
                }
                if (intType == 0 || intType == 5) // Payment No Corresponding to Type
                {
                    strCondition = "OperationTypeID = " + Convert.ToInt32(CboType.SelectedValue);
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "ReceiptAndPaymentID,PaymentNumber", "AccReceiptAndPaymentMaster", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;
                }

                if (intType == 6) // purchase Invoice
                {
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        strCondition = " PI.PurchaseInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.PurchaseInvoice + ") ";
                    }
                    else
                    {
                        strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " And PI.PurchaseInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.PurchaseInvoice + ")";
                    }
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { " distinct VI.VendorID,VI.VendorName ", "dbo.InvVendorInformation VI Inner JOIN dbo.InvPurchaseInvoiceMaster PI ON VI.VendorID = PI.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboSupplier.DataSource = null;
                    CboSupplier.ValueMember = "VendorID";
                    CboSupplier.DisplayMember = "VendorName";
                    CboSupplier.DataSource = datCombos;
                }

                if (intType == 7)
                {
                    // strCondition = " PurchaseInvoiceID In (Select ReferenceID From InvReceiptAndPayment Where OperationTypeID = 5)";
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "PurchaseInvoiceID,PurchaseInvoiceNo", "InvPurchaseInvoiceMaster ", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["PurchaseInvoiceID"] = 0;
                    datrow["PurchaseInvoiceNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "PurchaseInvoiceID";
                    CboReferenceNo.DisplayMember = "PurchaseInvoiceNo";
                    CboReferenceNo.DataSource = datCombos;
                }

                if (intType == 8) // Payment No Corresponding to Type
                {
                    strCondition = "OperationTypeID = " + Convert.ToInt32(CboType.SelectedValue);
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "ReceiptAndPaymentID,PaymentNumber", "AccReceiptAndPaymentMaster", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;
                }

                if (intType == 9)
                {
                    strCondition = "OperationTypeID In (" + (int)OperationType.SalesOrder + "," + (int)OperationType.SalesInvoice + "," + (int)OperationType.POS + ")";
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "OperationTypeID,OperationType", "OperationTypeReference", strCondition });
                    //CboType.DataSource = null;
                    CboType.DisplayMember = "OperationType";
                    CboType.ValueMember = "OperationTypeID";
                    CboType.DataSource = datCombos;
                }


                if (intType == 10 || intType == 29)
                {
                    if (intType == 10)
                    {
                        strCondition = " SalesOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.SalesOrder + ")";
                    }
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "SalesOrderID,SalesOrderNo", "InvSalesOrderMaster ", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesOrderID"] = 0;
                    datrow["SalesOrderNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "SalesOrderID";
                    CboReferenceNo.DisplayMember = "SalesOrderNo";
                    CboReferenceNo.DataSource = datCombos;
                }
                if (intType == 11)
                {
                    //strCondition = " SalesInvoiceID In (Select ReferenceID From InvReceiptAndPayment Where OperationTypeID = 9)";
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "SalesInvoiceID,SalesInvoiceNo", "InvSalesInvoiceMaster ", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesInvoiceID"] = 0;
                    datrow["SalesInvoiceNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "SalesInvoiceID";
                    CboReferenceNo.DisplayMember = "SalesInvoiceNo";
                    CboReferenceNo.DataSource = datCombos;
                }
                if (intType == 12)
                {
                    strCondition = " SalesInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesInvoice + ")";
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "SalesInvoiceID,SalesInvoiceNo", "InvSalesInvoiceMaster ", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesInvoiceID"] = 0;
                    datrow["SalesInvoiceNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "SalesInvoiceID";
                    CboReferenceNo.DisplayMember = "SalesInvoiceNo";
                    CboReferenceNo.DataSource = datCombos;
                }
                if (intType == 13)
                {
                    strCondition = " PointOfSalesID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ")";
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "PointOfSalesID,SalesNo", "InvPOSMaster ", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["PointOfSalesID"] = 0;
                    datrow["SalesNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "PointOfSalesID";
                    CboReferenceNo.DisplayMember = "SalesNo";
                    CboReferenceNo.DataSource = datCombos;
                }

                if (intType == 14) // Sales Order
                {

                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        strCondition = " SO.SalesOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesOrder + ")";
                    }
                    else
                    {
                        strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " And SO.SalesOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesOrder + ")";
                    }
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "distinct VI.VendorID,VI.VendorName ", "dbo.InvVendorInformation VI Inner JOIN dbo.InvSalesOrderMaster SO ON VI.VendorID = SO.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboCustomer.DataSource = null;
                    CboCustomer.ValueMember = "VendorID";
                    CboCustomer.DisplayMember = "VendorName";
                    CboCustomer.DataSource = datCombos;
                }
                if (intType == 15) //Sales Invoice
                {

                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        strCondition = " SI.SalesInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesInvoice + ") ";
                    }
                    else
                    {
                        strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " And SI.SalesInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesInvoice + ")";
                    }
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "distinct VI.VendorID,VI.VendorName ", "dbo.InvVendorInformation VI Inner JOIN dbo.InvSalesInvoiceMaster SI ON VI.VendorID = SI.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboCustomer.DataSource = null;
                    CboCustomer.ValueMember = "VendorID";
                    CboCustomer.DisplayMember = "VendorName";
                    CboCustomer.DataSource = datCombos;
                }
                //if (intType == 16) //Complaint
                //{
                //    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                //    {
                //        strCondition = " PO.SalesInvoiceID In (Select ReferenceID From InvReceiptAndPayment Where OperationTypeID = " + (int)OperationType.SalesInvoice + ")";
                //    }
                //    else
                //    {
                //        strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " And PO.SalesInvoiceID In (Select ReferenceID From InvReceiptAndPayment Where OperationTypeID = " + (int)OperationType.SalesInvoice + ")";
                //    }
                //    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "distinct VI.VendorID,VI.VendorName ", "dbo.InvVendorInformation VI Inner JOIN dbo.InvSalesInvoiceMaster PO ON VI.VendorID = PO.VendorID inner join STComplaints C on C.ReferenceID = PO.SalesInvoiceID and C.OperationTypeID = " + (int)OperationType.SalesInvoice + "", strCondition });
                //    datrow = datCombos.NewRow();
                //    datrow["VendorID"] = 0;
                //    datrow["VendorName"] = "ALL";
                //    datCombos.Rows.InsertAt(datrow, 0);
                //    CboCustomer.DataSource = null;
                //    CboCustomer.ValueMember = "VendorID";
                //    CboCustomer.DisplayMember = "VendorName";
                //    CboCustomer.DataSource = datCombos;
                //}
                if (intType == 17) // POS
                {
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        strCondition = "  SO.PointOfSalesID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ") ";
                    }
                    else
                    {
                        strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " And SO.PointOfSalesID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ")";
                    }
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { " distinct VI.VendorID,VI.VendorName ", "dbo.InvVendorInformation VI Inner JOIN dbo.InvPOSMaster SO ON VI.VendorID = SO.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboCustomer.DataSource = null;
                    CboCustomer.ValueMember = "VendorID";
                    CboCustomer.DisplayMember = "VendorName";
                    CboCustomer.DataSource = datCombos;
                }
                //Select PO.PurchaseOrderID,PO.PurchaseOrderNo
                //From dbo.InvVendorInformation VI 
                //Inner JOIN dbo.STPurchaseOrderMaster PO ON VI.VendorID = PO.VendorID
                //Where PO.CompanyID = 64 AND PO.VendorID = 227

                if (intType == 18 || intType == 34)  // ReferenceNo according to Company and Supplier
                {
                    if (intType != 34)
                    {
                        strCondition = "";
                        if (Convert.ToInt32(CboCompany.SelectedValue) != 0)
                        {
                            strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and PO.PurchaseOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.PurchaseOrder + ")";

                            if (Convert.ToInt32(CboSupplier.SelectedValue) != 0)
                            {
                                strCondition = strCondition + " and PO.VendorID = " + Convert.ToInt32(CboSupplier.SelectedValue) + " and PO.PurchaseOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.PurchaseOrder + ")";
                            }
                        }
                    }


                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { " PO.PurchaseOrderID,PO.PurchaseOrderNo", " dbo.InvVendorInformation VI Inner JOIN dbo.InvPurchaseOrderMaster PO ON VI.VendorID = PO.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["PurchaseOrderID"] = 0;
                    datrow["PurchaseOrderNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "PurchaseOrderID";
                    CboReferenceNo.DisplayMember = "PurchaseOrderNo";
                    CboReferenceNo.DataSource = datCombos;
                }

                if (intType == 19 || intType == 35)
                {
                    if (intType != 35)
                    {
                        strCondition = "";
                        if (Convert.ToInt32(CboCompany.SelectedValue) != 0)
                        {
                            strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and PI.PurchaseInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.PurchaseInvoice + ")";

                            if (Convert.ToInt32(CboSupplier.SelectedValue) != 0)
                            {
                                strCondition = strCondition + " and PI.VendorID = " + Convert.ToInt32(CboSupplier.SelectedValue) + " and PI.PurchaseInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.PurchaseInvoice + ")";
                            }
                        }
                    }


                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "PI.PurchaseInvoiceID,PI.PurchaseInvoiceNo", "dbo.InvVendorInformation VI Inner JOIN dbo.InvPurchaseInvoiceMaster PI ON VI.VendorID = PI.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["PurchaseInvoiceID"] = 0;
                    datrow["PurchaseInvoiceNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "PurchaseInvoiceID";
                    CboReferenceNo.DisplayMember = "PurchaseInvoiceNo";
                    CboReferenceNo.DataSource = datCombos;
                }
                if (intType == 20 || intType == 36)
                {
                    if (intType != 36)
                    {
                        strCondition = "";
                        if (Convert.ToInt32(CboCompany.SelectedValue) != 0)
                        {
                            strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and SI.SalesOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.SalesOrder + ")";

                            if (Convert.ToInt32(CboCustomer.SelectedValue) != 0)
                            {
                                strCondition = strCondition + " and SI.VendorID = " + Convert.ToInt32(CboCustomer.SelectedValue) + " and SI.SalesOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.SalesOrder + ")";
                            }
                        }
                    }

                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "SI.SalesOrderID,SI.SalesOrderNo", "dbo.InvVendorInformation VI Inner JOIN dbo.InvSalesOrderMaster SI ON VI.VendorID = SI.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesOrderID"] = 0;
                    datrow["SalesOrderNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "SalesOrderID";
                    CboReferenceNo.DisplayMember = "SalesOrderNo";
                    CboReferenceNo.DataSource = datCombos;
                }
                if (intType == 21 || intType == 37)
                {
                    if (intType != 37)
                    {
                        strCondition = "";
                        if (Convert.ToInt32(CboCompany.SelectedValue) != 0)
                        {
                            strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and SI.SalesInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.SalesInvoice + ")";

                            if (Convert.ToInt32(CboCustomer.SelectedValue) != 0)
                            {
                                strCondition = strCondition + " and SI.VendorID = " + Convert.ToInt32(CboCustomer.SelectedValue) + " and SI.SalesInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesInvoice + ")";
                            }
                        }
                    }
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "distinct SI.SalesInvoiceID,SI.SalesInvoiceNo", "dbo.InvVendorInformation VI Inner JOIN dbo.InvSalesInvoiceMaster SI ON VI.VendorID = SI.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["SalesInvoiceID"] = 0;
                    datrow["SalesInvoiceNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "SalesInvoiceID";
                    CboReferenceNo.DisplayMember = "SalesInvoiceNo";
                    CboReferenceNo.DataSource = datCombos;
                }

                if (intType == 22)
                {
                    strCondition = "";
                    if (Convert.ToInt32(CboCompany.SelectedValue) != 0)
                    {
                        strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and C.ComplaintID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = 18)";

                        if (Convert.ToInt32(CboCustomer.SelectedValue) != 0)
                        {
                            strCondition = strCondition + " and PO.VendorID = " + Convert.ToInt32(CboCustomer.SelectedValue) + " and C.ComplaintID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  18)";
                        }
                    }
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "distinct C.ComplaintID,C.ComplaintNo", "dbo.InvVendorInformation VI Inner JOIN dbo.InvSalesInvoiceMaster PO ON VI.VendorID = PO.VendorID inner join STComplaints C on C.ReferenceID = PO.SalesInvoiceID and C.OperationTypeID = " + (int)OperationType.SalesInvoice + "", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ComplaintID"] = 0;
                    datrow["ComplaintNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "ComplaintID";
                    CboReferenceNo.DisplayMember = "ComplaintNo";
                    CboReferenceNo.DataSource = datCombos;
                }

                if (intType == 23)
                {
                    strCondition = "";
                    if (Convert.ToInt32(CboCompany.SelectedValue) != 0)
                    {
                        strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and SI.PointOfSalesID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ")";

                        if (Convert.ToInt32(CboCustomer.SelectedValue) != 0)
                        {
                            strCondition = strCondition + " and  SI.VendorID = " + Convert.ToInt32(CboCustomer.SelectedValue) + " and SI.PointOfSalesID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ")";
                        }
                    }
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "SI.PointOfSalesID,SI.SalesNo", "dbo.InvVendorInformation VI Inner JOIN dbo.InvPOSMaster SI ON VI.VendorID = SI.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["PointOfSalesID"] = 0;
                    datrow["SalesNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "PointOfSalesID";
                    CboReferenceNo.DisplayMember = "SalesNo";
                    CboReferenceNo.DataSource = datCombos;
                }
                if (intType == 24)
                {
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { " VendorID,VendorName ", "InvVendorInformation ", " VendorTypeID = "+(int)VendorType.Customer+" " });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboCustomer.DataSource = null;
                    CboCustomer.ValueMember = "VendorID";
                    CboCustomer.DisplayMember = "VendorName";
                    CboCustomer.DataSource = datCombos;
                }
                if (intType == 25)
                {
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "RPM.ReceiptAndPaymentID,RPM.PaymentNumber", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID=3 Inner Join InvPurchaseOrderMaster POM on RPD.ReferenceID=POM.PurchaseOrderID ", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;
                }

                if (intType == 26)
                {
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "RPM.ReceiptAndPaymentID,RPM.PaymentNumber", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID=5 Inner Join InvPurchaseInvoiceMaster PIM  on RPD.ReferenceID=PIM.PurchaseInvoiceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;

                }

                if (intType == 27)
                {
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "RPM.ReceiptAndPaymentID,RPM.PaymentNumber", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID=8 inner join InvSalesOrderMaster SOM on SOM.SalesOrderID=RPD.ReferenceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;

                }

                if (intType == 28)
                {
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "RPM.ReceiptAndPaymentID,RPM.PaymentNumber", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID=9 inner join InvSalesInvoiceMaster SIM on SIM.SalesInvoiceID=RPD.ReferenceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;

                }

                if (intType == 30)  // For PaymentNumber Filling on Supplier Bais(PurchaseOrder) 
                {
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "RPM.ReceiptAndPaymentID,RPM.PaymentNumber", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID=3 inner join InvPurchaseOrderMaster POM on POM.PurchaseOrderID= RPD.ReferenceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;
                }

                if (intType == 31) // For PaymentNumber Filling on Supplier Bais(PurchaseInvoice) 
                {
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "RPM.ReceiptAndPaymentID,RPM.PaymentNumber", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID=5 inner join InvPurchaseInvoiceMaster PIM on PIM.PurchaseInvoiceID= RPD.ReferenceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;
                }

                if (intType == 32) // For ReceiptNumber Filling on Customer Bais(SalesOrder) 
                {
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "RPM.ReceiptAndPaymentID,RPM.PaymentNumber", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID=8 inner join InvSalesOrderMaster SOM on SOM.SalesOrderID= RPD.ReferenceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;
                }

                if (intType == 33)  // For ReceiptNumber Filling on Customer Bais(SalesInvoice)
                {
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "RPM.ReceiptAndPaymentID,RPM.PaymentNumber", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID=9 inner join InvSalesInvoiceMaster SIM on SIM.SalesInvoiceID= RPD.ReferenceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;
                }

                //---------------------------------


                if (intType == 40)  // STStockTransferMaster --Payement No
                {
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "RPM.ReceiptAndPaymentID,RPM.PaymentNumber", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID=24 inner join InvStockTransferMaster STM on STM.StockTransferID=RPD.ReferenceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;

                }

                if (intType == 41) // Stock Transfer --CustomerFilling
                {
                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        strCondition = " STM.StockTransferID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.StockTransfer + ") ";
                    }
                    else
                    {
                        strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " And STM.StockTransferID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.StockTransfer + ")";
                    }
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { " distinct VI.VendorID,VI.VendorName ", "dbo.InvVendorInformation VI Inner JOIN dbo.InvStockTransferMaster STM ON VI.VendorID = STM.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboSupplier.DataSource = null;
                    CboSupplier.ValueMember = "VendorID";
                    CboSupplier.DisplayMember = "VendorName";
                    CboSupplier.DataSource = datCombos;
                }


                if (intType == 38) // Stock Transfer-- Reference No
                {
                    // strCondition = " PurchaseInvoiceID In (Select ReferenceID From InvReceiptAndPayment Where OperationTypeID = 5)";
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "StockTransferID,StockTransferNo", "InvStockTransferMaster ", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["StockTransferID"] = 0;
                    datrow["StockTransferNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "StockTransferID";
                    CboReferenceNo.DisplayMember = "StockTransferNo";
                    CboReferenceNo.DataSource = datCombos;
                }

                if (intType == 42)
                {

                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "STM.StockTransferID,STM.StockTransferNo", "dbo.InvVendorInformation VI Inner JOIN dbo.InvStockTransferMaster STM ON VI.VendorID = STM.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["StockTransferID"] = 0;
                    datrow["StockTransferNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "StockTransferID";
                    CboReferenceNo.DisplayMember = "StockTransferNo";
                    CboReferenceNo.DataSource = datCombos;
                }
                if (intType == 0 || intType == 43)
                {
                    cboDateType.Items.Clear();
                    cboDateType.Items.Add("ALL");
                    cboDateType.Items.Add("Payment Date");
                    cboDateType.Items.Add("Created Date");
                    cboDateType.SelectedIndex = 0;
                }
                if (intType == 44)
                {
                    cboDateType.Items.Clear();
                    cboDateType.Items.Add("ALL");
                    cboDateType.Items.Add("Receipt Date");
                    cboDateType.Items.Add("Created Date");
                    cboDateType.SelectedIndex = 0;
                }
                //b
                if (intType == 45)     //POS Customer Filling
                {


                    if (Convert.ToInt32(CboCompany.SelectedValue) == 0)
                    {
                        strCondition = " POS.POSID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ") ";
                    }
                    else
                    {
                        strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " And POS.POSID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ")";
                    }
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "distinct VI.VendorID,VI.VendorName ", "dbo.InvVendorInformation VI Inner JOIN dbo.InvPOSMaster POS ON VI.VendorID = POS.VendorID", strCondition });
                    datrow = datCombos.NewRow();

                    datrow["VendorID"] = 0;
                    datrow["VendorName"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);

                    if (MobjClsMISBLLPaymentReceipt.GenearlCustomerCheck())
                    {
                        datrow = datCombos.NewRow();
                        datrow["VendorID"] = -1;
                        datrow["VendorName"] = "General Customer";
                        datCombos.Rows.InsertAt(datrow, 1);
                    }

                    CboCustomer.DataSource = null;
                    CboCustomer.ValueMember = "VendorID";
                    CboCustomer.DisplayMember = "VendorName";
                    CboCustomer.DataSource = datCombos;

                }

                if (intType == 46)   //POS Reference No
                {
                    //strCondition = " SalesInvoiceID In (Select ReferenceID From InvReceiptAndPayment Where OperationTypeID = 9)";

                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "POSID,POSNo", "InvPOSMaster ", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["POSID"] = 0;
                    datrow["POSNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "POSID";
                    CboReferenceNo.DisplayMember = "POSNo";
                    CboReferenceNo.DataSource = datCombos;
                }

                if (intType == 47)  //  POS Payement No
                {
                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "RPM.ReceiptAndPaymentID,RPM.PaymentNumber", "AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID=10 inner join InvPOSMaster POS on POS.POSID=RPD.ReferenceID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["ReceiptAndPaymentID"] = 0;
                    datrow["PaymentNumber"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboPaymentNo.DataSource = null;
                    CboPaymentNo.ValueMember = "ReceiptAndPaymentID";
                    CboPaymentNo.DisplayMember = "PaymentNumber";
                    CboPaymentNo.DataSource = datCombos;

                }

                if (intType == 48)   //POS Reference No On Customer Basis
                {

                    datCombos = MobjClsMISBLLPaymentReceipt.FillCombos(new string[] { "POS.POSID,POS.POSNo", "InvPOSMaster POS left join InvVendorInformation VI ON VI.VendorID = POS.VendorID", strCondition });
                    datrow = datCombos.NewRow();
                    datrow["POSID"] = 0;
                    datrow["POSNo"] = "ALL";
                    datCombos.Rows.InsertAt(datrow, 0);
                    CboReferenceNo.DataSource = null;
                    CboReferenceNo.ValueMember = "POSID";
                    CboReferenceNo.DisplayMember = "POSNo";
                    CboReferenceNo.DataSource = datCombos;
                }



                MObjClsLogWriter.WriteLog("Combobox filled succssfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
                return false;
            }
        }
        private void ShowReport()
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            Company = Convert.ToInt32(CboCompany.SelectedValue);
            DateType = cboDateType.SelectedIndex;
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder || Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice || Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.StockTransfer )
            {
                Vendor = Convert.ToInt32(CboSupplier.SelectedValue);
            }
            if (Convert.ToInt32(CboType.SelectedValue) ==  (int)OperationType.SalesOrder  || Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesInvoice  || Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.POS || Convert.ToInt32(CboType.SelectedValue) == 18)
            {
                Vendor = Convert.ToInt32(CboCustomer.SelectedValue);
            }
            int Type = Convert.ToInt32(CboType.SelectedValue);
            strFrom = Microsoft.VisualBasic.Strings.Format(DtpFromDate.Value.Date, "dd MMM yyyy");
            strTo = Microsoft.VisualBasic.Strings.Format(DtpToDate.Value.Date, "dd MMM yyyy");
            if (Convert.ToInt32(CboTransactionType.SelectedIndex) == 0)
            {
                TransactionType = 0;
            }
            if (Convert.ToInt32(CboTransactionType.SelectedIndex) == 1)
            {
                TransactionType = 1;
            }

            int PaymentNo = Convert.ToInt32(CboPaymentNo.SelectedValue);
            int intNo = Convert.ToInt32(CboReferenceNo.SelectedValue);


            if (DtpFromDate.LockUpdateChecked == true)
            {
                chkFrom = 1;
            }
            else
            {
                chkFrom = 0;
            }
            if (DtpToDate.LockUpdateChecked == true)
            {
                chkTo = 1;
            }
            else
            {
                chkTo = 0;
            }
            MsReportPath = Application.StartupPath + "\\MainReports\\RptMISPaymentReceipt.rdlc";
            ReportViewer1.LocalReport.ReportPath = MsReportPath;
            ReportParameter[] ReportParameter = new ReportParameter[11];
            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);

            if (CboType.Text.Trim() == "PurchaseOrder" || CboType.Text.Trim() == "PurchaseInvoice" || CboType.Text.Trim() == "Transfer Order")
            {
                Vendor = Convert.ToInt32(CboSupplier.SelectedValue);
                name = "Supplier";
                ReportParameter[1] = new ReportParameter("Associative", Convert.ToString(CboSupplier.Text.Trim()), false);
                ReportParameter[8] = new ReportParameter("AssociativeType", name, false);
            }
            if (CboType.Text.Trim() == "SalesOrder" || CboType.Text.Trim() == "SalesInvoice" || CboType.Text.Trim() == "Complaints" || CboType.Text.Trim() == "POS")
            {
                Vendor = Convert.ToInt32(CboCustomer.SelectedValue);
                name = "Customer";
                ReportParameter[1] = new ReportParameter("Associative", Convert.ToString(CboCustomer.Text.Trim()), false);
                ReportParameter[8] = new ReportParameter("AssociativeType", name, false);
            }
            if (CboTransactionType.SelectedIndex == 0)
            {
                name = "Supplier";
                ReportParameter[1] = new ReportParameter("Associative", Convert.ToString(CboSupplier.Text.Trim()), false);
                ReportParameter[8] = new ReportParameter("AssociativeType", name, false);
            }
            if (CboTransactionType.SelectedIndex == 1)
            {
                name = "Customer";
                ReportParameter[1] = new ReportParameter("Associative", Convert.ToString(CboCustomer.Text.Trim()), false);
                ReportParameter[8] = new ReportParameter("AssociativeType", name, false);
            }
            ReportParameter[2] = new ReportParameter("Type", Convert.ToString(CboType.Text.Trim()), false);

            if (chkFrom == 1 & chkTo == 1)
            {
                ReportParameter[3] = new ReportParameter("FromDate", strFrom, false);
                ReportParameter[4] = new ReportParameter("ToDate", strTo, false);
            }

            else if (chkFrom == 1)
            {
                string strTo1 = "";
                strTo1 = "ALL";

                ReportParameter[3] = new ReportParameter("FromDate", strFrom, false);
                ReportParameter[4] = new ReportParameter("ToDate", strTo1, false);

            }
            else if (chkTo == 1)
            {
                string strFrom1 = "";
                strFrom1 = "ALL";
                ReportParameter[3] = new ReportParameter("FromDate", strFrom1, false);
                ReportParameter[4] = new ReportParameter("ToDate", strTo, false);

            }
            else
            {
                string strFrom1 = "";
                strFrom1 = "ALL";
                string strTo1 = "";
                strTo1 = "ALL";

                ReportParameter[3] = new ReportParameter("FromDate", strFrom1, false);
                ReportParameter[4] = new ReportParameter("ToDate", strTo1, false);
            }

            ReportParameter[5] = new ReportParameter("TransactionType", Convert.ToString(CboTransactionType.Text.Trim()), false);

            ReportParameter[6] = new ReportParameter("ReferenceNo", Convert.ToString(CboReferenceNo.Text.Trim()), false);
            ReportParameter[7] = new ReportParameter("PaymentNo", Convert.ToString(CboPaymentNo.Text.Trim()), false);
            ReportParameter[8] = new ReportParameter("AssociativeType", name, false);
            //ReportParameter[9] = new ReportParameter("TransactionType", CboTransactionType.Text.Trim(), false);

            if (chkFrom == 1 || chkTo == 1)
            {
                ReportParameter[9] = new ReportParameter("DateType", Convert.ToString(cboDateType.Text.Trim()), false);
            }
            else
            {
                string strDateType1 = "";
                ReportParameter[9] = new ReportParameter("DateType", strDateType1, false);
            }
            ReportParameter[10] = new ReportParameter("Company", Convert.ToString(CboCompany.Text.Trim()), false);


            ReportViewer1.LocalReport.SetParameters(ReportParameter);
            ReportViewer1.LocalReport.DataSources.Clear();
            DTCompany = new DataTable();
            if (CboCompany.Text.Trim() == "ALL")
            {
                DTCompany = MobjClsMISBLLPaymentReceipt.DisplayALLCompanyHeaderReport();
            }
            else
            {
                DTCompany = MobjClsMISBLLPaymentReceipt.DisplayCompanyHeaderReport(Company);
            }
            ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);

            //ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);  
            clsBLLPermissionSettingsReports objClsBLLPermissionSettings = new clsBLLPermissionSettingsReports();
            string strPermittedCompany;
            DataTable DTPermComp = new DataTable();
            if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            {
                strPermittedCompany = null;
            }
            else
            {
                DTPermComp = objClsBLLPermissionSettings.GetPermittedCompany(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (int)ControlOperationType.RptPayments);
                if (DTPermComp.Rows.Count <= 0)
                    strPermittedCompany = null;
                else
                    strPermittedCompany = DTPermComp.Rows[0]["IsEnabled"].ToString();
            }
            DataTable DT = MobjClsMISBLLPaymentReceipt.DisplayPaymentReceiptReport(Company, Type, Vendor, strFrom, strTo, TransactionType, PaymentNo, intNo, chkFrom, chkTo, DateType, strPermittedCompany);
            if (DT.Rows.Count <= 0)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 7317, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
            }
            else
            {
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STPayment", DT));
                ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                ReportViewer1.ZoomMode = ZoomMode.Percent;
                this.Cursor = Cursors.Default;
            }

        }
        public void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", DTCompany));
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {
                ReportViewer1.Clear();
                ReportViewer1.Reset();
                if (PARReportValidation())
                {
                    BtnShow.Click -= new EventHandler(BtnShow_Click);
                    if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
                    {
                        BtnShow.Enabled = false;
                        ShowReport();
                    }
                    else
                    {
                        if (ReportValidation())
                        {
                            BtnShow.Enabled = false;
                            ShowReport();
                        }
                    }
                    Timer.Enabled = true;
                }
            }
            catch (Exception Ex)
            {
                Timer.Enabled = true;
            }
        }

        private bool ReportValidation()
        {
            if (CboCompany.DataSource == null)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9014, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                CboCompany.Focus();
                return false;
            }
            return true;
        }
        private bool PARReportValidation()
        {
            if (CboCompany.Enabled == true)
            {
                if (CboCompany.DataSource == null || CboCompany.SelectedValue == null || CboCompany.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9100, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPaymentReport.SetError(CboCompany, MsMessageCommon.Replace("#", "").Trim());
                    CboCompany.Focus();
                    return false;
                }
            }
            if (CboTransactionType.Enabled == true)
            {
                if (CboTransactionType.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9116, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPaymentReport.SetError(CboTransactionType, MsMessageCommon.Replace("#", "").Trim());
                    CboTransactionType.Focus();
                    return false;
                }
            }
            if (CboType.Enabled == true)
            {
                if (CboType.DataSource == null || CboType.SelectedValue == null || CboType.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9117, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPaymentReport.SetError(CboType, MsMessageCommon.Replace("#", "").Trim());
                    CboType.Focus();
                    return false;
                }
            }
            if (CboSupplier.Enabled == true)
            {
                if (CboTransactionType.SelectedIndex == 0 && (CboSupplier.DataSource == null || CboSupplier.SelectedValue == null || CboSupplier.SelectedIndex == -1))
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9104, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPaymentReport.SetError(CboSupplier, MsMessageCommon.Replace("#", "").Trim());
                    CboSupplier.Focus();
                    return false;
                }
            }
            if (CboCustomer.Enabled == true)
            {
                if (CboTransactionType.SelectedIndex == 1 && (CboCustomer.DataSource == null || CboCustomer.SelectedValue == null || CboCustomer.SelectedIndex == -1))
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9115, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPaymentReport.SetError(CboCustomer, MsMessageCommon.Replace("#", "").Trim());
                    CboCustomer.Focus();
                    return false;
                }
            }
            if (CboReferenceNo.Enabled == true)
            {
                if (CboReferenceNo.DataSource == null || CboReferenceNo.SelectedValue == null || CboReferenceNo.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9106, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPaymentReport.SetError(CboReferenceNo, MsMessageCommon.Replace("#", "").Trim());
                    CboReferenceNo.Focus();
                    return false;
                }
            }
            if (CboPaymentNo.Enabled == true)
            {
                if (CboPaymentNo.DataSource == null || CboPaymentNo.SelectedValue == null || CboPaymentNo.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9118, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPaymentReport.SetError(CboPaymentNo, MsMessageCommon.Replace("#", "").Trim());
                    CboPaymentNo.Focus();
                    return false;
                }
            }
            if (cboDateType.Enabled == true)
            {
                if (cboDateType.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9108, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErpPaymentReport.SetError(cboDateType, MsMessageCommon.Replace("#", "").Trim());
                    cboDateType.Focus();
                    return false;
                }
            }
            return true;
        }
        private void EnableDisable()
        {
            if (CboTransactionType.SelectedIndex == 1 || (Convert.ToInt32(CboType.SelectedValue) ==  (int)OperationType.SalesOrder ) || (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesInvoice ) || (Convert.ToInt32(CboType.SelectedValue) == 18) || (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.POS))//Receipt
            {
                lblCustomer.Visible = true;
                CboCustomer.Visible = true;
                lblSupplier.Visible = false;
                CboSupplier.Visible = false;
            }
            if (CboTransactionType.SelectedIndex == 0 || (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice) || (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder))//Payment
            {
                lblSupplier.Visible = true;
                CboSupplier.Visible = true;
                lblCustomer.Visible = false;
                CboCustomer.Visible = false;
            }
            if (CboTransactionType.SelectedIndex == 0)
            {
                lblSupplier.Visible = true;
                CboSupplier.Visible = true;
                lblCustomer.Visible = false;
                CboCustomer.Visible = false;
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Timer.Enabled = false;
            BtnShow.Click += new EventHandler(BtnShow_Click);
            BtnShow.Enabled = true;
            ErpPaymentReport.Clear();
        }

        private void DtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPaymentReport.Clear();
        }

        private void DtpToDate_ValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPaymentReport.Clear();
        }

        private void CboType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ErpPaymentReport.Clear();

            ReportViewer1.Clear();
            ReportViewer1.Reset();

            strCondition = "OperationTypeID = " + Convert.ToInt32(CboType.SelectedValue);
            EnableDisable();

            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)//purchase order
            {
                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " PurchaseOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.PurchaseOrder + ")";
                LoadCombos(4); // For PurchaseOrderNo

                strCondition = "";
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " OperationTypeID="+(int)OperationType.PurchaseOrder+"";
                LoadCombos(25);  // For PayementNo

                //LoadCombos(4);
                //LoadCombos(5);
                LoadCombos(3);// For Supplier
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice) //purchase invoice
            {

                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " PurchaseInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.PurchaseInvoice + ")";
                LoadCombos(7);

                strCondition = "";
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " OperationTypeID="+(int)OperationType.PurchaseInvoice+"";
                LoadCombos(26);
                //LoadCombos(8);
                LoadCombos(6);
            }
            if (Convert.ToInt32(CboType.SelectedValue) ==  (int)OperationType.SalesOrder ) //sales order
            {
                strCondition = "";

                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " SalesOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.SalesOrder + ")";
                LoadCombos(29); //

                strCondition = "";
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " OperationTypeID=" + (int)OperationType.SalesOrder + "";
                LoadCombos(32);

                LoadCombos(14);
                //LoadCombos(10);
                //LoadCombos(8);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesInvoice ) //sales invoice
            {
                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " SalesInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.SalesInvoice + ")";
                LoadCombos(11); //For SalesInvoiceNo

                strCondition = "";
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " OperationTypeID=" + (int)OperationType.SalesInvoice + "";
                LoadCombos(33); // For ReceiptNo

                //LoadCombos(8);
                LoadCombos(15);
            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.POS) //POS
            {
                LoadCombos(45);

                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " POSID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =   " + (int)OperationType.POS + ")";
                LoadCombos(46);


                strCondition = "";
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " OperationTypeID= " + (int)OperationType.POS + "";
                LoadCombos(47);

            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.StockTransfer )
            {
                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " StockTransferID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.StockTransfer + ")";
                LoadCombos(38);

                strCondition = "";
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " OperationTypeID=" + (int)OperationType.StockTransfer + "";
                LoadCombos(40);

                LoadCombos(41);
            }

            if (Convert.ToInt32(CboType.SelectedValue) == 18) //Complaint
            {
                LoadCombos(12);
                LoadCombos(16);
                LoadCombos(8);
            }
            //if (Convert.ToInt32(CboType.SelectedValue) == 10) //POS
            //{
            //    LoadCombos(13);
            //    LoadCombos(17);
            //    LoadCombos(8);
            //}
        }

        private void CboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPaymentReport.Clear();

            //EnableDisable();
            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);

            CboTransactionType.SelectedIndex = 0;
            LoadCombos(2);
            LoadCombos(3);
            EnableDisable();


            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)//purchase order
            {
                // LoadCombos(18);
                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                strCondition = strCondition + " PurchaseOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.PurchaseOrder + ")";

                LoadCombos(4);


            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice)//purchase invoice
            {
                LoadCombos(19);
            }
            if (Convert.ToInt32(CboType.SelectedValue) ==  (int)OperationType.SalesOrder ) //sales order
            {
                LoadCombos(20);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesInvoice ) //sales invoice
            {
                LoadCombos(21);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == 18) //Complaint
            {
                LoadCombos(22);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.POS) //POS
            {
                LoadCombos(23);
            }


        }

        private void CboTransactionType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPaymentReport.Clear();

            EnableDisable();
            // string strCondition = "";

            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            int intVendorID = Convert.ToInt32(CboSupplier.SelectedValue);//Supplier

            int Transaction = CboTransactionType.SelectedIndex;
            if (Transaction == 0)
            {
                LoadCombos(2);
                LoadCombos(3);
                //LoadCombos(22);               
                lblCustomer.Visible = false;
                CboCustomer.Visible = false;
                lblSupplier.Visible = true;
                CboSupplier.Visible = true;

                // strCondition = "PurchaseOrderID In (Select ReferenceID From InvReceiptAndPayment Where OperationTypeID = 3)";

                strCondition = "";

                if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)
                {
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    if (intVendorID != 0) strCondition = " intVendorID=" + intVendorID + " AND";

                    strCondition = strCondition + " PurchaseOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.PurchaseOrder + ")";
                    LoadCombos(4);
                }
                else if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice)
                {

                }

            }
            if (Transaction == 1)
            {
                LoadCombos(9);
                LoadCombos(14);
                lblSupplier.Visible = false;
                CboSupplier.Visible = false;
                lblCustomer.Visible = true;
                CboCustomer.Visible = true;

                strCondition = "";
                if (Convert.ToInt32(CboType.SelectedValue) ==  (int)OperationType.SalesOrder )  //For Filling Receipt No
                {
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    strCondition = strCondition + " OperationTypeID = " + (int)OperationType.SalesOrder + "";
                    LoadCombos(8);

                }

                if (Convert.ToInt32(CboType.SelectedValue) ==  (int)OperationType.SalesOrder ) // For Filling SalesOrder No
                {
                    strCondition = "";
                    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
                    strCondition = strCondition + " SalesOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.SalesOrder + ")";
                    LoadCombos(29);

                }

            }
        }

        private void CboCustomer_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPaymentReport.Clear();

            EnableDisable();

            strCondition = "";
            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            int intTransactionTypeType = Convert.ToInt32(CboTransactionType.SelectedValue);
            int intVendorID1 = Convert.ToInt32(CboCustomer.SelectedValue);//For Customer

            if (Convert.ToInt32(CboType.SelectedValue) ==  (int)OperationType.SalesOrder ) //sales order
            {
                strCondition = "";
                if (intCompany != 0) strCondition = " CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and SI.SalesOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesOrder + ") AND";
                if (intVendorID1 != 0) strCondition = strCondition + " SI.VendorID = " + Convert.ToInt32(CboCustomer.SelectedValue) + " and SI.SalesOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesOrder + ") AND";
                strCondition = strCondition + " SI.SalesOrderID In(Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesOrder + ")";

                LoadCombos(36);

                strCondition = "";
                if (intCompany != 0) strCondition = " Payment.CompanyID=" + intCompany + " AND";
                if (intVendorID1 != 0) strCondition = strCondition + " SOM.VendorID=" + intVendorID1 + " AND";
                strCondition = strCondition + " OperationTypeID=" + (int)OperationType.SalesOrder + "";
                // LoadCombos(8);
                LoadCombos(32);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesInvoice ) //sales invoice
            {
                strCondition = "";
                if (intCompany != 0) strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and SI.SalesInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesInvoice + ") AND";
                if (intVendorID1 != 0) strCondition = strCondition + " SI.VendorID = " + Convert.ToInt32(CboCustomer.SelectedValue) + " and SI.SalesInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesInvoice + ") AND";
                strCondition = strCondition + " SI.SalesInvoiceID In(Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.SalesInvoice + ")";

                LoadCombos(37);

                strCondition = "";
                if (intCompany != 0) strCondition = " Payment.CompanyID=" + intCompany + " AND";
                if (intVendorID1 != 0) strCondition = strCondition + " SIM.VendorID=" + intVendorID1 + " AND";
                strCondition = strCondition + " OperationTypeID=" + (int)OperationType.SalesInvoice + "";

                //LoadCombos(8);
                LoadCombos(33);

            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.POS) //POS
            {
                strCondition = "";
                if (intCompany != 0) strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and POS.POSID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ") AND";
                if (intVendorID1 != 0 & intVendorID1 != 1) strCondition = strCondition + " POS.VendorID = " + Convert.ToInt32(CboCustomer.SelectedValue) + " and POS.POSID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ") AND";
                if (intVendorID1 == 1) strCondition = strCondition + " isnull(POS.VendorID,0) = 0 " + " and POS.POSID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ") AND";
                strCondition = strCondition + " POS.POSID In(Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ")";

                LoadCombos(48);

                strCondition = "";
                if (intCompany != 0) strCondition = " Payment.CompanyID=" + intCompany + " AND";

                if (intVendorID1 != 0 & intVendorID1 != 1) strCondition = strCondition + " POS.VendorID=" + intVendorID1 + " AND";
                if (intVendorID1 == 1) strCondition = strCondition + " isnull(POS.VendorID,0) = 0" + " AND";
                strCondition = strCondition + " OperationTypeID= " + (int)OperationType.POS + "";

                LoadCombos(47);

            }

            if (Convert.ToInt32(CboType.SelectedValue) == 18) //Complaint
            {
                LoadCombos(22);
                LoadCombos(8);
            }
            //if (Convert.ToInt32(CboType.SelectedValue) == 10) //POS
            //{
            //    LoadCombos(23);
            //    LoadCombos(8);
            //}
        }

        private void CboSupplier_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ErpPaymentReport.Clear();

            strCondition = "";
            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            int intTransactionTypeType = Convert.ToInt32(CboTransactionType.SelectedValue);
            int intVendorID2 = Convert.ToInt32(CboSupplier.SelectedValue);//For Supplier


            ReportViewer1.Clear();
            ReportViewer1.Reset();
            EnableDisable();
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)//purchase order
            {
                strCondition = "";
                if (intCompany != 0) strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and PO.PurchaseOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = "+(int)OperationType.PurchaseOrder+") AND";
                if (intVendorID2 != 0) strCondition = strCondition + " PO.VendorID = " + Convert.ToInt32(CboSupplier.SelectedValue) + " and PO.PurchaseOrderID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = "+(int)OperationType.PurchaseOrder+") AND";
                strCondition = strCondition + " PO.PurchaseOrderID In(Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = "+(int)OperationType.PurchaseOrder+")";

                //LoadCombos(18);
                LoadCombos(34);

                strCondition = "";
                if (intCompany != 0) strCondition = " Payment.CompanyID=" + intCompany + " AND";
                if (intVendorID2 != 0) strCondition = strCondition + " POM.VendorID=" + intVendorID2 + " AND";
                strCondition = strCondition + " OperationTypeID="+(int)OperationType.PurchaseOrder+"";
                //LoadCombos(8);
                LoadCombos(30);
            }
            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice) //purchase invoice 
            {

                strCondition = "";
                if (intCompany != 0) strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and PI.PurchaseInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = "+(int)OperationType.PurchaseInvoice+") AND";
                if (intVendorID2 != 0) strCondition = strCondition + " PI.VendorID = " + Convert.ToInt32(CboSupplier.SelectedValue) + " and PI.PurchaseInvoiceID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = "+(int)OperationType.PurchaseInvoice+") AND";
                strCondition = strCondition + " PI.PurchaseInvoiceID In(Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = "+(int)OperationType.PurchaseInvoice+")";
                //LoadCombos(19);
                LoadCombos(35);

                strCondition = "";
                if (intCompany != 0) strCondition = " Payment.CompanyID=" + intCompany + " AND";
                if (intVendorID2 != 0) strCondition = strCondition + " PIM.VendorID=" + intVendorID2 + " AND";
                strCondition = strCondition + " OperationTypeID="+(int)OperationType.PurchaseInvoice+"";
                //LoadCombos(8);
                LoadCombos(31);
            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.StockTransfer ) //Stock Tranfer 
            {

                strCondition = "";
                if (intCompany != 0) strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and STM.StockTransferID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.StockTransfer + ") AND";
                if (intVendorID2 != 0) strCondition = strCondition + " STM.VendorID = " + Convert.ToInt32(CboSupplier.SelectedValue) + " and STM.StockTransferID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.StockTransfer + ") AND";
                strCondition = strCondition + " STM.StockTransferID In(Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID = " + (int)OperationType.StockTransfer + ")";

                LoadCombos(42);

                strCondition = "";
                if (intCompany != 0) strCondition = " Payment.CompanyID=" + intCompany + " AND";
                if (intVendorID2 != 0) strCondition = strCondition + " STM.VendorID=" + intVendorID2 + " AND";
                strCondition = strCondition + " OperationTypeID=" + (int)OperationType.StockTransfer + "";
                //LoadCombos(8);
                //LoadCombos(31);
                LoadCombos(40);
            }




        }
        private void ComboBox_KeyPress(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void CboPaymentNo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPaymentReport.Clear();

        }

        private void CboReferenceNo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //ReportViewer1.Clear();
            //ReportViewer1.Reset();

            //strCondition = "";

            //// int intVendorID = Convert.ToInt32(CboSupplier.SelectedValue);//Supplier
            //int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            //string intReferenceNo = CboReferenceNo.Text;


            //if (Convert.ToInt32(CboType.SelectedValue) == 3)
            //{
            //    if (intCompany != 0) strCondition = " CompanyID=" + intCompany + " AND";
            //    //if (intVendorID != 0) strCondition = " intVendorID=" + intVendorID + " AND";

            //   // strCondition = strCondition + " PurchaseOrderID In (Select ReferenceID From InvReceiptAndPayment Where OperationTypeID = 3)";

            //    if (intReferenceNo!="ALL") strCondition=strCondition + " ReferenceID In(Select PurchaseOrderID from STPurchaseOrderMaster where PurchaseOrderNo='" + intReferenceNo + "') AND";

            //    strCondition = strCondition + " OperationTypeID=3";

            //    //if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);

            //    LoadCombos(25);

            //}

        }

        private void CboReferenceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string intReferenceNo = CboReferenceNo.Text;
            ErpPaymentReport.Clear();

        }

        private void CboReferenceNo_SelectedValueChanged(object sender, EventArgs e)
        {

            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPaymentReport.Clear();

            strCondition = "";

            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            string intReferenceNo = CboReferenceNo.Text;
            int intVendorID;

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseOrder)
            {
                intVendorID = Convert.ToInt32(CboSupplier.SelectedValue);
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                if (intReferenceNo != "ALL") strCondition = strCondition + " RPD.ReferenceID In(Select PurchaseOrderID from InvPurchaseOrderMaster where PurchaseOrderNo='" + intReferenceNo + "') AND";
                if (intVendorID != 0) strCondition = strCondition + " POM.VendorID=" + intVendorID + " AND";

                strCondition = strCondition + " OperationTypeID="+(int)OperationType.PurchaseOrder+"";
                //if (strCondition.Length > 3) strCondition = strCondition.Substring(0, strCondition.Length - 3);
                LoadCombos(25);
            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.PurchaseInvoice)
            {
                intVendorID = Convert.ToInt32(CboSupplier.SelectedValue);
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                if (intReferenceNo != "ALL") strCondition = strCondition + " RPD.ReferenceID In(Select PurchaseInvoiceID from InvPurchaseInvoiceMaster where PurchaseInvoiceNo='" + intReferenceNo + "') AND";
                if (intVendorID != 0) strCondition = strCondition + " PIM.VendorID=" + intVendorID + " AND";
                strCondition = strCondition + " OperationTypeID="+(int)OperationType.PurchaseInvoice+"";
                LoadCombos(26);

            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.StockTransfer )
            {
                intVendorID = Convert.ToInt32(CboSupplier.SelectedValue);
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                if (intReferenceNo != "ALL") strCondition = strCondition + " RPD.ReferenceID In(Select StockTransferID from InvStockTransferMaster where StockTransferNo='" + intReferenceNo + "') AND";
                if (intVendorID != 0) strCondition = strCondition + " STM.VendorID=" + intVendorID + " AND";
                strCondition = strCondition + " OperationTypeID=" + (int)OperationType.StockTransfer + "";
                LoadCombos(40);

            }




            if (Convert.ToInt32(CboType.SelectedValue) ==  (int)OperationType.SalesOrder )
            {
                intVendorID = Convert.ToInt32(CboCustomer.SelectedValue);
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                if (intReferenceNo != "ALL") strCondition = strCondition + " RPD.ReferenceID In(Select SalesOrderID from InvSalesOrderMaster where SalesOrderNo='" + intReferenceNo + "') AND";
                if (intVendorID != 0) strCondition = strCondition + " SOM.VendorID=" + intVendorID + " AND";
                strCondition = strCondition + " OperationTypeID=" + (int)OperationType.SalesOrder + "";
                LoadCombos(27);

            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.SalesInvoice )
            {
                intVendorID = Convert.ToInt32(CboCustomer.SelectedValue);
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                if (intReferenceNo != "ALL") strCondition = strCondition + " RPD.ReferenceID In(Select SalesInvoiceID from InvSalesInvoiceMaster where SalesInvoiceNo='" + intReferenceNo + "') AND";
                if (intVendorID != 0) strCondition = strCondition + " SIM.VendorID=" + intVendorID + " AND";
                strCondition = strCondition + " OperationTypeID=" + (int)OperationType.SalesInvoice + "";
                LoadCombos(28);
            }

            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.POS)
            {
                intVendorID = Convert.ToInt32(CboCustomer.SelectedValue);
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";
                if (intReferenceNo != "ALL") strCondition = strCondition + " RPD.ReferenceID In(Select POSID from InvPOSMaster where POSNo='" + intReferenceNo + "') AND";
                if (intVendorID != 0 & intVendorID != -1) strCondition = strCondition + " POS.VendorID=" + intVendorID + " AND";
                if (intVendorID == -1) strCondition = strCondition + " isnull(POS.VendorID,0)=" + "0" + " AND";
                strCondition = strCondition + " OperationTypeID= " + (int)OperationType.POS + "";
                LoadCombos(47);

            }


        }

        private void CboTransactionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpPaymentReport.Clear();

            if (CboTransactionType.SelectedIndex == 0)
            {
                LoadCombos(43);
            }
            else
            {
                LoadCombos(44);
            }
        }

        private void cboDateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErpPaymentReport.Clear();

            if (cboDateType.SelectedIndex == 0)
            {
                lblFrom.Enabled = false;
                lblTo.Enabled = false;
                DtpFromDate.Enabled = false;
                DtpToDate.Enabled = false;
                DtpFromDate.LockUpdateChecked = false;
                DtpToDate.LockUpdateChecked = false;
            }
            else
            {
                lblFrom.Enabled = true;
                lblTo.Enabled = true;
                DtpFromDate.Enabled = true;
                DtpToDate.Enabled = true;
                DtpFromDate.LockUpdateChecked = true;
            }
        }

        private void CboCustomer_SelectedValueChanged(object sender, EventArgs e)
        {
            ReportViewer1.Clear();
            ReportViewer1.Reset();
            ErpPaymentReport.Clear();

            EnableDisable();

            strCondition = "";
            int intCompany = Convert.ToInt32(CboCompany.SelectedValue);
            int intTransactionTypeType = Convert.ToInt32(CboTransactionType.SelectedValue);
            int intVendorID1 = Convert.ToInt32(CboCustomer.SelectedValue);//For Customer


            if (Convert.ToInt32(CboType.SelectedValue) == (int)OperationType.POS) //POS
            {
                strCondition = "";
                if (intCompany != 0) strCondition = "CompanyID = " + Convert.ToInt32(CboCompany.SelectedValue) + " and POS.POSID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ") AND";
                if (intVendorID1 != 0 & intVendorID1 != -1) strCondition = strCondition + " POS.VendorID = " + Convert.ToInt32(CboCustomer.SelectedValue) + " and POS.POSID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ") AND";
                if (intVendorID1 == -1) strCondition = strCondition + " isnull(POS.VendorID,0) = 0 " + " and POS.POSID In (Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ") AND";
                strCondition = strCondition + " POS.POSID In(Select RPD.ReferenceID from  AccReceiptAndPaymentMaster RPM Inner Join AccReceiptAndPaymentDetails  RPD on RPM.ReceiptAndPaymentID = RPD.ReceiptAndPaymentID and RPM.OperationTypeID =  " + (int)OperationType.POS + ")";

                LoadCombos(48);

                strCondition = "";
                if (intCompany != 0) strCondition = " RPM.CompanyID=" + intCompany + " AND";

                if (intVendorID1 != 0 & CboCustomer.Text != "General Customer") strCondition = strCondition + " POS.VendorID=" + intVendorID1 + " AND";
                if (CboCustomer.Text == "General Customer") strCondition = strCondition + " isnull(POS.VendorID,0) = 0" + " AND";
                strCondition = strCondition + " OperationTypeID= " + (int)OperationType.POS + "";

                LoadCombos(47);


            }
        }
    }
    }
