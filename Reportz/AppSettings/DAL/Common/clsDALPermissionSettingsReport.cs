﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace MyBooksERP
{
    public class clsDALPermissionSettingsReport
    {
        ArrayList prmPermissions;                                          // array list for storing parameters
        public DataLayer MobjDataLayer;                                  // obj of datalayer    
        private string strProcedurePermissionSettings = "spPermissionSettings";

        public clsDALPermissionSettingsReport(DataLayer objDataLayer)
        {
            //constructor
            MobjDataLayer = objDataLayer;
        }

        public void GetPermissions(int intRoleID,int intCompanyID,int intModuleID,int intMenuID,out bool blnPrintEmailPermission,out bool blnCreatePermission,out bool blnUpdatePermission,out bool blnDeletePermission)
        {
            blnCreatePermission = blnPrintEmailPermission = blnUpdatePermission = blnDeletePermission = false;
            prmPermissions = new ArrayList();
            prmPermissions.Add(new SqlParameter("@Mode",1));
            prmPermissions.Add(new SqlParameter("@RoleID", intRoleID));
            prmPermissions.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmPermissions.Add(new SqlParameter("@ModuleID",intModuleID));
            prmPermissions.Add(new SqlParameter("@MenuID",intMenuID));
            DataTable dtPermissions = new DataTable();
            dtPermissions = MobjDataLayer.ExecuteDataTable(strProcedurePermissionSettings, prmPermissions);
            if (dtPermissions.Rows.Count > 0)
            {
                    blnCreatePermission = Convert.ToBoolean(dtPermissions.Rows[0]["IsCreate"]);
                    blnPrintEmailPermission = Convert.ToBoolean(dtPermissions.Rows[0]["IsPrintEmail"]);
                    blnUpdatePermission = Convert.ToBoolean(dtPermissions.Rows[0]["IsUpdate"]);
                    blnDeletePermission = Convert.ToBoolean(dtPermissions.Rows[0]["IsDelete"]);
            }
        }

        public DataTable GetMenuPermissions(int intRoleID, int intCompanyID)
        {
            prmPermissions = new ArrayList();
            prmPermissions.Add(new SqlParameter("@Mode", 3));
            prmPermissions.Add(new SqlParameter("@RoleID", intRoleID));
            prmPermissions.Add(new SqlParameter("@CompanyID", intCompanyID));
            return MobjDataLayer.ExecuteDataTable(strProcedurePermissionSettings, prmPermissions); ;
        }
        public DataTable GetPermittedCompany(int intRoleID, int intCompanyID, int intControlID)// Only fro report by AMAL
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 13));
            prmReportDetails.Add(new SqlParameter("@RoleID", intRoleID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@ControlID", intControlID));
            return MobjDataLayer.ExecuteDataTable(strProcedurePermissionSettings, prmReportDetails);
        }

    }
}
