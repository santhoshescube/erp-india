﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,25 Feb 2011>
Description:	<Description,,DAL for ReportViewer>
================================================
*/
namespace MyBooksERP
{
    public class clsDALReportViewer : IDisposable
    {
        ArrayList prmReportDetails;
        DataTable dtpreportTable;

        public clsDTOReportviewer objclsDTOReportviewer { get; set; }
        public DataLayer objclsconnection { get; set; }
     
        

        
        public DataTable DisplayCompanyReport() // for company report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 17));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spCompany", prmReportDetails);
        }

        public DataTable DisplayCompanyBankReport() //company Bank Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 18));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spCompany", prmReportDetails);
        }

        public DataTable DisplayBankReport()  //Bank Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@BankBranchID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spBankDetails",prmReportDetails);
        }

        public DataTable DisplayEmployeeReport()  //Employee  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@EmployeeID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spEmployee", prmReportDetails);
        }

        public DataTable DisplayVendorReport()  //Vendor  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 12));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }

        public DataTable DisplayVendorAddressReport() //VendorAddress  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",13));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplayCustomerSalesReport()  //Customer  Sales History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 34));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplayCustomerPaymentReport()  //Customer  Payment History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 35));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplayCustomerItemsReport()  //Customer  Sales item History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 36));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplaySupplierPurchaseReport()  //Supplier Purchase History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 37));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplaySupplierPaymentReport()  //Supplier Payment History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 38));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplaySupplierItemsReport()  //Supplier Items History
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 39));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }
        public DataTable DisplaySupplierNearestNeighbourReport()  // Nearest Neighbour Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 30));
            prmReportDetails.Add(new SqlParameter("@VendorID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvVendorInformation", prmReportDetails);
        }

        public DataTable DisplayWareHouseReport() //WareHouse Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 15));
            prmReportDetails.Add(new SqlParameter("@WarehouseID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvWarehouseMasterTransactions", prmReportDetails);
        }

        public DataTable DisplayWareHouseDetailsReport() //WareHouseDetails  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 16));
            prmReportDetails.Add(new SqlParameter("@WarehouseID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvWarehouseMasterTransactions", prmReportDetails);
        }
        public DataSet DisplayCompanySettingsReport() //  CompanySettings Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 12));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spCompanySettings", prmReportDetails);
        }
        public DataSet DisplayRoleSettingsReport() //  CompanySettings Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 3));
            prmReportDetails.Add(new SqlParameter("@RoleID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("STRoleDetailsTransaction", prmReportDetails);
        }

        public DataSet DisplayProductReport() //  Product Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@ItemID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvItemMaster", prmReportDetails);
        }

        public DataSet DisplayAlertSettingsReport() //  AlertSetting Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 11));
            prmReportDetails.Add(new SqlParameter("@AlertSettingID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAlertSetting", prmReportDetails);
        }

        public DataTable DisplayUOMReport() //UOM  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@UOMTypeID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvUnitOfMeasurement", prmReportDetails);
        }

        public DataTable DisplayPaymentTermsReport() //UOM  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 5));
            return objclsconnection.ExecuteDataTable("STPaymentTermsTransaction", prmReportDetails);
        }

        public DataTable DisplaySTDiscountTypeReferenceReport() //STDiscountTypeReference Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",14));
            prmReportDetails.Add(new SqlParameter("@DiscountTypeID",objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvDiscounts", prmReportDetails);
        }

        public DataTable DisplaySTDiscountItemWiseDetailsReport() //STDiscountItemWiseDetails  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",15));
            prmReportDetails.Add(new SqlParameter("@DiscountTypeID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvDiscounts", prmReportDetails);
        }

        public DataTable DisplaySTDiscountCustomerWiseDetailsReport() //STDiscountCustomerWiseDetails  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",16));
            prmReportDetails.Add(new SqlParameter("@DiscountTypeID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spInvDiscounts", prmReportDetails);
        }

        public DataSet DisplayPaymentDetailsReport() //Payment Details Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 22));
            prmReportDetails.Add(new SqlParameter("@ReceiptAndPaymentID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccReceiptsAndPayments", prmReportDetails);
        }

        public DataSet DisplayPaymentDetailsReportForAlMajdal() //Payment Details Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 33));
            prmReportDetails.Add(new SqlParameter("@ReceiptAndPaymentID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccReceiptsAndPayments", prmReportDetails);
        }

        public DataSet DisplayExchangeCurrencyReport() //Exchange currency Details Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spExchangeCurrency", prmReportDetails);
        }

        public DataSet DisplayDeliveryNoteReport() //Delivery Note Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 15));
            //prmReportDetails.Add(new SqlParameter("@Mode", 25));
            prmReportDetails.Add(new SqlParameter("@ItemIssueID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvItemIssue", prmReportDetails);
        }

        public DataSet DisplayCustomerWiseDeliveryNoteReport()//Customerwise Delivery Note report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 25));
            prmReportDetails.Add(new SqlParameter("@ItemIssueID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvItemIssue", prmReportDetails);

        }

        public DataSet DisplayMaterialIssueReport() //Material Issue Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 11));
            prmReportDetails.Add(new SqlParameter("@MaterialIssueID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvMaterialIssue", prmReportDetails);
        }

        public DataSet DisplayMaterialReturnReport() //Material Return Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 11));
            prmReportDetails.Add(new SqlParameter("@MaterialReturnID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvMaterialReturn", prmReportDetails);
        }
  
        public DataSet DisplayExpenseReport() //Delivery Expense Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@ExpenseID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvExpense", prmReportDetails);
        }

        public DataSet DisplayItemGroupReport() //Item Group Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",6));
            prmReportDetails.Add(new SqlParameter("@ItemGroupID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvItemGroupMaster", prmReportDetails);
        }


        public DataSet VacationProcessReport() // for Vacation Process Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 9));
            prmReportDetails.Add(new SqlParameter("@VacationID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayVacationProcessTransactions", prmReportDetails);
        }

        public DataSet DisplayPurchaseDetailsReport(int intFormType)
        {
            // 1 - PurchaseIndent, 2 - PurchaseQuotation, 3 - PurchaseOrder, 4 - GRN, 5 - PurchaseInvoice
            switch (intFormType)
            {
                case 1:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 19));
                    prmReportDetails.Add(new SqlParameter("@PurchaseIndentID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("InvPurchaseIndent", prmReportDetails);
                case 2:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 19));
                    prmReportDetails.Add(new SqlParameter("@PurchaseQuotationID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvPurchaseQuotation", prmReportDetails);
                case 3:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 20));
                    prmReportDetails.Add(new SqlParameter("@PurchaseOrderID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spPurchaseOrder", prmReportDetails);
                case 4:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 18));
                    prmReportDetails.Add(new SqlParameter("@GRNID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spGRN", prmReportDetails);
                case 5:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 18));
                    prmReportDetails.Add(new SqlParameter("@PurchaseInvoiceID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spPurchaseInvoice", prmReportDetails);
                case 6:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 20));
                    prmReportDetails.Add(new SqlParameter("@GRNID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvDirectGRN", prmReportDetails);
                default:
                    return null;
            }
        }
        public DataSet DisplayPILocationReport()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 29));
            prmReportDetails.Add(new SqlParameter("@PurchaseInvoiceID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvPurchaseInvoice", prmReportDetails);
        }
        public DataSet DisplayGRNLocationReport()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 23));
            prmReportDetails.Add(new SqlParameter("@GRNID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvGRN", prmReportDetails);
        }
        public DataTable DisplaySalesInvoice1Report() //sales invoice detail report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 23));
            prmReportDetails.Add(new SqlParameter("@TermsID", 1));
            prmReportDetails.Add(new SqlParameter("@OperationTypeID", 9));
            return objclsconnection.ExecuteDataTable("spInvSalesInvoice", prmReportDetails);
        }

        public DataSet DisplaySalesDetailReport(int intFormType)
        {
            switch (intFormType)
            {
                case 1:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 16));
                    prmReportDetails.Add(new SqlParameter("@SalesQuotationID ", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvSalesQuotation", prmReportDetails);
                case 2:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 18));
                    prmReportDetails.Add(new SqlParameter("@SalesOrderID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvSalesOrder", prmReportDetails);
                case 3:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode",objclsDTOReportviewer.IsExpenseListInSales ? 19 : 88 ));
                    prmReportDetails.Add(new SqlParameter("@SalesInvoiceID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvSalesInvoice", prmReportDetails);
                case 4:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 19));
                    prmReportDetails.Add(new SqlParameter("@SalesReturnID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvSalesReturn", prmReportDetails);
                case 5:
                    prmReportDetails = new ArrayList();
                    prmReportDetails.Add(new SqlParameter("@Mode", 7));
                    prmReportDetails.Add(new SqlParameter("@InvoiceID", objclsDTOReportviewer.intRecId));
                    return objclsconnection.ExecuteDataSet("spInvCommercialInvoice", prmReportDetails);
                default:
                    return null;
            }
        }

        public DataSet DisplayCompanyReportHeader(int intCompanyID) //CompnayReportHeader
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objclsconnection.ExecuteDataSet("spInvPurchaseModuleFunctions", prmReportDetails);
        }

        public DataTable DisplayCompanyDefaultHeader() //Company Default Header
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 22));
            return objclsconnection.ExecuteDataTable("spCompany", prmReportDetails);
        }
        public DataSet DisplayOpeningStockReport() //Opening Stock Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 9));
            prmReportDetails.Add(new SqlParameter("@OpeningStockID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvOpeningStock", prmReportDetails);
        }      
       
        public DataSet DisplayAccountSettingsReport() //Account Settings Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccSettings", prmReportDetails);
        }
        public DataSet DisplayRecurrentSetupReport() //Account RecurrentSetup Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 9));
            prmReportDetails.Add(new SqlParameter("@RecurrenceSetupID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccRecurranceSetup", prmReportDetails);
        }

        public DataSet DisplayRFQReport() //RFQ  Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 19));
            prmReportDetails.Add(new SqlParameter("@RFQID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvRFQ", prmReportDetails);
        }

        public DataSet DisplayUOMConversionReport(int intUomClassID, int intUomBaseID) // UOM Conversion
        {
            prmReportDetails = new ArrayList();
            if(intUomClassID == 0)
                prmReportDetails.Add(new SqlParameter("@Mode", 4));
            else if (intUomClassID > 0 && intUomBaseID == 0)
                prmReportDetails.Add(new SqlParameter("@Mode", 6));
            else if (intUomClassID > 0 && intUomBaseID > 0)
                prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@UOMTypeID", intUomClassID));
            prmReportDetails.Add(new SqlParameter("@UOMID", intUomBaseID));
            return objclsconnection.ExecuteDataSet("spInvUomConversion", prmReportDetails);
        }
        public DataSet DisplayStockAdjustmentReport() //Stock Adjustment Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "10"));
            prmReportDetails.Add(new SqlParameter("@StockAdjustmentID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvStockAdjustment", prmReportDetails);
        }

        public DataTable DisplayTermsAndConditionReport(int operation, int intCompanyID) // for all TermsAndConditionreport
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@OperationTypeID", operation));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objclsconnection.ExecuteDataTable("spTermsAndConditions", prmReportDetails);

        }
        public DataSet DisplayGeneralReceiptsAndPayements()// Display report for Jounrnal Voucher
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 18));
            prmReportDetails.Add(new SqlParameter("@VoucherID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccJournalVoucher", prmReportDetails);
        }

        public DataSet DisplayGroupJV()// Display report for Group JV
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 12));
            prmReportDetails.Add(new SqlParameter("@GroupJVID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccGroupJV", prmReportDetails);
        }

        public DataSet DisplayAccountsOpeningBalance()// Display report for OpeningBalance
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            //prmReportDetails.Add(new SqlParameter("@AccountID", objclsDTOReportviewer.intRecId));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intCompany));
            return objclsconnection.ExecuteDataSet("spAccOpeningBalance", prmReportDetails);
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsconnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }


        public DataSet DisplayProjectReport() //Project Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 10));
            prmReportDetails.Add(new SqlParameter("@ProjectID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvProjects", prmReportDetails);
        }

        public DataSet DisplayStockTransfer() //Project Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 16));
            prmReportDetails.Add(new SqlParameter("@StockTransferID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvStockTransfer", prmReportDetails);
        }
        public DataSet DisplayDebitNoteReport()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 23));
            prmReportDetails.Add(new SqlParameter("@PurchaseReturnID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvPurchaseReturn", prmReportDetails);
        }

        public DataSet DisplayDebitNoteLocationReport()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 24));
            prmReportDetails.Add(new SqlParameter("@PurchaseReturnID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvPurchaseReturn", prmReportDetails);
        }

        public DataSet DisplayItemIssueLocationReport()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 29));
            prmReportDetails.Add(new SqlParameter("@ItemIssueID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvItemIssue", prmReportDetails);
        }
        public DataSet DisplayPricingSchemeReport() //Delivery Expense Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 14));
            prmReportDetails.Add(new SqlParameter("@RowNumber", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("[spInvPricingScheme]", prmReportDetails);
        }

        public DataSet DisplayPOSReport() // POS Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 17));
            prmReportDetails.Add(new SqlParameter("@POSID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spInvPOS", prmReportDetails);

        }
        public DataSet DisplayLeavePolicy() //Leave Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 20));
            prmReportDetails.Add(new SqlParameter("@LeavePolicyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayLeavePolicy", prmReportDetails);

        }

        public DataSet DisplayShiftPolicy() //Shift Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 13));
            prmReportDetails.Add(new SqlParameter("@ShiftID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayShiftPolicy", prmReportDetails);

        }
        public DataSet DisplayWorkPolicy() //Work Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "WP"));
            prmReportDetails.Add(new SqlParameter("@PolicyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayWorkPolicy", prmReportDetails);

        }

        public DataSet DisplayOvertimePolicy() //OvertimePolicy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "11"));
            prmReportDetails.Add(new SqlParameter("@OTPolicyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayOvertimePolicy", prmReportDetails);

        }
        public DataSet DisplayReceiptIssue() //OvertimePolicy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "5"));
            prmReportDetails.Add(new SqlParameter("@DocumentID", objclsDTOReportviewer.intRecId));
            prmReportDetails.Add(new SqlParameter("@StatusID", objclsDTOReportviewer.intStatusID));
            return objclsconnection.ExecuteDataSet("spDocuments", prmReportDetails);

        }
        public DataSet DisplayAccRecurringJournalSetup() //AccRecurringJournalSetup
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "6"));
            prmReportDetails.Add(new SqlParameter("@RecurringJournalSetupID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccJournalRecurrenceSetup", prmReportDetails);

        }

        public DataSet DisplaySalaryStructure() //OvertimePolicy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "19"));
            prmReportDetails.Add(new SqlParameter("@SalaryStructureID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPaySalaryStructure", prmReportDetails);

        }


        public DataSet DisplaySalaryRelease() //SalaryRelease Form
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@ProcessDate1", objclsDTOReportviewer.strDate));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intCompany));
            return objclsconnection.ExecuteDataSet("spPayEmployeePaymentDetailRelease", prmReportDetails);

        }

        public DataSet DisplayCompanyHeader()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intCompany));
            return objclsconnection.ExecuteDataSet("spInvPurchaseModuleFunctions", prmReportDetails);
        }

        public DataSet GetJobOrderDetails()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 8));
            prmReportDetails.Add(new SqlParameter("@JobOrderID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPrdCompanywiseProduction", prmReportDetails);
        }
        public DataSet PrintExtraCharges(long ExtraChargeID)
        {
            return new DataLayer().ExecuteDataSet("spExtraCharges", new List<SqlParameter>{
                new SqlParameter("@Mode",15),
                new SqlParameter("@ExtraChargeID",ExtraChargeID)
            });
        }
        public DataSet DisplayExtraChargePayment(long ExtraChargePaymentID)
        {
            return new DataLayer().ExecuteDataSet("spExtraChargePayment", new List<SqlParameter>{
                new SqlParameter("@Mode",20),
                new SqlParameter("@ExtraChargePaymentID",ExtraChargePaymentID)
            });

        }


        public static DataTable GetAllCompanies()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 1));
            // prmReportDetails.Add(new SqlParameter("@CompanyID", 1));
            prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }
        public static DataTable GetAllCompaniesMulti()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 1));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }
        public static DataTable GetAllBranchesByCompany(int CompanyID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 2));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

        }

        public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany)
        {
            return LoadEmployees(CompanyID, BranchID, IncludeCompany, null, null, null, null, null, null, null, false, null, null);

        }

        public static DataTable GetEmployeesByLocation(int CompanyID, int BranchID, bool IncludeCompany, int WorkLocationID)
        {
            return LoadEmployees(CompanyID, BranchID, IncludeCompany, null, null, null, null, WorkLocationID, null, null, false, null, null);
        }

        public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DocumentTypeID, string DocumentNumber)
        {

            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 16));
            prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));

            prmReportDetails.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            if (DocumentNumber.Trim().ToUpper() == "-1")
            {
            }
            else
                prmReportDetails.Add(new SqlParameter("@DocumentNumber", DocumentNumber));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
            prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));


            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

        }

        public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID)
        {
            return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, null, null, null, null, null, null, false, null, null);
        }



        public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, bool OnlySettledEmployees)
        {
            return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, null, null, null, null, null, OnlySettledEmployees, null, null);
        }


        public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID)
        {
            return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, null, null, null, null, null, false, null, null);
        }


        public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID)
        {
            return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, null, null, null, null, false, null, null);
        }

        public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID)
        {
            return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID, null, null, null, false, null, null);
        }

        public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID, int WorkLocationID)
        {
            return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID, WorkLocationID, null, null, false, null, null);
        }

        public static DataTable GetAllEmployeesArb(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID, int WorkLocationID)
        {
            return LoadEmployeesArb(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID, WorkLocationID, null, null, false);
        }

        private static DataTable LoadEmployees(int CompanyID, int BranchID, bool IncludeCompany, int? DepartmentID, int? DesignationID, int? EmploymentTypeID, int? WorkStatusID, int? WorkLocationID, int? DocumentTypeID, string DocumentNumber, bool LoadOnlySettlementEmployees, int? CountryID, int? ReligionID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 3));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
            prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            if (DepartmentID != null && DepartmentID > 0)
                prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
            if (DesignationID != null && DesignationID > 0)
                prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
            if (EmploymentTypeID != null && EmploymentTypeID > 0)
                prmReportDetails.Add(new SqlParameter("@EmploymentTypeID", EmploymentTypeID));
            if (WorkStatusID != null && WorkStatusID > 0)
                prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));

            if (WorkLocationID != null && WorkLocationID > 0)
                prmReportDetails.Add(new SqlParameter("@WorkLocationID", WorkLocationID));

            if (DocumentTypeID != null && DocumentTypeID > 0)
                prmReportDetails.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));


            if (DocumentNumber != null && DocumentNumber.ToInt32() > 0)
                prmReportDetails.Add(new SqlParameter("@DocumentNumber", DocumentNumber));

            if (CountryID != null && CountryID.ToInt32() > 0)
                prmReportDetails.Add(new SqlParameter("@CountryID", CountryID));

            if (ReligionID != null && ReligionID.ToInt32() > 0)
                prmReportDetails.Add(new SqlParameter("@ReligionID", ReligionID));

            if (LoadOnlySettlementEmployees == true)
            {
                prmReportDetails.Add(new SqlParameter("@LoadSettledEmployees", LoadOnlySettlementEmployees));
            }

            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }

        private static DataTable LoadEmployeesArb(int CompanyID, int BranchID, bool IncludeCompany, int? DepartmentID, int? DesignationID, int? EmploymentTypeID, int? WorkStatusID, int? WorkLocationID, int? DocumentTypeID, string DocumentNumber, bool LoadOnlySettlementEmployees)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 39));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));


            prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));

            if (DepartmentID != null && DepartmentID > 0)
                prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
            if (DesignationID != null && DesignationID > 0)
                prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
            if (EmploymentTypeID != null && EmploymentTypeID > 0)
                prmReportDetails.Add(new SqlParameter("@EmploymentTypeID", EmploymentTypeID));
            if (WorkStatusID != null && WorkStatusID > 0)
                prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));

            if (WorkLocationID != null && WorkLocationID > 0)
                prmReportDetails.Add(new SqlParameter("@WorkLocationID", WorkLocationID));

            if (DocumentTypeID != null && DocumentTypeID > 0)
                prmReportDetails.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));


            if (DocumentNumber != null && DocumentNumber.ToInt32() > 0)
                prmReportDetails.Add(new SqlParameter("@DocumentNumber", DocumentNumber));


            if (LoadOnlySettlementEmployees == true)
            {
                prmReportDetails.Add(new SqlParameter("@LoadSettledEmployees", LoadOnlySettlementEmployees));
            }

            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }


        public static DataTable GetAllDepartments()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 5));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }

        public static DataTable GetAllDesignation()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

        }

        public static DataTable GetAllEmploymentTypes()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }

        public static DataTable GetAllWorkStatus()
        {
            ArrayList prmReportDetails = new ArrayList();

            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }

        public static DataTable GetReligion()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 48));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }
        public static DataTable GetCountry()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 47));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }
        public static DataTable GetNationality()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 49));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }
        public static DataTable GetAllWorkLocations(int CompanyID, int BranchID, bool IncludeCompany)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 8));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
            prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }

        public static DataTable GetCompanyHeader(int CompanyID, int BranchID, bool IncludeCompany)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 9));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
            prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }


        public static DataTable GetAllDocumentTypes()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 10));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }

        public static DataTable GetAllDocumentNumbers(int CompanyID, int BranchID, bool IncludeCompany, int DocumentTypeID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 11));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
            prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            prmReportDetails.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }

        public static DataTable GetAllDeductions()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 12));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }

        public static DataTable GetAllAssetTypes()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 13));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }
        public static DataTable GetAllAssets(int AssetTypeID, int CompanyID, int BranchID, bool IncludeCompany)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 14));
            prmReportDetails.Add(new SqlParameter("@BenefitTypeID", AssetTypeID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
            prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }

        public static DataTable GetAllFinancialYears(int intCompanyID, int intBranchID, bool blnIncludeCompany)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 32));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@BranchID", intBranchID));
            prmReportDetails.Add(new SqlParameter("@IncludeCompany", blnIncludeCompany));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

        }

        public static DataTable GetPaymentMonthYear(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 17));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
            prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
            prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
            prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
            prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

        }
        public static DataTable GetAllExpenseTypes()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 18));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }
        public static DataTable GetAllSettlementTypes()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 20));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

        }

        public static DataTable GetMonthForAttendance(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
            prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
            prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
            prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
            prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }
        public static DataTable GetAllAssetStatus(int AssetSearchType)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 33));
            prmReportDetails.Add(new SqlParameter("@Type", AssetSearchType));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

        }
        public static DataTable GetCompaniesandBranches()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 34));
            prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

        }
        public static DataTable GetEmployeeForTransfer()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 35));
            prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }
        public static bool IsCompanyPermissionExists(int CompanyID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 50));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteScalar("SpReportsCommon", prmReportDetails).ToInt32() > 0;
        }
        //Without transfer join fill employees in current working company
        private static DataTable LoadEmployeesIncurrentCompany(int CompanyID, int BranchID, bool IncludeCompany, int? DepartmentID, int? DesignationID, int? EmploymentTypeID, int? WorkStatusID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 36));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
            prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));

            if (DepartmentID != null && DepartmentID > 0)
                prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
            if (DesignationID != null && DesignationID > 0)
                prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
            if (EmploymentTypeID != null && EmploymentTypeID > 0)
                prmReportDetails.Add(new SqlParameter("@EmploymentTypeID", EmploymentTypeID));
            if (WorkStatusID != null && WorkStatusID > 0)
                prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));



            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
        }

        public static DataTable GetAllEmployeesInCurrentCompany(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID)
        {
            return LoadEmployeesIncurrentCompany(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID);
        }
        public static DataTable GetTransactionType()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 40));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

        }
        public static DataTable GetProject()
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 41));
            return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

        }


        public DataSet DisplayShiftScheduleDetails()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 11));
            prmReportDetails.Add(new SqlParameter("@ShiftScheduleID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayShiftScheduleTransaction", prmReportDetails);
        }

        public static DataTable GetDocumentReceiptIssueDetails(int OperationTypeID, int DocumentTypeID, int DocumentID, int OrderNo)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 23));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
            prmDocuments.Add(new SqlParameter("@OrderNo", OrderNo));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }


        public static DataTable GetOtherDocumentDetails(int CurrentIndex, int DocumentTypeID, int DocumentID, int OperationTypeID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 24));
            prmDocuments.Add(new SqlParameter("@RowIndex", CurrentIndex));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }

        public DataSet DisplayLeaveStructure(int intEmployeeID, int intCompanyID, string date1, string CurrentFinYear)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 37));
            prmReportDetails.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@Date1", Convert.ToDateTime(date1)));
            prmReportDetails.Add(new SqlParameter("@CurrentFinYear", CurrentFinYear));
            return objclsconnection.ExecuteDataSet("spPayEmployeeLeave", prmReportDetails);
        }


        public DataTable DisplayWorkLocation()  //Work Location
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@WorkLocationID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayWorkLocation", prmReportDetails);
        }

        public DataSet DisplayocationScheduleReport()  //Location Schedule
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 12));
            prmReportDetails.Add(new SqlParameter("@ScheduleID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayLocationSchedule", prmReportDetails);
        }
        public DataSet DisplayDeductionPolicy() //Work Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@AdditionDeductionPolicyId", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayDeductionPolicyTransaction", prmReportDetails);

        }
        public DataSet DisplayEmployeeLeaveEntry()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 36));
            prmReportDetails.Add(new SqlParameter("@LeaveID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayEmployeeLeave", prmReportDetails);
        }

        public DataSet DisplayPFPolicy() //PF Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@AdditionDeductionPolicyId", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayPFPolicyTransaction", prmReportDetails);
        }
        public DataTable DisplayCompanyAssetReport()  //Comapny Assets  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@BenefitTypeID", objclsDTOReportviewer.intRecId));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intCompany));
            return objclsconnection.ExecuteDataTable("PayCompanyAssets", prmReportDetails);
        }
        public DataTable DisplayVisaReport()  //Visa Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "EVR"));
            prmReportDetails.Add(new SqlParameter("@VisaID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spDocumentReport", prmReportDetails);
        }
        public DataTable DisplayEmirateHealthReport()  //EmirateHealth Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "6"));
            prmReportDetails.Add(new SqlParameter("@CardID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("MvfEmployeeEmiratesCardTransactions", prmReportDetails);
        }
        public DataTable DisplaHealthCardReport()  //EmirateHealth Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "9"));
            prmReportDetails.Add(new SqlParameter("@CardID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("MvfEmployeeHealthCardTransactions", prmReportDetails);
        }
        public DataTable QualificationDetails()
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 8));
            prmReportDetails.Add(new SqlParameter("@QualificationID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayQualification", prmReportDetails);
        }
        public static DataTable GetEmployeeLicenseReport(int LicenseID)
        {
            ArrayList prmReportDetails = new ArrayList();

            prmReportDetails.Add(new SqlParameter("@Mode", "EDR"));
            prmReportDetails.Add(new SqlParameter("@LicenseID", LicenseID));

            return new clsConnection().ExecuteDataTable("spDocumentReport", prmReportDetails);
        }


        public DataTable EmpSettlementDetails() //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@Mode", 36));
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayEmployeeSettlementTransaction", prmEmpSettlement);

        }

        public DataSet EmpGratuityReport() //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", objclsDTOReportviewer.intRecId));
            prmEmpSettlement.Add(new SqlParameter("@IsArabicView", 0));
            return objclsconnection.ExecuteDataSet("spPayGratuityReport", prmEmpSettlement);

        }

        public static DataTable EmpSettlementDetails(int SettlementID) //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@Mode", 36));
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", SettlementID));
            return new DataLayer().ExecuteDataTable("spPayEmployeeSettlementTransaction", prmEmpSettlement);

        }


        public DataTable EmpSettlement() //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@Mode", 37));
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayEmployeeSettlementTransaction", prmEmpSettlement);

        }


        public static DataTable EmpSettlement(int SettlementID) //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@Mode", 37));
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", SettlementID));
            return new DataLayer().ExecuteDataTable("spPayEmployeeSettlementTransaction", prmEmpSettlement);

        }

        public DataSet GetEmployeeLoanReport()
        {
            ArrayList prmEmployeeLoan = new ArrayList();
            prmEmployeeLoan.Add(new SqlParameter("@Mode", 8));
            prmEmployeeLoan.Add(new SqlParameter("@LoanID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayEmployeeLoan", prmEmployeeLoan);
        }
        public DataTable DisplayInsuranceCardReport()  //Healthcard Report
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@InsuranceCardID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayInsuranceCard", parameters);
        }

        public DataTable DisplayInsuranceDetailsReport()  //Insurance details Report
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@InsuranceID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayInsuranceDetails", parameters);
        }
        public DataTable DisplayLabourCardReport()  //EmirateHealth Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 8));
            prmReportDetails.Add(new SqlParameter("@LabourCardID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spEmployeeLabourCard", prmReportDetails);
        }

        public DataSet GetLeaveRequestDetials()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@LeaveRequestID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayLeaveRequest", parameters);
        }
        public DataSet GetAlerts()
        {
            ArrayList prmAlertreport = new ArrayList();
            prmAlertreport.Add(new SqlParameter("@Mode", 5));
            prmAlertreport.Add(new SqlParameter("@SearchKey", objclsDTOReportviewer.strSearchKey));
            prmAlertreport.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            prmAlertreport.Add(new SqlParameter("@FromDate", objclsDTOReportviewer.strFromDate));
            prmAlertreport.Add(new SqlParameter("@ToDate", objclsDTOReportviewer.strToDate));
            prmAlertreport.Add(new SqlParameter("@AlertTypeID", objclsDTOReportviewer.intAlertType));


            return objclsconnection.ExecuteDataSet("spAlertReport", prmAlertreport);
        }
        public DataTable GetPrintExpenseDetails()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@EmployeeExpenseID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayEmployeeExpense", parameters);
        }

        public DataTable GetPrintDepositDetails()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@DepositID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayDeposit", parameters);
        }

        public DataSet GetPrintLoanRepaymentDetails()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@RepaymentID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayLoanRepayment", parameters);
        }

        public DataTable GetPrintSalaryAdvanceDetails()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@SalaryAdvanceID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPaySalaryAdvance", parameters);
        }

        #region DisplayLeaveExtension
        public DataTable DisplayLeaveExtension(int EmployeeID, int CompanyID)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "ShowReport"));
            prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            return objclsconnection.ExecuteDataTable("spEmployeeLeaveExtension", prmReportDetails);
        }
        #endregion DisplayLeaveExtension

        public static DataTable DisplayTradeLicenseReport(long TradeLicenseID)  //Tradelicense report
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@TradeLicenseID", TradeLicenseID));
            return new clsConnection().ExecuteDataTable("spPayTradeLicense", parameters);
        }

        public static DataTable DisplayLeaseAgreementReport(long LeaseAgreementID)  //Tradelicense report
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@LeaseID", LeaseAgreementID));
            return new clsConnection().ExecuteDataTable("spPayLeaseAgreement", parameters);
        }

        public DataSet DisplayAssetHandoverDetails()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 14));
            prmReportDetails.Add(new SqlParameter("@EmpBenefitID", objclsDTOReportviewer.intRecId));
            prmReportDetails.Add(new SqlParameter("@IsArabicView", 0));
            return objclsconnection.ExecuteDataSet("spPayAssetHandover", prmReportDetails);
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (prmReportDetails != null)
                prmReportDetails = null;

            if (dtpreportTable != null)
                dtpreportTable.Dispose();


        }

        #endregion

    }
}
