﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyBooksERP
{
    public class clsDALAccountSummaryReport
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOSummaryReport objDTOAccountSummary { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        
        public clsDALAccountSummaryReport(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }
        public DataSet DisplayAccountsSummaryReoprt()
        {
            string strProc = "spAccRptAccounts";
            parameters = new ArrayList();

            if (this.objDTOAccountSummary.intTempMenuID  == (int)eMenuID.GeneralLedger)
                //parameters.Add(new SqlParameter("@Mode", 1));
                strProc = "spAccRptGeneralLedger";
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.DayBook)
                parameters.Add(new SqlParameter("@Mode", 2));
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.CashBook)
                parameters.Add(new SqlParameter("@Mode", 3));
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.AccountsSummary)
                parameters.Add(new SqlParameter("@Mode", 4));
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.TrialBalance)
                parameters.Add(new SqlParameter("@Mode", 5));
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.BalanceSheet)
                parameters.Add(new SqlParameter("@Mode", 7));
            else if (this.objDTOAccountSummary.intTempMenuID == (int)eMenuID.ProfitAndLossAccount)
                parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@AccountID", this.objDTOAccountSummary.intTempAccountID ));
            parameters.Add(new SqlParameter("@CompanyID", this.objDTOAccountSummary.intTempCompanyID ));
            parameters.Add(new SqlParameter("@FromDate", this.objDTOAccountSummary.dtpTempFromDate));
            parameters.Add(new SqlParameter("@ToDate", this.objDTOAccountSummary.dtpTempToDate));
            parameters.Add(new SqlParameter("@IsGroup",this.objDTOAccountSummary.IsTempGroup ));
            return MobjDataLayer.ExecuteDataSet(strProc, parameters);
        }

        public DataSet DisplayCompanyHeader()
        {
            try
            {
                using (MobjDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 31));
                    parameters.Add(new SqlParameter("@CompanyID", this.objDTOAccountSummary.intTempCompanyID));
                    return MobjDataLayer.ExecuteDataSet("spInvPurchaseModuleFunctions", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }   
    }
}
