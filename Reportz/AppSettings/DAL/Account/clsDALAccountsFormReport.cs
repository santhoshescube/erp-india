﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyBooksERP
{
    /***********************************************************
    * Author       : Junaid
    * Created On   : 13 APRL 2012
    * Purpose      : Create Opening Balance for Accounts
    *                
    * **********************************************************/
    public class clsDALAccountsFormReport
    {
        public DataLayer objDataLayer { get; set; }
        public clsDTOAccountsFormReport objclsDTOAccountsFormReport { get; set; }
        ArrayList parameters;
        public DataTable FillCombos(string[] saFields)
        {
            try
            {
                if (saFields.Length == 3)
                {
                    using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                    {
                        objCommonUtility.PobjDataLayer = this.objDataLayer;
                        return objCommonUtility.FillCombos(saFields);
                    }
                }
                return null;
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayBalanceSheet()
        {
            try
            {
                using (objDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 7));
                    parameters.Add(new SqlParameter("@CompanyID", this.objclsDTOAccountsFormReport.intTempCompanyID));
                    parameters.Add(new SqlParameter("@FromDate", this.objclsDTOAccountsFormReport.dtpTempFromDate));
                    parameters.Add(new SqlParameter("@ToDate", this.objclsDTOAccountsFormReport.dtpTempToDate));
                    return objDataLayer.ExecuteDataSet("spAccRptAccounts", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayCompanyHeader()
        {
            try
            {
                using (objDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 31));
                    parameters.Add(new SqlParameter("@CompanyID", this.objclsDTOAccountsFormReport.intTempCompanyID));
                    return objDataLayer.ExecuteDataSet("spInvPurchaseModuleFunctions", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayProfitAndLossAccounts()
        {
            try
            {
                using (objDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 8));
                    parameters.Add(new SqlParameter("@CompanyID", this.objclsDTOAccountsFormReport.intTempCompanyID));
                    parameters.Add(new SqlParameter("@FromDate", this.objclsDTOAccountsFormReport.dtpTempFromDate));
                    parameters.Add(new SqlParameter("@ToDate", this.objclsDTOAccountsFormReport.dtpTempToDate));
                    return objDataLayer.ExecuteDataSet("spAccRptAccounts", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayIncomeStatement()
        {
            try
            {
                using (objDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 12));
                    parameters.Add(new SqlParameter("@CompanyID", this.objclsDTOAccountsFormReport.intTempCompanyID));
                    parameters.Add(new SqlParameter("@FromDate", this.objclsDTOAccountsFormReport.dtpTempFromDate));
                    parameters.Add(new SqlParameter("@ToDate", this.objclsDTOAccountsFormReport.dtpTempToDate));
                    return objDataLayer.ExecuteDataSet("spAccRptAccounts", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayCashFlow()
        {
            try
            {
                using (objDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 10));
                    parameters.Add(new SqlParameter("@CompanyID", this.objclsDTOAccountsFormReport.intTempCompanyID));
                    parameters.Add(new SqlParameter("@AccountID", 2));
                    parameters.Add(new SqlParameter("@FromDate", this.objclsDTOAccountsFormReport.dtpTempFromDate));
                    parameters.Add(new SqlParameter("@ToDate", this.objclsDTOAccountsFormReport.dtpTempToDate));
                    return objDataLayer.ExecuteDataSet("spAccRptAccounts", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayFundFlow()
        {
            try
            {
                using (objDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 11));
                    parameters.Add(new SqlParameter("@CompanyID", this.objclsDTOAccountsFormReport.intTempCompanyID));
                    parameters.Add(new SqlParameter("@FromDate", this.objclsDTOAccountsFormReport.dtpTempFromDate));
                    parameters.Add(new SqlParameter("@ToDate", this.objclsDTOAccountsFormReport.dtpTempToDate));
                    return objDataLayer.ExecuteDataSet("spAccRptAccounts", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayStockSummary()
        {
            try
            {
                using (objDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 9));
                    parameters.Add(new SqlParameter("@CompanyID", this.objclsDTOAccountsFormReport.intTempCompanyID));
                    parameters.Add(new SqlParameter("@FromDate", this.objclsDTOAccountsFormReport.dtpTempFromDate));
                    parameters.Add(new SqlParameter("@ToDate", this.objclsDTOAccountsFormReport.dtpTempToDate));
                    return objDataLayer.ExecuteDataSet("spAccRptAccounts", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public DataSet DisplayStatementofAccounts()
        {
            try
            {
                using (objDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 18));
                    parameters.Add(new SqlParameter("@CompanyID", this.objclsDTOAccountsFormReport.intTempCompanyID));
                    parameters.Add(new SqlParameter("@FromDate", this.objclsDTOAccountsFormReport.dtpTempFromDate));
                    parameters.Add(new SqlParameter("@ToDate", this.objclsDTOAccountsFormReport.dtpTempToDate));
                    parameters.Add(new SqlParameter("@AccountID", this.objclsDTOAccountsFormReport.intTempAccountID));
                    parameters.Add(new SqlParameter("@IsGroup", false));
                    return objDataLayer.ExecuteDataSet("spAccRptAccounts", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public DataSet DisplayStockSummaryQtywise()
        {
            try
            {
                using (objDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 19));
                    parameters.Add(new SqlParameter("@CompanyID", this.objclsDTOAccountsFormReport.intTempCompanyID));
                    parameters.Add(new SqlParameter("@FromDate", this.objclsDTOAccountsFormReport.dtpTempFromDate));
                    parameters.Add(new SqlParameter("@ToDate", this.objclsDTOAccountsFormReport.dtpTempToDate));
                    parameters.Add(new SqlParameter("@AccountID", this.objclsDTOAccountsFormReport.intTempAccountID));
                    return objDataLayer.ExecuteDataSet("spAccRptAccounts", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayStockSummaryItemwise()
        {
            try
            {
                using (objDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 20));
                    parameters.Add(new SqlParameter("@CompanyID", this.objclsDTOAccountsFormReport.intTempCompanyID));
                    parameters.Add(new SqlParameter("@FromDate", this.objclsDTOAccountsFormReport.dtpTempFromDate));
                    parameters.Add(new SqlParameter("@ToDate", this.objclsDTOAccountsFormReport.dtpTempToDate));
                    parameters.Add(new SqlParameter("@AccountID", this.objclsDTOAccountsFormReport.intTempAccountID));
                    return objDataLayer.ExecuteDataSet("spAccRptAccounts", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public DataSet DisplayStockLedger()
        {
            try
            {
                using (objDataLayer = new DataLayer())
                {
                    parameters = new ArrayList();
                    parameters.Add(new SqlParameter("@Mode", 21));
                    parameters.Add(new SqlParameter("@CompanyID", this.objclsDTOAccountsFormReport.intTempCompanyID));
                    parameters.Add(new SqlParameter("@FromDate", this.objclsDTOAccountsFormReport.dtpTempFromDate));
                    parameters.Add(new SqlParameter("@ToDate", this.objclsDTOAccountsFormReport.dtpTempToDate));
                    parameters.Add(new SqlParameter("@AccountID", this.objclsDTOAccountsFormReport.intTempAccountID));
                    return objDataLayer.ExecuteDataSet("spAccRptAccounts", parameters);
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}