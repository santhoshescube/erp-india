﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

namespace MyBooksERP
{
    class ClsDALRptAging
    {
        ArrayList prmReportDetails;
        public DataLayer objclsConnection { get; set; }

        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }

        public DataTable DisplayCompanyHeaderReport(int CompanyId) // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyId));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);
        }

        public DataTable DisplayALLCompanyHeaderReport() // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 32));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);
        }
        public DataTable GetAgeingDetails(int CompanyID, DateTime ReportDate, int CustomerID)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 1));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@ReportDate", ReportDate));
            prmReportDetails.Add(new SqlParameter("@CustomerID", CustomerID));
            return objclsConnection.ExecuteDataTable("STRptAgingReport", prmReportDetails);
        }
    }
}
