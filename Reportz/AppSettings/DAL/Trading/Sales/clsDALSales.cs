﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,29 April 2011>
Description:	<Description,,DAL for Sale>
================================================
*/
namespace MyBooksERP
{
    public class clsDALSales
    {

        ArrayList prmReportDetails;

        public DataLayer objclsConnection { get; set; }

        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }
        //between ('1 Apr 2011') and ('27 Apr 2011')


        public DataTable DisplaySalesQuotationReport(int intCompany, int intCustomer, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) //  sales Report
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strDate="";

            if (intCompany != 0) strSearchCondition = " SQ.CompanyID=" + intCompany + " AND";
            //if (intType != 0) strSearchCondition = strSearchCondition + " Type = " + intType + " AND";
            if (intCustomer != 0) strSearchCondition = strSearchCondition + " SQ.VendorID =" + intCustomer + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " SQ.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " SQ.SalesQuotationNo =" + intNo + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UM.EmployeeID =" + intExecutive + " AND";


            if (intDateType == 1) strDate = " SQ.QuotationDate";

            else if (intDateType == 2) strDate=" SQ.CreatedDate";

            else if (intDateType == 3) strDate = " SQ.DueDate"; 
            
            if (chkFrom == 1 & chkTo == 1)
            {
                //strSearchCondition = strSearchCondition + " SQ.CreatedDate between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                //strSearchCondition = strSearchCondition + " SQ.CreatedDate >= (convert(datetime,'" + strFrom + "',106)" + ")" + " AND";
                strSearchCondition = strSearchCondition + strDate + " >= (convert(datetime,'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= (convert(datetime,'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }
            
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptSales", prmReportDetails);

        }

        public DataTable DisplaySalesOrderReport(int intCompany, int intCustomer, string strFrom, string strTo , int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) //  Employee Report
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " SO.CompanyID=" + intCompany + " AND";
            if (intCustomer != 0) strSearchCondition = strSearchCondition + " SO.VendorID =" + intCustomer + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " SO.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " SO.SalesOrderNo =" + intNo + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UM.EmployeeID=" + intExecutive + " AND";

            //if (chk == 0) strSearchCondition = strSearchCondition + " SO.CreatedDate between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            //else if (chk == 1)
            //{
            //    strFrom = string.Empty;
            //    strTo = string.Empty;
            //    strSearchCondition = strSearchCondition + "";
            //}


            if (intDateType == 1) strDate = " SO.OrderDate";

            else if (intDateType == 2) strDate = " SO.CreatedDate";

            else if (intDateType == 3) strDate = " SO.DueDate"; 

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= (convert(datetime,'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= (convert(datetime,'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }



            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            prmReportDetails.Add(new SqlParameter("@Mode", 2));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptSales", prmReportDetails);

        }

        public DataTable DisplaySalesInvoiceReport(int intCompany, int intCustomer, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) //  Employee Report
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " SI.CompanyID=" + intCompany + " AND";
            if (intCustomer != 0) strSearchCondition = strSearchCondition + " SI.VendorID =" + intCustomer + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " SI.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " SI.SalesInvoiceNo =" + intNo + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UM.EmployeeID =" + intExecutive + " AND";

            if (intDateType == 1) strDate = " SI.InvoiceDate";

            else if (intDateType == 2) strDate = " SI.CreatedDate";

            else if (intDateType == 3) strDate = " SI.DueDate"; 



            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= (convert(datetime,'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= (convert(datetime,'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 3));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptSales", prmReportDetails);

        }



        public DataTable DisplaySalesReturnReport(int intCompany, int intCustomer, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) //  Employee Report
        {
            prmReportDetails = new ArrayList();
            DataTable Dt = new DataTable();
            Dt = null;
            string strSearchCondition = "";
            string strDate = "";

           
           
                 if (intCompany != 0) strSearchCondition = " SIM.CompanyID =" + intCompany + " AND";
                if (intCustomer != 0) strSearchCondition = strSearchCondition + " SIM.VendorID =" + intCustomer + " AND";
                if (intstatus != 0) strSearchCondition = strSearchCondition + " SRM.StatusID =" + intstatus + " AND";
                if (intNo != 0) strSearchCondition = strSearchCondition + " SRM.SalesReturnID =" + intNo + " AND";
                if (intExecutive != 0) strSearchCondition = strSearchCondition + " UM.EmployeeID =" + intExecutive + " AND";


                if (intDateType == 1) strDate = " SRM.ReturnDate";

                else if (intDateType == 2) strDate = " SRM.CreatedDate";

                else if (intDateType == 3) strDate = " SRM.DueDate";


                if (intDateType != 3)
                {

                    if (chkFrom == 1 & chkTo == 1)
                    {
                        strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
                    }
                    else if (chkFrom == 1)
                    {
                        strSearchCondition = strSearchCondition + strDate + " >= (convert(datetime,'" + strFrom + "',106)" + ")" + " AND";
                    }
                    else if (chkTo == 1)
                    {
                        strSearchCondition = strSearchCondition + strDate + " <= (convert(datetime,'" + strTo + "',106)" + ")" + " AND";
                    }
                    else
                    {
                        strFrom = string.Empty;
                        strTo = string.Empty;
                        strSearchCondition = strSearchCondition + "";
                    }
                }



                if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);

                prmReportDetails.Add(new SqlParameter("@Mode", 11));
                prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
                return objclsConnection.ExecuteDataTable("spInvRptSales", prmReportDetails);
          
                   
        }

        //a

        public DataTable DisplayPOSReport(int intCompany, int intCustomer, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) //  POS Report
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " POS.CompanyID=" + intCompany + " AND";
            if (intCustomer != 0 & intCustomer != -1) strSearchCondition = strSearchCondition + " POS.VendorID =" + intCustomer + " AND";
            if (intCustomer == -1) strSearchCondition = strSearchCondition + " isnull(POS.VendorID,0) =0 AND"; 
            if (intstatus != 0) strSearchCondition = strSearchCondition + " POS.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " POS.POSNo =" + intNo + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UM.EmployeeID =" + intExecutive + " AND";


            if (intDateType == 1) strDate = " Convert(datetime,Convert(varchar(15),POSDate,106),106)";

            else if (intDateType == 2) strDate = " Convert(datetime,Convert(varchar(15),POS.CreatedDate,106),106)";  // POS.CreatedDate

            else if (intDateType == 3) strDate = " POS.DueDate";


            if (intDateType != 3)
            {
                if (chkFrom == 1 & chkTo == 1)
                {

                    strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
                }
                else if (chkFrom == 1)
                {

                    strSearchCondition = strSearchCondition + strDate + " >= (convert(datetime,'" + strFrom + "',106)" + ")" + " AND";
                }
                else if (chkTo == 1)
                {
                    strSearchCondition = strSearchCondition + strDate + " <= (convert(datetime,'" + strTo + "',106)" + ")" + " AND";
                }
                else
                {
                    strFrom = string.Empty;
                    strTo = string.Empty;
                    strSearchCondition = strSearchCondition + "";
                }
            }

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode",12));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptSales", prmReportDetails);

        }

        //b
        public DataSet DisplayPOS1(int intCompany, int intCustomer, string strFrom, string strTo, int intType, int intstatus, int intNo, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) //  sales Report
        {

            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strSearchCondition2 = "";
            string strSearchCondition3 = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " POS.CompanyID=" + intCompany + " AND";
            if (intCustomer != 0 & intCustomer!=-1) strSearchCondition = strSearchCondition + " POS.VendorID =" + intCustomer + " AND";
            if (intCustomer == -1) strSearchCondition = strSearchCondition + " isnull(POS.VendorID,0) =0 AND"; 
            if (intstatus != 0) strSearchCondition = strSearchCondition + " POS.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " POS.POSID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition1 = strSearchCondition1 + " POSDetail.POSID =" + intNo + " AND";
            //if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.OperationType=7  " + " AND";
            //if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.PostToAccount=1 " + " AND";
            //if (intNo != 0) strSearchCondition2 = strSearchCondition2 + "  EM.ReferenceID = " + intNo + " AND";

            if (intNo != 0) strSearchCondition3 = " POS.POSID =" + intNo + " AND";
            if (intCompany != 0) strSearchCondition3 = strSearchCondition3 + " Terms.CompanyID =" + intCompany + " AND";



            if (intDateType == 1) strDate = " Convert(datetime,Convert(varchar(15),POSDate,106),106)";

            else if (intDateType == 2) strDate = " Convert(datetime,Convert(varchar(15),POS.CreatedDate,106),106)";

            else if (intDateType == 3) strDate = " POS.DueDate";

            if (chkFrom == 1 & chkTo == 1)
            {

                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {

                strSearchCondition = strSearchCondition + strDate + " >= (convert(datetime,'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= (convert(datetime,'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }


            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);
            //if (strSearchCondition2.Length > 3) strSearchCondition2 = strSearchCondition2.Substring(0, strSearchCondition2.Length - 3);
            if (strSearchCondition3.Length > 3) strSearchCondition3 = strSearchCondition3.Substring(0, strSearchCondition3.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 13));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
           // prmReportDetails.Add(new SqlParameter("@SearchCondition2", strSearchCondition2));
            prmReportDetails.Add(new SqlParameter("@SearchCondition3", strSearchCondition3));
            return objclsConnection.ExecuteDataSet("spInvRptSales", prmReportDetails);
        }



        public DataSet DisplaySalesQuotation(int intCompany, int intCustomer, string strFrom, string strTo, int intType, int intstatus, int intNo, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) //  sales Report
        {
        
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strSearchCondition2 = "";
            string strSearchCondition3 = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " SQ.CompanyID=" + intCompany + " AND";
            if (intCustomer != 0) strSearchCondition = strSearchCondition + " SQ.VendorID =" + intCustomer + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " SQ.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " SQ.SalesQuotationID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition1 = strSearchCondition1 + " QD.SalesQuotationID =" + intNo + " AND";
            if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.OperationTypeID="+(int)OperationType.SalesQuotation+"  " + " AND";
            if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.IsDirectExpense = 1 " + " AND";
            if (intNo != 0) strSearchCondition2 = strSearchCondition2 + "  EM.ReferenceID = " + intNo + " AND";
            if (intNo != 0) strSearchCondition3 = " SQ.SalesQuotationID =" + intNo + " AND";
            if (intCompany != 0) strSearchCondition3 = strSearchCondition3 + " Terms.CompanyID =" + intCompany + " AND";

          

            if (intDateType == 1) strDate = " SQ.QuotationDate";

            else if (intDateType == 2) strDate = " SQ.CreatedDate";

            else if (intDateType == 3) strDate = " SQ.DueDate";

            if (chkFrom == 1 & chkTo == 1)
            {
                
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                
                strSearchCondition = strSearchCondition + strDate + " >= (convert(datetime,'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= (convert(datetime,'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }


            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);
            if (strSearchCondition2.Length > 3) strSearchCondition2 = strSearchCondition2.Substring(0, strSearchCondition2.Length - 3);
            if (strSearchCondition3.Length > 3) strSearchCondition3 = strSearchCondition3.Substring(0, strSearchCondition3.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition2", strSearchCondition2));
            prmReportDetails.Add(new SqlParameter("@SearchCondition3", strSearchCondition3));
            return objclsConnection.ExecuteDataSet("spInvRptSales", prmReportDetails);
             
        }
        public DataSet DisplaySalesOrder(int intCompany, int intCustomer, string strFrom, string strTo , int intType, int intstatus, int intNo, int chkFrom, int chkTo, int intDateType, string strPermittedCompany)
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strSearchCondition2 = "";
            string strSearchCondition3 = "";
            string strSearchCondition4 = "";
            string strDate = "";

            if (intCompany != 0) strSearchCondition = " SO.CompanyID=" + intCompany + " AND";
            if (intCustomer != 0) strSearchCondition = strSearchCondition + " SO.VendorID =" + intCustomer + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " SO.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " SO.SalesOrderID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition1 = strSearchCondition1 + "QD.SalesOrderID =" + intNo + " AND";
            if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.OperationTypeID = "+(int)OperationType.SalesOrder+"  " + " AND";
            if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.IsDirectExpense=1 " + " AND";
            if (intNo != 0) strSearchCondition2 = strSearchCondition2 + " QD.SalesOrderID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition3 = " SO.SalesOrderID =" + intNo + " AND";
            if (intCompany != 0) strSearchCondition3 = strSearchCondition3 + " Terms.CompanyID =" + intCompany + " AND";
            if (intNo != 0) strSearchCondition4 = " where SO.SalesOrderID =" + intNo ;


            if (intDateType == 1) strDate = " SO.OrderDate";

            else if (intDateType == 2) strDate = " SO.CreatedDate";

            else if (intDateType == 3) strDate = " SO.DueDate";

            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= (convert(datetime,'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= (convert(datetime,'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);
            if (strSearchCondition2.Length > 3) strSearchCondition2 = strSearchCondition2.Substring(0, strSearchCondition2.Length - 3);
            if (strSearchCondition3.Length > 3) strSearchCondition3 = strSearchCondition3.Substring(0, strSearchCondition3.Length - 3);

            
            prmReportDetails.Add(new SqlParameter("@Mode", 5));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition2", strSearchCondition2));
            prmReportDetails.Add(new SqlParameter("@SearchCondition3", strSearchCondition3));
            prmReportDetails.Add(new SqlParameter("@SearchCondition4", strSearchCondition4));
            return objclsConnection.ExecuteDataSet("spInvRptSales", prmReportDetails);
        }

        public DataSet DisplaySalesInvoice(int intCompany, int intCustomer, string strFrom, string strTo, int intType, int intstatus, int intNo, int SalesOrderid, int chkFrom, int chkTo, int intDateType, string strPermittedCompany)
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strSearchCondition2 = "";
            string strSearchCondition3 = "";
            string strSearchCondition4 = "";

            string strDate = "";

            if (intCompany != 0) strSearchCondition = " SI.CompanyID=" + intCompany + " AND";
            if (intCustomer != 0) strSearchCondition = strSearchCondition + " SI.VendorID =" + intCustomer + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " SI.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " SI.SalesInvoiceID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition1 = strSearchCondition1 + " QD.SalesInvoiceID =" + intNo + " AND";
            if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.OperationTypeID= "+(int)OperationType.SalesInvoice+"" + " AND";
            if (intType != 0) strSearchCondition2 = strSearchCondition2 + " EM.IsDirectExpense=1" + " AND";

            //EM.PostToAccount=1
            //if (intNo != 0) strSearchCondition2 = strSearchCondition2 + " SI.SalesInvoiceID =" + intNo + " AND";
            //if (SalesOrderid != 0) strSearchCondition2 = strSearchCondition2 + " QD.SalesOrderID =" + SalesOrderid + " AND";
            if (intNo != 0) strSearchCondition2 = strSearchCondition2 + " SIM.SalesInvoiceID =" + intNo + " AND";

            if (intNo != 0) strSearchCondition3 = strSearchCondition3 + " where SI.SalesInvoiceID = " + intNo + " AND";

            if (intNo != 0) strSearchCondition4 = strSearchCondition4 + " SI.SalesInvoiceID = " + intNo + " AND";
            if (intCompany != 0) strSearchCondition4 = strSearchCondition4 + " Terms.CompanyID =" + intCompany + " AND";

            if (intDateType == 1) strDate = " SI.InvoiceDate";

            else if (intDateType == 2) strDate = " SI.CreatedDate";

            else if (intDateType == 3) strDate = " SI.DueDate";



            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= (convert(datetime,'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= (convert(datetime,'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }


            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);
            if (strSearchCondition2.Length > 3) strSearchCondition2 = strSearchCondition2.Substring(0, strSearchCondition2.Length - 3);
            if (strSearchCondition3.Length > 3) strSearchCondition3 = strSearchCondition3.Substring(0, strSearchCondition3.Length - 3);
            if (strSearchCondition4.Length > 3) strSearchCondition4 = strSearchCondition4.Substring(0, strSearchCondition4.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition2", strSearchCondition2));
            prmReportDetails.Add(new SqlParameter("@SearchCondition3", strSearchCondition3));
            prmReportDetails.Add(new SqlParameter("@SearchCondition4", strSearchCondition4));
            return objclsConnection.ExecuteDataSet("spInvRptSales", prmReportDetails);
        }


        public DataSet DisplaySalesReturn(int intCompany, int intCustomer, string strFrom, string strTo, int intstatus, int intNo, int chkFrom, int chkTo, int intDateType, int intExecutive, string strPermittedCompany)
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strSearchCondition1 = "";
            string strSearchCondition2 = "";

            string strDate = "";

            if (intCompany != 0) strSearchCondition = " SIM.CompanyID =" + intCompany + " AND";
            if (intExecutive != 0) strSearchCondition = strSearchCondition + " UM.EmployeeID =" + intExecutive + " AND";
            if (intCustomer != 0) strSearchCondition = strSearchCondition + " SIM.VendorID =" + intCustomer + " AND";
            if (intstatus != 0) strSearchCondition = strSearchCondition + " SRM.StatusID =" + intstatus + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " SRM.SalesReturnID =" + intNo + " AND";
            if (intNo != 0) strSearchCondition1 = strSearchCondition1 + " SR.SalesReturnID =" + intNo + " AND";

            if (intNo != 0) strSearchCondition2 = strSearchCondition2 + " SR.SalesReturnID = " + intNo + " AND";
            if (intCompany != 0) strSearchCondition2 = strSearchCondition2 + " Terms.CompanyID =" + intCompany + " AND";




            if (intDateType == 1) strDate = " SRM.ReturnDate";

            else if (intDateType == 2) strDate = " SRM.CreatedDate";

            else if (intDateType == 3) strDate = " SRM.DueDate";


            if (chkFrom == 1 & chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (chkFrom == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " >= (convert(datetime,'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (chkTo == 1)
            {
                strSearchCondition = strSearchCondition + strDate + " <= (convert(datetime,'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }


            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            if (strSearchCondition1.Length > 3) strSearchCondition1 = strSearchCondition1.Substring(0, strSearchCondition1.Length - 3);
            if (strSearchCondition2.Length > 3) strSearchCondition2 = strSearchCondition2.Substring(0, strSearchCondition2.Length - 3);


            prmReportDetails.Add(new SqlParameter("@Mode", 10));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            prmReportDetails.Add(new SqlParameter("@SearchCondition1", strSearchCondition1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition2", strSearchCondition2));
            return objclsConnection.ExecuteDataSet("spInvRptSales", prmReportDetails);
        }

        public DataTable DisplayCompanyHeaderReport(int CompanyId) // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyId));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);

        }

        public DataTable DisplayALLCompanyHeaderReport() // for all company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 32));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);

        }

        public DataTable DisplayExpenseOrderId(int intType) // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 9));
            prmReportDetails.Add(new SqlParameter("@SalesInvoiceID", intType));
            return objclsConnection.ExecuteDataTable("spInvRptSales", prmReportDetails);

        }
        public DataTable GetSaleInvoiceDetail(long lngInvoiceID)
        {
            try
            {
                prmReportDetails = new ArrayList();
                prmReportDetails.Add(new SqlParameter("@Mode", 22));
                prmReportDetails.Add(new SqlParameter("@SalesInvoiceID", lngInvoiceID));

                return objclsConnection.ExecuteDataTable("spInvSalesInvoice", prmReportDetails);
            }
            catch (Exception ex) { throw ex; }
        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@RoleID", intRoleID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", prmReportDetails);
        }


        public bool GenearlCustomerCheck()
        {
            bool blnRetValue = false;
            string strProcedureName = string.Empty;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            //parameters.Add(new SqlParameter("@CompanyID", intComapnyID));

            SqlDataReader sdr = objclsConnection.ExecuteReader("spInvRptSales", parameters);
            while (sdr.Read())
            {
                if (sdr["VendorID"] == DBNull.Value)
                {
                    blnRetValue = true;

                }

            }
            sdr.Close();
            return blnRetValue;
        }
    }
}
