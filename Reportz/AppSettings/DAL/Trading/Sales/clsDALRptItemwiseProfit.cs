﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyBooksERP
{
    public class clsDALRptItemwiseProfit
    {

        readonly string PROCEDURE_NAME = "spItemWiseProfit";
        DataLayer MObjConnection;

        public clsDALRptItemwiseProfit()
        {
            MObjConnection = new DataLayer();
        }

        public DataSet LoadCombos(int intCompanyID, int intCategoryID, int intSubCategoryID)
        {
            return MObjConnection.ExecuteDataSet(PROCEDURE_NAME, new List<SqlParameter>
            {
                new SqlParameter("@Mode",1),
                new SqlParameter("@CompanyID",intCompanyID),
                new SqlParameter("@CategoryID",intCategoryID),
                new SqlParameter("@SubCategoryID",intSubCategoryID)
            });
        }

        public DataSet GetReport(int intItemID, int intCompanyID, int intCategoryID, int intSubCategoryID, int intTypeID, DateTime dtFromDate, DateTime dtToDate, bool IsGroup)
        {
            return MObjConnection.ExecuteDataSet(PROCEDURE_NAME, new List<SqlParameter>
            {
                new SqlParameter("@Mode",2),
                new SqlParameter("@CompanyID",intCompanyID),
                new SqlParameter("@CategoryID",intCategoryID),
                new SqlParameter("@SubCategoryID",intSubCategoryID),
                new SqlParameter("@ItemID",intItemID),
                new SqlParameter("@OperationTypeID",intTypeID),
                new SqlParameter("@FromDate",dtFromDate),
                new SqlParameter("@ToDate",dtToDate),
                new SqlParameter("@IsGroup",IsGroup)
            });
        }
    }
}
