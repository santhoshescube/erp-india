﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALRptPendingDelivery
    {
        ArrayList prmReportDetails;
        public DataLayer MobjclsConnection { get; set; }

        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = MobjclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }

        public DataTable GetCompanyHeader(int intCompanyID)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",1));
            prmReportDetails.Add(new SqlParameter("@CompanyID",intCompanyID));
            return MobjclsConnection.ExecuteDataTable("spInvRptPendingDelivery", prmReportDetails);

        }

        public DataSet GetReport(int intCompanyID, int intVendorID, long lngItemIssueID, int intStatusID, DateTime dtFromdate, DateTime dtTodate, int intchkFrmTo)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 2));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@VendorID", intVendorID));
            prmReportDetails.Add(new SqlParameter("@ItemIssueID", lngItemIssueID));
            prmReportDetails.Add(new SqlParameter("@StatusID", intStatusID));
            prmReportDetails.Add(new SqlParameter("@FromDate", dtFromdate));
            prmReportDetails.Add(new SqlParameter("@ToDate", dtTodate));
            prmReportDetails.Add(new SqlParameter("@chkFrmTo", intchkFrmTo));
            return MobjclsConnection.ExecuteDataSet("spInvRptPendingDelivery", prmReportDetails);
        }
    }
}
