﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,11 July 2011>
Description:	<Description,,DAL for Payment/Receipt>
================================================
*/
namespace MyBooksERP
{
    public class ClsMISDALPaymentReceipt
    {

        ArrayList prmReportDetails;

        public DataLayer objclsConnection { get; set; }

        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }

        public DataTable DisplayPaymentReceiptReport(int intCompany, int intType, int intVendor, string strFrom, string strTo, int intTransactionType, int intPaymentNo, int intNo, int chkFrom, int chkTo, int DateType, string strPermittedCompany) //  sales Report
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            string strCondition = "";
            string strDate = "";
            if (intCompany != 0) strSearchCondition = " Payments.CompanyID=" + intCompany + " AND";
           
            if (intType == (int)OperationType.PurchaseOrder)
            {
                if (intVendor != 0) strSearchCondition = strSearchCondition + " VI.VendorID =" + intVendor + " AND";
            }
            //if (intType == (int)OperationType.PurchaseInvoice)
            //{
            //    if (intVendor != 0) strSearchCondition = strSearchCondition + " VI1.VendorID =" + intVendor + " AND";
            //}
            if (intType == (int)OperationType.PurchaseInvoice)
            {
                if (intVendor != 0) strSearchCondition = strSearchCondition + " VI.VendorID =" + intVendor + " AND";
            }
            if (intType == (int)OperationType.SalesOrder)
            {
                if (intVendor != 0) strSearchCondition = strSearchCondition + " VI2.VendorID =" + intVendor + " AND";
            }
            //if (intType == (int)OperationType.SalesInvoice)
            //{
            //    if (intVendor != 0) strSearchCondition = strSearchCondition + " VI3.VendorID =" + intVendor + " AND";
            //}
            if (intType == (int)OperationType.SalesInvoice)
            {
                if (intVendor != 0) strSearchCondition = strSearchCondition + " VI.VendorID =" + intVendor + " AND";
            }
            if (intType == (int)OperationType.POS)
            {
                if (intVendor != 0 & intVendor!= -1) strSearchCondition = strSearchCondition + " VI4.VendorID =" + intVendor + " AND";
                if (intVendor == -1) strSearchCondition = strSearchCondition + " isnull(VI4.VendorID,0) =0" + " AND";
                                                                               
            }
            if (intType == 18)
            {
                if (intVendor != 0) strSearchCondition = strSearchCondition + " VI5.VendorID =" + intVendor + " AND";
            }
            if (intType == (int)OperationType.StockTransfer)
            {
                if (intVendor != 0) strSearchCondition = strSearchCondition + " VI6.VendorID =" + intVendor + " AND";
            }
            

            if (intTransactionType != 3) strSearchCondition = strSearchCondition + " Payments.IsReceipt =" + intTransactionType + " AND";
            if (intPaymentNo != 0) strSearchCondition = strSearchCondition + " Payments.ReceiptAndPaymentID =" + intPaymentNo + " AND";

            if (DateType == 1) strDate = " Payments.PaymentDate";
            else if (DateType == 2) strDate = " Payments.CreatedDate";

           
            //if (chk == 0) strSearchCondition = strSearchCondition + " Payments.PaymentDate between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";

            if (chkFrom==1 & chkTo==1) 
                strSearchCondition = strSearchCondition + strDate + " between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            else if(chkFrom==1)
              
                strSearchCondition = strSearchCondition + strDate + " >= (convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            else if(chkTo==1)
                strSearchCondition = strSearchCondition + strDate + " <= (convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";

            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }

            strCondition = strSearchCondition;
            if (intType != 0) strSearchCondition = strSearchCondition + " Payments.OperationTypeID = " + intType + " AND";
            if (intNo != 0) strSearchCondition = strSearchCondition + " RPD.ReferenceID   =" + intNo + " AND";

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            DataTable dtReportSource = new DataTable();

            if (intType == (int)OperationType.SalesInvoice)
                prmReportDetails.Add(new SqlParameter("@Mode", 4));
            else if (intType ==  (int)OperationType.PurchaseInvoice)
                prmReportDetails.Add(new SqlParameter("@Mode", 3));
            else
                prmReportDetails.Add(new SqlParameter("@Mode", 1));

            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            DataTable dtInvoice = objclsConnection.ExecuteDataTable("spInvRptPaymentReceipt", prmReportDetails);
            if (dtReportSource.Rows.Count > 0)
            {
                int intCol = dtInvoice.Columns.Count;
                foreach (DataRow dr in dtInvoice.Rows)
                {
                    DataRow drNew = dtReportSource.NewRow();
                    for (int i = 0; i < intCol; i++)
                    {
                        drNew[i] = dr[i];
                    }
                    dtReportSource.Rows.Add(drNew);
                }
                return dtReportSource;
            }
            return dtInvoice;
        }

        public DataTable DisplayCompanyHeaderReport(int CompanyId) // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyId));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);

        }

        public DataTable DisplayALLCompanyHeaderReport() // for all company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 32));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);

        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@RoleID", intRoleID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", prmReportDetails);
        }


        

        public bool GenearlCustomerCheck()
        {
            bool blnRetValue = false;
            string strProcedureName = string.Empty;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            //parameters.Add(new SqlParameter("@CompanyID", intComapnyID));

            SqlDataReader sdr = objclsConnection.ExecuteReader("spInvRptPaymentReceipt", parameters);
            while (sdr.Read())
            {
                if (sdr["VendorID"] == DBNull.Value)
                {
                    blnRetValue = true;

                }
               
            }
            sdr.Close();
            return blnRetValue;
        }
    }
}
