﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    class clsDALRptExpense
    {


         private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALRptExpense() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable DisplayCompanyReportHeader(int intCompanyID) //CompnayReportHeader
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 31),
                new SqlParameter("@CompanyID",intCompanyID)
                };
            return DataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", this.sqlParameters);


        }

        public DataTable GetExpenseDetails(int intType, int intCompanyID, int intExpenseTypeID, int intDateType, DateTime dteFromDate, DateTime dteToDate)
        {
            this.sqlParameters = new List<SqlParameter> ();

            if (intType == 0)
            {
                 this.sqlParameters.Add(new SqlParameter("@Mode", 1));//Purchase
            }
            else if (intType == 1)
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 3));// Sales
            }
            this.sqlParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
                this.sqlParameters .Add(new SqlParameter("@ExpenseTypeID",intExpenseTypeID)); 
                this.sqlParameters .Add(new SqlParameter("@DateType",intDateType)); 
                this.sqlParameters .Add(new SqlParameter("@FromDate",dteFromDate));
                this.sqlParameters.Add(new SqlParameter("@ToDate", dteToDate));

                return DataLayer.ExecuteDataTable("spInvRptExpense", this.sqlParameters);
        }

        public DataTable GetExpenseSummary(int intType, int intCompanyID , int intDateType, DateTime dteFromDate, DateTime dteToDate)
        {
            this.sqlParameters = new List<SqlParameter>();

            if (intType == 0)
            {

                this.sqlParameters.Add(new SqlParameter("@Mode", 2));//Purchase
            }
            else if (intType == 1)
            {

                this.sqlParameters.Add(new SqlParameter("@Mode", 4));// Sales

            }
            this.sqlParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            this.sqlParameters.Add(new SqlParameter("@DateType", intDateType));
            this.sqlParameters.Add(new SqlParameter("@FromDate", dteFromDate));
            this.sqlParameters.Add(new SqlParameter("@ToDate", dteToDate));

            return DataLayer.ExecuteDataTable("spInvRptExpense", this.sqlParameters);
        }





    }
}
