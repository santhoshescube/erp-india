﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,28 April 2011>
Description:	<Description,,DAL for Employee>
================================================
*/

namespace MyBooksERP
{
    public class clsDALEmployee
    {
        ArrayList prmReportDetails;

        public DataLayer objclsConnection { get; set; }

        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }


        public DataTable DisplayALLEmployeeReport(int intCompany, int intDesignation, int intDepartment, int intWorkStatus, string stPermittedCompany) //  Employee Report
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            {
                if (intCompany != 0) strSearchCondition = " CM.CompanyID=" + intCompany + " AND";
            }
            else
            {
                if (stPermittedCompany != null)
                    strSearchCondition = "CM.CompanyID in(SELECT CompanyMaster.CompanyID FROM CompanyMaster WHERE CompanyID IN (" + stPermittedCompany + ")) AND";
            }

            if (intDesignation != 0) strSearchCondition = strSearchCondition + " d.DesignationID =" + intDesignation + " AND";
            if (intDepartment != 0) strSearchCondition = strSearchCondition + " dp.DepartmentID =" + intDepartment + " AND";
            if (intWorkStatus != 0) strSearchCondition = strSearchCondition + " WSR.WorkStatusID =" + intWorkStatus + " AND";
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptEmployee", prmReportDetails);
        }
        public DataTable DisplayCompanyHeaderReport(int CompanyId) // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyId));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);
        }

        public DataTable DisplayALLCompanyHeaderReport() // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 32));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);

        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@RoleID", intRoleID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable("STPermissionSettings", prmReportDetails);
        }
        

    }
}
