﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    class clsDALRptEmolyeeProfile
    {

        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALRptEmolyeeProfile() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable DisplayCompanyReportHeader(int intCompanyID) //CompnayReportHeader
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 31),
                new SqlParameter("@CompanyID",intCompanyID)
                };
            return DataLayer.ExecuteDataTable("spInvPurchaseModuleFunctions", this.sqlParameters);

        }

        public static DataTable GetEmployeeInformation(int intCompanyID, int intBranchID, int intDepartmentID, int intDesignationID, int intEmploymentTypeID, int intEmployeeID, bool intIncludeComp, int intLocation, int intWorkStatus, int intNationalityID, int intReligionID, string sDateOfJoining, string sDateOfJoiningTo)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>
            { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID", intBranchID) ,
                new SqlParameter("@DepartmentID", intDepartmentID) ,
                new SqlParameter("@DesignationID", intDesignationID) ,
                new SqlParameter("@EmploymentTypeID", intEmploymentTypeID),
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@IncludeComp", intIncludeComp),
                new SqlParameter("@LocationID", intLocation),
                new SqlParameter("@WorkStatusID", intWorkStatus),
                new SqlParameter("@NationalityID", intNationalityID),
                new SqlParameter("@ReligionID", intReligionID),
                new SqlParameter("@DOJ",sDateOfJoining=="01-Jan-1900"?null:sDateOfJoining),
                new SqlParameter("@CountryID", -1),
                new SqlParameter("@DOJTo", sDateOfJoiningTo),
                new SqlParameter("@UserID",ClsCommonSettings.UserID)
            };
            return new DataLayer().ExecuteDataTable("spPayRptEmployeeProfileSummary", sqlParameters);
        }

        public static DataTable DisplaySingleEmployeeReport(int intEmployeeID)  //Employee  Report
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>
            { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@EmployeeID", intEmployeeID)
            };
            return new DataLayer().ExecuteDataTable("spPayRptEmployeeProfileSummary", sqlParameters);
        }


    }
}
