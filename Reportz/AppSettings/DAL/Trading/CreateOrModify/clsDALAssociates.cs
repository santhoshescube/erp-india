﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Windows.Forms;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,17 May 2011>
Description:	<Description,DAL for Associative>
================================================
*/
namespace MyBooksERP 
{
    public class clsDALAssociates
    {
        ArrayList prmReportDetails;

        public DataLayer objclsConnection { get; set; }

        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }

        public AutoCompleteStringCollection FillStringCollection(string[] sarFieldValues)
        {
            DataTable datValues = new DataTable();
            AutoCompleteStringCollection strColValues = new AutoCompleteStringCollection();

            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    datValues = objCommonUtility.FillCombos(sarFieldValues);

                    for (int intICounter = 0; intICounter < datValues.Rows.Count; intICounter++)
                        strColValues.Add(datValues.Rows[intICounter][sarFieldValues[0]].ToString());


                    return strColValues;

                }

            }
            return null;
        }

        public DataTable DisplayAssociativeReport(int intType, string strType, int intTypeselected) //  Employee Report
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";

            if (intType != 0) strSearchCondition = " V.VendorTypeID=" + intType + " AND";
            if (intTypeselected == 1) strSearchCondition = strSearchCondition + " CountryReference.CountryName like '" + strType + "%' AND";
            if (intTypeselected == 2) strSearchCondition = strSearchCondition + " Status like '" + strType + "%' AND";
            if (intTypeselected == 3) strSearchCondition = strSearchCondition + " VA.MobileNo like '" + strType + "%' AND";
            if (intTypeselected == 4) strSearchCondition = strSearchCondition + " T.TransactionType like '" + strType + "%' AND";

            if (intTypeselected == 5) strSearchCondition = strSearchCondition + " SIM.SalesInvoiceNo like '" + strType + "%' AND";
            if (intTypeselected == 6) strSearchCondition = strSearchCondition + " SOM.SalesOrderNo like '" + strType + "%' AND";
            if (intTypeselected == 7) strSearchCondition = strSearchCondition + " PIM.PurchaseInvoiceNo like '" + strType + "%' AND";
            if (intTypeselected == 8) strSearchCondition = strSearchCondition + " POM.PurchaseOrderNo like '" + strType + "%' AND";

            
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);
            prmReportDetails.Add(new SqlParameter("@Mode", 1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptAssociates", prmReportDetails);

        }       

        public DataTable DisplayALLCompanyHeaderReport() // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 32));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);

        }


    }
}
