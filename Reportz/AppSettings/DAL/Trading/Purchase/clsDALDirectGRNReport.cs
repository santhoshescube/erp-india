﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using System.Data;
using System.Data.SqlClient;
/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,22 Feb 2011>
   Description:	<Description,,Purchase DAL Class>
================================================
*/
namespace MyBooksERP
{
    public class clsDALDirectGRNReport : IDisposable
    {
        ClsCommonUtility MobjClsCommonUtility; // object of clsCommonUtility
        public DataLayer MobjDataLayer;

        public clsDTOGRNReport PobjClsDTOGRN { get; set; }

        public clsDALDirectGRNReport(DataLayer objDataLayer)
        {
            MobjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataSet FillCombo(int intCompanyID, int intVendorID, long lngGRNID, int intStatusID)
        {
            ArrayList prmRptGRN = new ArrayList();
            prmRptGRN.Add(new SqlParameter("@Mode", 1));
            prmRptGRN.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmRptGRN.Add(new SqlParameter("@VendorID", intVendorID));
            return MobjDataLayer.ExecuteDataSet("spInvRptGRN", prmRptGRN);
        }

        public DataSet GetReport(int intCompanyID, int intVendorID, long lngGRNID, int intStatusID, DateTime? dtFrmDate, DateTime? dtToDate, int intChkFrmTo)
        {
            ArrayList prmRptGRN = new ArrayList();
            prmRptGRN.Add(new SqlParameter("@Mode",2));
            prmRptGRN.Add(new SqlParameter("@CompanyID",intCompanyID));
            prmRptGRN.Add(new SqlParameter("@VendorID",intVendorID));
            prmRptGRN.Add(new SqlParameter("@GRNID",lngGRNID));
            prmRptGRN.Add(new SqlParameter("@StatusID",intStatusID));
            prmRptGRN.Add(new SqlParameter("@FromDate", dtFrmDate));
            prmRptGRN.Add(new SqlParameter("@ToDate", dtToDate));
            prmRptGRN.Add(new SqlParameter("@chkFrmTo", intChkFrmTo));
            return MobjDataLayer.ExecuteDataSet("spInvRptGRN", prmRptGRN);
        }

       
        #region IDisposable Members

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion

       
    }
}
