﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    class clsDALRptPurchaseSummary
    {
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;
        private string strProcedureName = "spRptPurchaseSummary";

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }


        public DataSet LoadCombo(int CompanyID)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID)
                };
            return DataLayer.ExecuteDataSet(strProcedureName, this.sqlParameters);
        }

        public DataSet GetReport(int ItemID, int CompanyID, int VendorID, DateTime dtFromDate, DateTime dtToDate, int intFilterType)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",CompanyID),
                new SqlParameter("@ItemID",ItemID),
                new SqlParameter("@VendorID",VendorID),
                new SqlParameter("@FromDate",dtFromDate),
                new SqlParameter("@ToDate",dtToDate),
                new SqlParameter("@FilterType",intFilterType)
                };
            return DataLayer.ExecuteDataSet(strProcedureName, this.sqlParameters);
        }
    }
}
