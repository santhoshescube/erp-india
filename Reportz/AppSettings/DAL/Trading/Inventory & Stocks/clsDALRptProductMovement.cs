﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace MyBooksERP
{
    public class clsDALRptProductMovement
    {
        readonly string PROCEDURE_NAME = "spRptProductMovement";
        ArrayList alParameters;
        DataLayer MObjConnection;

        public clsDALRptProductMovement()
        {
            MObjConnection = new DataLayer();
        }

        public DataSet GetReport(int CompanyID, DateTime FromDate, DateTime ToDate, int intCategoryID, int intSubCategoryID, int intItemID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "1"));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
            alParameters.Add(new SqlParameter("@CategoryID", intCategoryID));
            alParameters.Add(new SqlParameter("@SubCategoryID", intSubCategoryID));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            return MObjConnection.ExecuteDataSet(this.PROCEDURE_NAME, alParameters);
        }

        public DataTable GetCompany()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "2"));
            alParameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.LoginCompanyID));

            return MObjConnection.ExecuteDataTable(this.PROCEDURE_NAME, alParameters);
        }

        public DataTable GetWareHouseByCompany(int intCompanyID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "3"));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            return MObjConnection.ExecuteDataTable(this.PROCEDURE_NAME, alParameters);
        }

        public DataTable GetCategory()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "4"));
            return MObjConnection.ExecuteDataTable(this.PROCEDURE_NAME, alParameters);
        }

        public DataTable GetSubCategoryByCategory(int intCategoryID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "5"));
            alParameters.Add(new SqlParameter("@CategoryID", intCategoryID));
            return MObjConnection.ExecuteDataTable(this.PROCEDURE_NAME, alParameters);
        }

        public SqlDataReader GetDetailsForChart(int CompanyID, DateTime FromDate, DateTime ToDate, int intCategoryID, int intSubCategoryID, int intItemID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "8"));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
            alParameters.Add(new SqlParameter("@CategoryID", intCategoryID));
            alParameters.Add(new SqlParameter("@SubCategoryID", intSubCategoryID));
            alParameters.Add(new SqlParameter("@ItemID", intItemID));
            return MObjConnection.ExecuteReader(this.PROCEDURE_NAME, alParameters);
        }

        public DataTable GetItems(int intCompanyID, int intCategoryID, int intSubCategoryID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "7"));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            alParameters.Add(new SqlParameter("@CategoryID", intCategoryID));
            alParameters.Add(new SqlParameter("@SubCategoryID", intSubCategoryID));
            return MObjConnection.ExecuteDataTable(this.PROCEDURE_NAME, alParameters);
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = MObjConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }
            }
            return null;
        }
        public DataSet GetPurchaseSalesReport(int CompanyID, DateTime FromDate, DateTime ToDate, int intType, long lngReferenceID, int intReportType)
        {
            alParameters = new ArrayList();
            if (intReportType == 1)
            {
                if(intType == 0)
                alParameters.Add(new SqlParameter("@Mode", 12));
                else
                alParameters.Add(new SqlParameter("@Mode", 9));
            }
            else
            if (intReportType == 2)
            {
                if (intType == 0)
                    alParameters.Add(new SqlParameter("@Mode", 13));
                else
                    alParameters.Add(new SqlParameter("@Mode", 11));
            }

            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
            alParameters.Add(new SqlParameter("@ReferenceID", lngReferenceID));
            alParameters.Add(new SqlParameter("@OperationTypeID", intType));
            return MObjConnection.ExecuteDataSet(this.PROCEDURE_NAME, alParameters);
        }

        public DataTable GetReferenceNo(int intCompanyID,int intOperationTypeID,DateTime dtFromDate,DateTime dtToDate)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "10"));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            alParameters.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
            alParameters.Add(new SqlParameter("@FromDate", dtFromDate));
            alParameters.Add(new SqlParameter("@ToDate", dtToDate));
            return MObjConnection.ExecuteDataTable(this.PROCEDURE_NAME, alParameters);
        }

    }
}
