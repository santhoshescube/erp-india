﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,27 April 2011>
Description:	<Description,,DAL for Product>
================================================
*/

namespace MyBooksERP 
{
    public  class clsDALProductReport
    {
        ArrayList prmReportDetails;

        public DataLayer objclsConnection { get; set; }

        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }
            }
            return null;
        }

        public DataTable DisplayALLProductReport(int intCompanyID, int intCategoryID, int intManufactureID, int intStatusID, int intWarehouseID,int intSubCategory, string ExpiryDate, string strPermittedCompany,int intFeature,int intFeatureValue) //  Product Report
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            if (intCompanyID != 0) strSearchCondition = "T.CompanyID=" + intCompanyID + " AND";
            if (intCategoryID != 0) strSearchCondition = strSearchCondition + " T.CategoryID=" + intCategoryID + " AND";
            if (intManufactureID != 0) strSearchCondition = strSearchCondition + " T.ManufactureID=" + intManufactureID + " AND";
            if (intStatusID != 0) strSearchCondition = strSearchCondition + " T.StatusID=" + intStatusID + " AND";
            if (intWarehouseID != 0) strSearchCondition = strSearchCondition + " T.WarehouseID=" + intWarehouseID + " AND";
            if (intSubCategory != 0) strSearchCondition = strSearchCondition + " T.SubCategoryID=" + intSubCategory + " AND";
            if (intFeature != -1) strSearchCondition = strSearchCondition + " T.FeatureID=" + intFeature + " AND";
            if (intFeatureValue != -1) strSearchCondition = strSearchCondition + " T.FeatureValueID=" + intFeatureValue + " AND";

            if (!string.IsNullOrEmpty(ExpiryDate)) strSearchCondition = strSearchCondition + " (isnull(T.ExpiryDate,'' ) <> '') and  T.ExpiryDate <= (convert(datetime,'" + ExpiryDate + "',106)" + ")" + "AND ";
             
            if (strSearchCondition.Length > 4) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);
            if (intWarehouseID != 0) 
                prmReportDetails.Add(new SqlParameter("@Mode", 5));
            else
                prmReportDetails.Add(new SqlParameter("@Mode", 2));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptProduct", prmReportDetails);            
        }

        public DataTable DisplayALLPriceHistory(int intCompanyID, int intWarehouseID, int intCategoryID,int intSubCategory, int intManufactureID, int intStatusID, int intchkFrom, int intchkTo, string strFrom, string strTo, string strPermittedCompany)
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";
            //if (ClsCommonSettings.RoleID == 1 || ClsCommonSettings.RoleID == 2)
            //{
                if (intCompanyID != 0) strSearchCondition = "T.CompanyID= " + intCompanyID + " AND";
            //}
            //else
            //{
            //    strSearchCondition = "T.CompanyID in(SELECT CompanyMaster.CompanyID FROM CompanyMaster WHERE CompanyID IN (" + strPermittedCompany + ")) AND";
            //}
            if (intCategoryID != 0) strSearchCondition = strSearchCondition + " T.CategoryID=" + intCategoryID + " AND";
            if (intManufactureID != 0) strSearchCondition = strSearchCondition + " T.ManufactureID=" + intManufactureID + " AND";
            if (intStatusID != 0) strSearchCondition = strSearchCondition + " T.StatusID=" + intStatusID + " AND";
            if (intWarehouseID != 0) strSearchCondition = strSearchCondition + " T.WarehouseID=" + intWarehouseID + " AND";
            if (intSubCategory != 0) strSearchCondition = strSearchCondition + " T.SubCategoryID=" + intSubCategory + " AND";

            if (intchkFrom == 1 & intchkTo == 1)
            {
                strSearchCondition = strSearchCondition + " T.Date between " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else if (intchkFrom == 1)
            {
                strSearchCondition = strSearchCondition + " T.Date >= " + "(convert(datetime," + "'" + strFrom + "',106)" + ")" + " AND";
            }
            else if (intchkTo == 1)
            {
                strSearchCondition = strSearchCondition + " T.Date <= " + "(convert(datetime," + "'" + strTo + "',106)" + ")" + " AND";
            }
            else
            {
                strFrom = string.Empty;
                strTo = string.Empty;
                strSearchCondition = strSearchCondition + "";
            }
 
            if (strSearchCondition.Length > 4) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 4);
            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable("spInvRptProduct", prmReportDetails);   
        }

       public DataTable DisplayALLCompanyHeaderReport() // for company report
       {
           prmReportDetails = new ArrayList();
           prmReportDetails.Add(new SqlParameter("@Mode", 32));
           return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);
       }

       public DataTable DisplayCompanyHeaderReport(int CompanyId) // for company report
       {
           prmReportDetails = new ArrayList();
           prmReportDetails.Add(new SqlParameter("@Mode", 31));
           prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyId));
           return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);
       }
       public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
       {
           ArrayList prmReportDetails = new ArrayList();
           prmReportDetails.Add(new SqlParameter("@Mode", 4));
           prmReportDetails.Add(new SqlParameter("@RoleID", intRoleID));
           prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
           prmReportDetails.Add(new SqlParameter("@ControlID", intControlID));
           return objclsConnection.ExecuteDataTable("STPermissionSettings", prmReportDetails);
       }
       public DataTable GetPermittedCompany(int intRoleID, int intCompanyID, int intControlID)
       {
           ArrayList prmReportDetails = new ArrayList();
           prmReportDetails.Add(new SqlParameter("@Mode", 3));
           prmReportDetails.Add(new SqlParameter("@RoleID", intRoleID));
           prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
           prmReportDetails.Add(new SqlParameter("@ControlID", intControlID));
           return objclsConnection.ExecuteDataTable("spInvRptProduct", prmReportDetails);
       }
    }
}
