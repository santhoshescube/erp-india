﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALRptVendorPriceHistory
    {
        ArrayList prmReportDetails;
        public DataLayer MobjclsConnection { get; set; }

        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = MobjclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }
            }
            return null;
        }

        public DataTable LoadItems(int intType)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode",1));
            prmReportDetails.Add(new SqlParameter("@TypeID",intType));
            return MobjclsConnection.ExecuteDataTable("spInvRptVendorPriceHistory", prmReportDetails);
        }

        public DataTable GetReport(int intTypeID, int intItemID, bool blnIsGroup, DateTime dtFromDate, DateTime dtToDate, int intchkFrmTo)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 2));
            prmReportDetails.Add(new SqlParameter("@TypeID",intTypeID));
            prmReportDetails.Add(new SqlParameter("@ItemID",intItemID));
            prmReportDetails.Add(new SqlParameter("@IsGroup",blnIsGroup));
            prmReportDetails.Add(new SqlParameter("@FromDate",dtFromDate));
            prmReportDetails.Add(new SqlParameter("@ToDate",dtToDate));
            prmReportDetails.Add(new SqlParameter("@chkFrmTo",intchkFrmTo));
            return MobjclsConnection.ExecuteDataTable("spInvRptVendorPriceHistory", prmReportDetails);
        }
    }

}
