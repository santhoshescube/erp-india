﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALRptProductGroup
    {
        ArrayList prmReportDetails;
        public DataLayer objclsConnection { get; set; }

        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }

        public DataTable GetCompanyHeader(int intCompanyID)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 1));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objclsConnection.ExecuteDataTable("spInvRptProductGroup", prmReportDetails);
        }

        public DataTable GetReport(int intCompanyID,int intItemGroupID,DateTime dtFromDate,DateTime dtToDate,int intchkFrmTo)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 2));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@ItemGroupID", intItemGroupID));
            prmReportDetails.Add(new SqlParameter("@FromDate", dtFromDate));
            prmReportDetails.Add(new SqlParameter("@ToDate", dtToDate));
            prmReportDetails.Add(new SqlParameter("@chkFrmTo", intchkFrmTo));
            return objclsConnection.ExecuteDataTable("spInvRptProductGroup", prmReportDetails);
        }
    }
}
