﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,18 May 2011>
Description:	<Description,,DAL for Stock>
================================================
*/
namespace MyBooksERP
{
    public class ClsDALMISStockReport
    {
        ArrayList prmReportDetails;

        public DataLayer objclsConnection { get; set; }
        public string strProcedurePurchaseModule = "spInvPurchaseModuleFunctions";
        public string strProcedureStockDetails = "spInvRptStock";
        public string strProcedurePermissionSettings = "STPermissionSettings";

        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }
                //public DataTable DisplayStockReport(int intCompany, int intWarehouse, string strFrom, string strTo, int chk, int intCategory, int intSubCategory, int intItemName,int intBatch) 



        public DataTable DisplayZeroReport(int intCompany, int intWarehouse, int intCategory, int intSubCategory, int intItemName, string strBatch)
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";

            if (intCompany != 0) strSearchCondition = " SD.CompanyID=" + intCompany + " AND";
            if (intWarehouse != 0) strSearchCondition = strSearchCondition + " SD.WarehouseID =" + intWarehouse + " AND";
            if (intCategory != 0) strSearchCondition = strSearchCondition + " IM.CategoryID =" + intCategory + " AND";
            if (intSubCategory != 0) strSearchCondition = strSearchCondition + " IM.SubCategoryID =" + intSubCategory + " AND";
            if (intItemName != 0) strSearchCondition = strSearchCondition + " IM.ItemID=" + intItemName + " AND";
            if (strBatch == "ALL")
            {
                strSearchCondition = strSearchCondition + "";
            }
            else
            {
                strSearchCondition = strSearchCondition + " IB.BatchNo ='" + strBatch + "' AND";
            }
            
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 1));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable(strProcedureStockDetails, prmReportDetails);

        }
        public DataTable DisplayNormalStockReport(int intCompany, int intWarehouse, int intCategory, int intSubCategory, int intItemName,string strBatch) 
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";

            if (intCompany != 0) strSearchCondition = " SD.CompanyID=" + intCompany + " AND";
            if (intWarehouse != 0) strSearchCondition = strSearchCondition + " SD.WarehouseID =" + intWarehouse + " AND";
            if (intCategory != 0) strSearchCondition = strSearchCondition + " IM.CategoryID =" + intCategory + " AND";
            if (intSubCategory != 0) strSearchCondition = strSearchCondition + " IM.SubCategoryID =" + intSubCategory + " AND";
            if (intItemName != 0) strSearchCondition = strSearchCondition +" IM.ItemID=" + intItemName + " AND";
            if (strBatch == "ALL")
            {
                strSearchCondition = strSearchCondition + "";
            }
            else
            {
                strSearchCondition = strSearchCondition + " IB.BatchNo ='" + strBatch + "' AND";
            }

            //if (chk == 0) strSearchCondition = strSearchCondition + " SM.LastModifiedDate between " + "(" + "'" + strFrom + "'" + ")" + " AND " + "(" + "'" + strTo + "'" + ")" + " AND";
            //else if (chk == 1)
            //{
            //    strFrom = string.Empty;
            //    strTo = string.Empty;
            //    strSearchCondition = strSearchCondition + "";
            //}
            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 2));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable(strProcedureStockDetails, prmReportDetails);

        }

        public DataTable DisplayAllReorderLevelStockReport(int intCompany, int intWarehouse, int intCategory, int intSubCategory, int intItemName, string strBatch)
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";

            if (intCompany != 0) strSearchCondition = " SD.CompanyID=" + intCompany + " AND";
            if (intWarehouse != 0) strSearchCondition = strSearchCondition + " SD.WarehouseID =" + intWarehouse + " AND";
            if (intCategory != 0) strSearchCondition = strSearchCondition + " IM.CategoryID =" + intCategory + " AND";
            if (intSubCategory != 0) strSearchCondition = strSearchCondition + " IM.SubCategoryID =" + intSubCategory + " AND";
            if (intItemName != 0) strSearchCondition = strSearchCondition + " IM.ItemID=" + intItemName + " AND";
            if (strBatch == "ALL")
            {
                strSearchCondition = strSearchCondition + "";
            }
            else
            {
                strSearchCondition = strSearchCondition + " IB.BatchNo ='" + strBatch + "' AND";
            }

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 3));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable(strProcedureStockDetails, prmReportDetails);
        }

        public DataTable DisplayReorderLevelStockReport(int intCompany, int intWarehouse, int intCategory, int intSubCategory, int intItemName, string strBatch)
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";

            if (intCompany != 0) strSearchCondition = " SD.CompanyID=" + intCompany + " AND";
            if (intWarehouse != 0) strSearchCondition = strSearchCondition + " SD.WarehouseID =" + intWarehouse + " AND";
            if (intCategory != 0) strSearchCondition = strSearchCondition + " IM.CategoryID =" + intCategory + " AND";
            if (intSubCategory != 0) strSearchCondition = strSearchCondition + " IM.SubCategoryID =" + intSubCategory + " AND";
            if (intItemName != 0) strSearchCondition = strSearchCondition + " IM.ItemID=" + intItemName + " AND";
            if (strBatch == "ALL")
            {
                strSearchCondition = strSearchCondition + "";
            }
            else
            {
                strSearchCondition = strSearchCondition + " IB.BatchNo ='" + strBatch + "' AND";
            }

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable(strProcedureStockDetails, prmReportDetails);
        }

        public DataTable DisplayLocationwiseStockReport(int intCompany, int intWarehouse, int intCategory, int intSubCategory, int intItemName, string strBatch)
        {
            prmReportDetails = new ArrayList();
            string strSearchCondition = "";

            if (intCompany != 0) strSearchCondition = " ITML.CompanyID=" + intCompany + " AND";
            if (intWarehouse != 0) strSearchCondition = strSearchCondition + " ITML.WarehouseID=" + intWarehouse + " AND";
            if (intCategory != 0) strSearchCondition = strSearchCondition + " CAT.CategoryID=" + intCategory + " AND";
            if (intSubCategory != 0) strSearchCondition = strSearchCondition + " SCAT.SubCategoryID=" + intSubCategory + " AND";
            if (intItemName != 0) strSearchCondition = strSearchCondition + " ITML.ItemID=" + intItemName + " AND";
            if (strBatch == "ALL")
            {
                strSearchCondition = strSearchCondition + "";
            }
            else
            {
                strSearchCondition = strSearchCondition + " BD.BatchNo='" + strBatch + "' AND";
            }

            if (strSearchCondition.Length > 3) strSearchCondition = strSearchCondition.Substring(0, strSearchCondition.Length - 3);

            prmReportDetails.Add(new SqlParameter("@Mode", 5));
            prmReportDetails.Add(new SqlParameter("@SearchCondition", strSearchCondition));
            return objclsConnection.ExecuteDataTable(strProcedureStockDetails, prmReportDetails);
        }
        public DataTable DisplayCompanyHeaderReport(int CompanyId) // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyId));
            return objclsConnection.ExecuteDataTable(strProcedurePurchaseModule, prmReportDetails);
        }

        public DataTable DisplayALLCompanyHeaderReport() // for company report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 32));
            return objclsConnection.ExecuteDataTable(strProcedurePurchaseModule, prmReportDetails);

        }
        public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 4));
            prmReportDetails.Add(new SqlParameter("@RoleID", intRoleID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@ControlID", intControlID));
            return objclsConnection.ExecuteDataTable(strProcedurePermissionSettings, prmReportDetails);
        }
    }
}
