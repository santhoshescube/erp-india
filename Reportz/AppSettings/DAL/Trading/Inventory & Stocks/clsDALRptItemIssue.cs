﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/* 
=================================================
Author:		    Sanju
Create date:    07 May 2012
Description:	Delivery Note,Material Issue, Material Return Reports DAL
================================================
*/
namespace MyBooksERP
{
   public  class clsDALRptItemIssue
   {
       public DataLayer objclsConnection { get; set; }
        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }
            }
            return null;
        }
        public DataTable GetReportDetails(int intType,string strFilterCondition)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Type", intType));
            alParameters.Add(new SqlParameter("@SearchCondition",strFilterCondition));
            return objclsConnection.ExecuteDataSet("spInvRptItemIssue", alParameters).Tables[0];
        }
        public DataTable DisplayCompanyHeaderReport(int CompanyId) // for company report
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyId));
            return objclsConnection.ExecuteDataTable("spInvPurchaseModuleFunctions", prmReportDetails);
        }
    }
}
