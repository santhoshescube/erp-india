﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

public class clsDALRptCTC
{
    public static DataSet GetEmployeeCTCReport(int CompanyID,int BranchID,bool IncludeCompany, int EmployeeID, DateTime? Date, string FinYear)
    {
        List<SqlParameter> sqlParameters = new List<SqlParameter>();
        sqlParameters.Add(new SqlParameter("@Mode", 1));
        sqlParameters.Add(new SqlParameter("@CompanyID", CompanyID ));
        sqlParameters.Add(new SqlParameter("@BranchID", BranchID));
        sqlParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        sqlParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        sqlParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        if (Date != null)
            sqlParameters.Add(new SqlParameter("@Date", Date));
        if (FinYear != "")
            sqlParameters.Add(new SqlParameter("@FinYear", FinYear));
        return new DataLayer().ExecuteDataSet("spRPTCTC", sqlParameters);
    }
}
   
       
