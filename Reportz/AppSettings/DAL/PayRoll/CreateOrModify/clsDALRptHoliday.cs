﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALRptHoliday
    {
        public static DataTable GetReport(int CompanyID, DateTime FromDate, DateTime ToDate)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@FromDate",FromDate),
                new SqlParameter("@ToDate", ToDate)
                     };
            return new DataLayer().ExecuteDataTable("spRptHoliday", sqlParameters);
        }
    }
}
