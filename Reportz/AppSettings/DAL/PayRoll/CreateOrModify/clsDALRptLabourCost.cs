﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsDALRptLabourCost
    {

        public static DataSet GetReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int WorkStatusID, int EmployeeID, bool IsMonth, int Month, int Year, DateTime FinYearStartDate, DateTime FinYearEndDate, DateTime MonthEndDate)
        {
             List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID),
                new SqlParameter("@IncludeCompany",IncludeCompany),
                new SqlParameter("@DepartmentID",DepartmentID) ,
                new SqlParameter("@WorkStatusID",WorkStatusID) ,
                new SqlParameter("@EmployeeID",EmployeeID),
                new SqlParameter("@IsMonth",IsMonth),
                new SqlParameter("@Month",Month),
                new SqlParameter("@Year",Year),
                new SqlParameter("@FinYearStartDate",FinYearStartDate),
                new SqlParameter("@FinYearEndDate", FinYearEndDate),
                new SqlParameter("@MonthEndDate", MonthEndDate),
                  new SqlParameter("@UserID",ClsCommonSettings.UserID)


                     };
             return new DataLayer().ExecuteDataSet("spPayRptLabourCost", sqlParameters);
        }
    }
}
