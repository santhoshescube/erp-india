﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;



public class clsDALReportCommon
{

    public static DataTable GetAllCompanies()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 1));
       // prmReportDetails.Add(new SqlParameter("@CompanyID", 1));
        prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    public static DataTable GetAllCompaniesMulti()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 1));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    public static DataTable GetAllBranchesByCompany(int CompanyID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 2));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetAllEmployees(int CompanyID,int BranchID,bool IncludeCompany)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, null, null, null, null, null, null, null, false,null,null);

    }

    public static DataTable GetEmployeesByLocation(int CompanyID, int BranchID, bool IncludeCompany, int WorkLocationID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany,null, null, null, null, WorkLocationID, null, null,false,null,null);
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DocumentTypeID, string DocumentNumber)
    {

        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 16));
        prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));

         prmReportDetails.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        if (DocumentNumber.Trim().ToUpper() == "-1")
        {
        }
        else
            prmReportDetails.Add(new SqlParameter("@DocumentNumber", DocumentNumber));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID ));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));


        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany,int DepartmentID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, null, null, null, null, null, null, false,null,null);
    }



    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, bool OnlySettledEmployees)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, null, null, null, null, null, OnlySettledEmployees,null,null);
    }


    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID,int DesignationID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, null, null, null, null, null, false,null,null);
    }


    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID,int EmploymentTypeID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, null, null, null, null, false,null,null);
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID, null, null, null, false,null,null);
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID,int WorkLocationID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID, WorkLocationID, null, null, false,null,null);
    }

    public static DataTable GetAllEmployeesArb(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID, int WorkLocationID)
    {
        return LoadEmployeesArb(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID, WorkLocationID, null, null, false);
    }

    private static DataTable LoadEmployees(int CompanyID, int BranchID, bool IncludeCompany, int? DepartmentID, int? DesignationID, int? EmploymentTypeID, int? WorkStatusID, int? WorkLocationID, int? DocumentTypeID, string DocumentNumber, bool LoadOnlySettlementEmployees, int? CountryID, int? ReligionID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 3));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        if (DepartmentID != null && DepartmentID > 0)
            prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
        if (DesignationID != null && DesignationID > 0)
            prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
        if (EmploymentTypeID != null && EmploymentTypeID > 0)
            prmReportDetails.Add(new SqlParameter("@EmploymentTypeID", EmploymentTypeID));
        if (WorkStatusID != null && WorkStatusID > 0)
            prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));

        if (WorkLocationID != null && WorkLocationID > 0)
            prmReportDetails.Add(new SqlParameter("@WorkLocationID", WorkLocationID));

        if (DocumentTypeID != null && DocumentTypeID  > 0)
            prmReportDetails.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID ));


        if (DocumentNumber != null && DocumentNumber.ToInt32() > 0)
            prmReportDetails.Add(new SqlParameter("@DocumentNumber", DocumentNumber));

        if (CountryID != null && CountryID.ToInt32() > 0)
            prmReportDetails.Add(new SqlParameter("@CountryID", CountryID));

        if (ReligionID != null && ReligionID.ToInt32() > 0)
            prmReportDetails.Add(new SqlParameter("@ReligionID", ReligionID));

        if (LoadOnlySettlementEmployees == true)
        {
            prmReportDetails.Add(new SqlParameter("@LoadSettledEmployees", LoadOnlySettlementEmployees));
        }

        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    private static DataTable LoadEmployeesArb(int CompanyID, int BranchID, bool IncludeCompany, int? DepartmentID, int? DesignationID, int? EmploymentTypeID, int? WorkStatusID, int? WorkLocationID, int? DocumentTypeID, string DocumentNumber, bool LoadOnlySettlementEmployees)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 39));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));


        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));

        if (DepartmentID != null && DepartmentID > 0)
            prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
        if (DesignationID != null && DesignationID > 0)
            prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
        if (EmploymentTypeID != null && EmploymentTypeID > 0)
            prmReportDetails.Add(new SqlParameter("@EmploymentTypeID", EmploymentTypeID));
        if (WorkStatusID != null && WorkStatusID > 0)
            prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));

        if (WorkLocationID != null && WorkLocationID > 0)
            prmReportDetails.Add(new SqlParameter("@WorkLocationID", WorkLocationID));

        if (DocumentTypeID != null && DocumentTypeID > 0)
            prmReportDetails.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));


        if (DocumentNumber != null && DocumentNumber.ToInt32() > 0)
            prmReportDetails.Add(new SqlParameter("@DocumentNumber", DocumentNumber));


        if (LoadOnlySettlementEmployees == true)
        {
            prmReportDetails.Add(new SqlParameter("@LoadSettledEmployees", LoadOnlySettlementEmployees));
        }

        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }


    public static DataTable GetAllDepartments()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 5));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllDesignation()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 4));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetAllEmploymentTypes()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 6));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllWorkStatus()
    {
        ArrayList prmReportDetails = new ArrayList();

        prmReportDetails.Add(new SqlParameter("@Mode", 7));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetReligion()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 48));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    public static DataTable GetCountry()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 47));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    public static DataTable GetNationality()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 49));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    public static DataTable GetAllWorkLocations(int CompanyID, int BranchID, bool IncludeCompany)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 8));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetCompanyHeader(int CompanyID,int BranchID,bool IncludeCompany)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 9));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }


    public static DataTable GetAllDocumentTypes()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 10));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllDocumentNumbers(int CompanyID,int BranchID,bool IncludeCompany,int DocumentTypeID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 11));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID ));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        prmReportDetails.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllDeductions()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 12));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllAssetTypes()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 13));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    public static DataTable GetAllAssets(int AssetTypeID, int CompanyID, int BranchID, bool IncludeCompany)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 14));
        prmReportDetails.Add(new SqlParameter("@BenefitTypeID", AssetTypeID));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllFinancialYears(int intCompanyID, int intBranchID, bool blnIncludeCompany)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 32));
        prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", intBranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", blnIncludeCompany));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetPaymentMonthYear(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 17));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
        prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
        prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));
       return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }
    public static DataTable GetAllExpenseTypes()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 18));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    public static DataTable GetAllSettlementTypes()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 20));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetMonthForAttendance(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 31));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
        prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
        prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    public static DataTable GetAllAssetStatus(int AssetSearchType)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 33));
        prmReportDetails.Add(new SqlParameter("@Type", AssetSearchType));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }
    public static DataTable GetCompaniesandBranches()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 34));
        prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }
    public static DataTable GetEmployeeForTransfer()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 35));
        prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    public static bool IsCompanyPermissionExists(int CompanyID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 50));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteScalar("SpReportsCommon", prmReportDetails).ToInt32() > 0;
    }
    //Without transfer join fill employees in current working company
    private static DataTable LoadEmployeesIncurrentCompany(int CompanyID, int BranchID, bool IncludeCompany, int? DepartmentID, int? DesignationID, int? EmploymentTypeID, int? WorkStatusID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 36));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));

        if (DepartmentID != null && DepartmentID > 0)
            prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
        if (DesignationID != null && DesignationID > 0)
            prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
        if (EmploymentTypeID != null && EmploymentTypeID > 0)
            prmReportDetails.Add(new SqlParameter("@EmploymentTypeID", EmploymentTypeID));
        if (WorkStatusID != null && WorkStatusID > 0)
            prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));

  

        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllEmployeesInCurrentCompany(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID)
    {
        return LoadEmployeesIncurrentCompany(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID);
    }
    public static DataTable GetTransactionType()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 40));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }
    public static DataTable GetProject()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 41));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

}

