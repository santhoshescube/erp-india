﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    class clsDALRptSalaryStructure
    {
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALRptSalaryStructure() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public static DataTable GetSalaryStructureSummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@DesignationID", DesignationID) ,
                new SqlParameter("@WorkStatusID", WorkStatusID) ,
                new SqlParameter("@EmploymentTypeID", EmploymentTypeID),
                new SqlParameter("@IncludeComp", IncludeComp),
                new SqlParameter("@EmployeeID",EmployeeID),
                 new SqlParameter("@UserID",ClsCommonSettings.UserID)
                  
                     };
            return new DataLayer().ExecuteDataTable("spPayRptSalaryStructure", sqlParameters);
        }

        public static DataTable GetSalaryStructureHistorySummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@DesignationID", DesignationID) ,
                new SqlParameter("@WorkStatusID", WorkStatusID) ,
                new SqlParameter("@EmploymentTypeID", EmploymentTypeID),
                new SqlParameter("@IncludeComp", IncludeComp),
                new SqlParameter("@EmployeeID",EmployeeID)
                  
                     };
            return new DataLayer().ExecuteDataTable("spPayRptSalaryStructure", sqlParameters);
        }

    }

}
