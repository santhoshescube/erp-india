﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;

namespace MyBooksERP
{

   public class clsDALSalarySlip
   {

      ArrayList prmSlip; 
      ClsCommonUtility MObjClsCommonUtility;
      DataLayer MobjDataLayer;

      public clsDTOSalarySlip PobjclsDTOSalarySlip { get; set; } // DTO Salary 

      public clsDALSalarySlip(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }


      public DataTable datMasterSum()
      {
          prmSlip = new ArrayList();
          prmSlip.Add(new SqlParameter("@PayID", PobjclsDTOSalarySlip.strPayID));
          prmSlip.Add(new SqlParameter("@CompanyPayFlag", PobjclsDTOSalarySlip.intCompanyPayFlag));
          prmSlip.Add(new SqlParameter("@DivisionID", PobjclsDTOSalarySlip.intDivisionID));
          return MobjDataLayer.ExecuteDataTable("spPayEmployeePaymentSalarySlipMaster", prmSlip);
      }
      public DataTable datMaster()
      {
          prmSlip = new ArrayList();
          prmSlip.Add(new SqlParameter("@PayID", PobjclsDTOSalarySlip.strPayID));
          prmSlip.Add(new SqlParameter("@CompanyPayFlag", PobjclsDTOSalarySlip.intCompanyPayFlag));
          prmSlip.Add(new SqlParameter("@DivisionID", PobjclsDTOSalarySlip.intDivisionID));
          return MobjDataLayer.ExecuteDataTable("spPayEmployeePaymentSalarySlipMaster", prmSlip);
      }
      public DataTable datDetail()
      {
          prmSlip = new ArrayList();
          prmSlip.Add(new SqlParameter("@PayID", PobjclsDTOSalarySlip.strPayID));
          prmSlip.Add(new SqlParameter("@Month", PobjclsDTOSalarySlip.intMonthInt));
          prmSlip.Add(new SqlParameter("@year", PobjclsDTOSalarySlip.intYearInt));
          return MobjDataLayer.ExecuteDataTable("spPayGetHourlyDetail", prmSlip);
      }
      public DataTable datDetailDed()
      {
          prmSlip = new ArrayList();
          prmSlip.Add(new SqlParameter("@PayID", PobjclsDTOSalarySlip.strPayID));
          prmSlip.Add(new SqlParameter("@Month", PobjclsDTOSalarySlip.intMonthInt));
          prmSlip.Add(new SqlParameter("@year", PobjclsDTOSalarySlip.intYearInt));
          return MobjDataLayer.ExecuteDataTable("spPayGetHourlyDetail", prmSlip);
      }

      public DataTable MdatLeaveDed()
      {
          prmSlip = new ArrayList();
          prmSlip.Add(new SqlParameter("@PaymentID", PobjclsDTOSalarySlip.strPayID));
          return MobjDataLayer.ExecuteDataSet("spPayGetLeaveDetailSalarySlip", prmSlip).Tables[0];
      }

      public DataTable MdatOTDed()
      {
          prmSlip = new ArrayList();
          prmSlip.Add(new SqlParameter("@PaymentID", PobjclsDTOSalarySlip.strPayID));
          return MobjDataLayer.ExecuteDataSet("spPayOTPolicyDetailList", prmSlip).Tables[0];
      }
      public DataTable MdatHourDetail()
      {
          prmSlip = new ArrayList();
          prmSlip.Add(new SqlParameter("@PayID", PobjclsDTOSalarySlip.strPayID));
          prmSlip.Add(new SqlParameter("@Month", PobjclsDTOSalarySlip.intMonthInt));
          prmSlip.Add(new SqlParameter("@year", PobjclsDTOSalarySlip.intYearInt));
          return MobjDataLayer.ExecuteDataTable("spPayGetHourlyDetail", prmSlip);
      }
      public DataTable MdatSalarySlipWorkInfo()
      {
          prmSlip = new ArrayList();
          prmSlip.Add(new SqlParameter("@PaymentID", PobjclsDTOSalarySlip.strPayID));
          return MobjDataLayer.ExecuteDataSet("spPayGetSalarySlipWorkInfo", prmSlip).Tables[0];
      }

      public DataTable CurrencySubType()
      {
          String sQuery = "SELECT CTB.CurrencySubType,CT.CurrencySubType FROM  CompanyMaster AS CM left join CurrencySubTypeReference CTB  on CM.CurrencyId =CTB.CurrencyID and CTB.Base=1 LEFT JOIN CurrencySubTypeReference CT  on CM.CurrencyId =CT.CurrencyID and CT.Base=0  Where CM.CompanyID=" + PobjclsDTOSalarySlip.intCompanyID + " ";
          return MobjDataLayer.ExecuteDataTable(sQuery);
      }

      public string GetPaymentIDList()
      {
          DataTable DT;
          prmSlip = new ArrayList();
          prmSlip.Add(new SqlParameter("@Type", 2));
          prmSlip.Add(new SqlParameter("@intYearInt", PobjclsDTOSalarySlip.intYearInt));
          prmSlip.Add(new SqlParameter("@intMonthInt", PobjclsDTOSalarySlip.intMonthInt));
          prmSlip.Add(new SqlParameter("@intCompanyID", PobjclsDTOSalarySlip.intCompanyID));
          prmSlip.Add(new SqlParameter("@intDepartmentID", PobjclsDTOSalarySlip.intDepartmentID));
          prmSlip.Add(new SqlParameter("@intDesignationID", PobjclsDTOSalarySlip.intDesignationID));
          prmSlip.Add(new SqlParameter("@WorkStatusID", PobjclsDTOSalarySlip.WorkStatusID));
          prmSlip.Add(new SqlParameter("@intEmployeeID", PobjclsDTOSalarySlip.intEmployeeID));
          prmSlip.Add(new SqlParameter("@BranchID", PobjclsDTOSalarySlip.BranchID));
          prmSlip.Add(new SqlParameter("@IncludeCompany", PobjclsDTOSalarySlip.IncludeCompany));
          DT = MobjDataLayer.ExecuteDataTable("spPayGetInfoWebformMain", prmSlip);

          if (DT.Rows.Count > 0)
          {
              return DT.Rows[0][0].ToString().Trim();
          }
          else
          {
              return "";
          }

      }


      public DataTable FillCombos(string[] saFieldValues)
      {
          // function for getting datatable for filling combo
          if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
          {
              ArrayList prmCommon = new ArrayList();
              prmCommon.Add(new SqlParameter("@Mode", "0"));
              prmCommon.Add(new SqlParameter("@Fields", saFieldValues[0]));
              prmCommon.Add(new SqlParameter("@TableName", saFieldValues[1]));
              prmCommon.Add(new SqlParameter("@Condition", saFieldValues[2]));

              return MobjDataLayer.ExecuteDataTable("spCommonReferenceTransaction", prmCommon);
              // return MObjClsCommonUtility.FillCombos(saFieldValues);
          }
          else
              return null;
      }

      public DataTable FillCombos(string sQuery)
      {
          // function for getting datatable for filling combo

          return MobjDataLayer.ExecuteDataTable(sQuery);

      }


      public DataTable GetCompanyDetail()
      {
          prmSlip = new ArrayList();
          prmSlip.Add(new SqlParameter("@ids", PobjclsDTOSalarySlip.intCompanyID));
          return MobjDataLayer.ExecuteDataSet("spPayGaReportCompanyForm", prmSlip).Tables[0];
      }




      public DataTable GetPayment()
      {
          prmSlip = new ArrayList();
          prmSlip.Add(new SqlParameter("@CompanyID", PobjclsDTOSalarySlip.intCompanyID));
          prmSlip.Add(new SqlParameter("@ProcessDate", PobjclsDTOSalarySlip.strProcessDate));
          return MobjDataLayer.ExecuteDataSet("spPayDisplayWordReportMaster", prmSlip).Tables[0];
      }



   }

}
