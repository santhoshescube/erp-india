﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace MyBooksERP
{
    public class clsDALRptPayments
    {
        public static DataTable GetMonthlyPayments(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@Month", Month),
                new SqlParameter("@Year", Year),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", 0),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                new SqlParameter("@IsPayStructureOnly", IsPayStructureOnly),
                 

                  
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }

        public static DataTable GetSummaryPayments(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, DateTime FromDate, DateTime ToDate, bool IncludeCompany, int TransactionTypeID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@FromDate", FromDate.AddDays(-FromDate.Day+1)),
                new SqlParameter("@ToDate", ToDate.AddMonths(1).AddDays(-ToDate.Day)),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", 0),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                 new SqlParameter("@UserID", ClsCommonSettings.UserID)
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }




    }
}
