﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace MyBooksERP
{
/*****************************************************
* Created By       : Arun
* Creation Date    : 24 Apr 2012
* Description      : Handle Attendance Reports Summary

* ******************************************************/
    public class clsDALAttendanceReport
    {

       private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALAttendanceReport() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
       /// <summary>
       /// Filling Employees Combos
       /// </summary>
       /// <param name="intCompanyID"></param>
       /// <param name="intBranchID"></param>
       /// <returns></returns>
        public static DataTable GetReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, bool IsMonth, DateTime AttendanceDate, int Month, int Year)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@BranchID", BranchID));
            alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
            alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
            alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@IsMonth", IsMonth));
            alParameters.Add(new SqlParameter("@Date", AttendanceDate));
            alParameters.Add(new SqlParameter("@Month", Month));
            alParameters.Add(new SqlParameter("@Year", Year));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataTable("spPayRptAttendace", alParameters);
        }

        public static DataSet GetPunchingReport(int EmployeeID, DateTime AttendanceDate)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 2));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@Date", AttendanceDate));
            return new DataLayer().ExecuteDataSet("spPayRptAttendace", alParameters);
        }

        public static DataTable GetSummaryReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 3));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@BranchID", BranchID));
            alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
            alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
            alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@Month", Month));
            alParameters.Add(new SqlParameter("@Year", Year));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataTable("spPayRptAttendace", alParameters);
        }

        public static DataTable GetEmployeeAttendnaceAllEmployee(int intMode, int intCompanyID, int intBranchID, int intEmployeeID, string strDate, int intIncludeCompany, int intLocationID)
        {
            //intMode=5 All Employee in one month
            //intMode=9 All Employee in one Day
            List<SqlParameter> sqlParameters = null;
            sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", intMode),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID", intBranchID) ,
                 new SqlParameter("@EmployeeID", intEmployeeID) ,
                 new SqlParameter("@Date", strDate),
                  new SqlParameter("@IncludeComp", intIncludeCompany),
                          new SqlParameter("@LocationID", intLocationID),
                          new SqlParameter("@UserID", ClsCommonSettings.UserID),
                  };
            return new DataLayer().ExecuteDataTable("spPayRptAttendanceSummary", sqlParameters);

        }
        /// <summary>
        /// Getting day Summary of a prticular day
        /// </summary>
        /// <param name="intCompanyID"></param>
        /// <param name="intEmployeeID"></param>
        /// <param name="intDayID"></param>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static DataTable GetEmployeeDetailsAttendance(int intEmployeeID, int intDayID, string strDate, int intLocationID)
        {
            //new SqlParameter("@CompanyID",intCompanyID),
            List<SqlParameter> sqlParameters = null;
            sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 6),
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@DayID", intDayID) ,
                new SqlParameter("@Date", strDate),
                     new SqlParameter("@LocationID", intLocationID)
                 
                  };
            return new DataLayer().ExecuteDataTable("spPayRptAttendanceSummary", sqlParameters);

        }
        /// <summary>
        /// Entry exit details of a particular day
        /// </summary>
        /// <param name="intCompanyID"></param>
        /// <param name="intEmployeeID"></param>
        /// <param name="intDayID"></param>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static DataTable GetEmployeeDayEntryExitDetails(int intEmployeeID, string strDate, int intLocationID)
        {//new SqlParameter("@CompanyID",intCompanyID), 
            List<SqlParameter> sqlParameters = null;
            sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 7),
                new SqlParameter("@EmployeeID", intEmployeeID),
                 new SqlParameter("@Date", strDate),
                    new SqlParameter("@LocationID", intLocationID)
                  };
            return new DataLayer().ExecuteDataTable("spPayRptAttendanceSummary", sqlParameters);
        }

        public static DataTable GetEmployeeSummary(int intCompanyID, int intEmployeeID, int intBranchID, string strStartDate, string strEndDate, int intIncludeCompany, int intLocationID)
        {
            List<SqlParameter> sqlParameters = null;
            sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 8),
                new SqlParameter("@CompanyID",intCompanyID), 
                 new SqlParameter("@EmployeeID", intEmployeeID),
                  new SqlParameter("@BranchID", intBranchID),
                 new SqlParameter("@StartDate", strStartDate),
                 new SqlParameter("@EndDate", strEndDate),
                  new SqlParameter("@IncludeComp", intIncludeCompany),
                    new SqlParameter("@LocationID", intLocationID),
                    new SqlParameter("@UserID", ClsCommonSettings.UserID),
                                   };
            return new DataLayer().ExecuteDataTable("spPayRptAttendanceSummary", sqlParameters);
        }


    }
}
