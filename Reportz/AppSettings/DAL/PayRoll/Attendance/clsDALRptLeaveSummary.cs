﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyBooksERP
{
    public class clsDALRptLeaveSummary
    {
        public clsDALRptLeaveSummary() { }

        public static DataTable GetLeaveReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int WorkStatusID, int EmployeeID, DateTime FromDate, DateTime ToDate)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@BranchID", BranchID));
            alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
            alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
            return new DataLayer().ExecuteDataTable("spPayRptLeaveSummary", alParameters);
        }

        public static DataTable GetLeaveSummaryReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int WorkStatusID, int EmployeeID, string FinYearStartDate)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 2));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@BranchID", BranchID));
            alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
            alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@FinYearStartDate", FinYearStartDate));
            return new DataLayer().ExecuteDataTable("spPayRptLeaveSummary", alParameters);
        }




    }
}
