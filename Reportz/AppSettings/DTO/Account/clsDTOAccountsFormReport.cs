﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    /***********************************************************
    * Author       : Junaid
    * Created On   : 13 APRL 2012
    * Purpose      : Create Opening Balance for Accounts
    *                
    * **********************************************************/
    public class clsDTOAccountsFormReport
    {
        public int intTempMenuID { get; set; }
        public int intTempAccountID { get; set; }
        public int intTempCompanyID { get; set; }
        public DateTime dtpTempFromDate { get; set; }
        public DateTime dtpTempToDate { get; set; }
        
    }
}
