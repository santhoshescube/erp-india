﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,25 Feb 2011>
Description:	<Description,,DTO for ReportViewer>
================================================
*/
namespace MyBooksERP
{

   public class clsDTOReportviewer
    {
       //public int intmode { get; set; }
       public Int64 intRecId { get; set; }
       public string strType { get; set; }
       public string strDate { get; set; }
       public int intExecutive { get; set; }
       public int intCompany { get; set; }
       public int intOperation { get; set; }
       public int intStatusID { get; set; } // DocDocumentstatus.Receipt or issue
       public int intTempMenuID { get; set; }
       public int intTempCompanyID { get; set; }
       public int intTempAccountID { get; set; }
       public DateTime dtpTempFromDate { get; set; }
       public DateTime dtpTempToDate { get; set; }
       public bool IsTempGroup { get; set; }
       public string strFromDate { get; set; }
       public string strToDate { get; set; }
       public int intAlertType { get; set; }
       public int intCompanyID { get; set; }
       public string strSearchKey { get; set; }
       public int intPageIndex { get; set; }
       public int intPageSize { get; set; }
       public bool IsExpenseListInSales { get; set; }
   }
}


