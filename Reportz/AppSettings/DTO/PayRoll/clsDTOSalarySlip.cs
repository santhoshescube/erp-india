﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
    public class clsDTOSalarySlip
    {

        public int intTemplateID { get; set; }
        public int intProcessYear { get; set; }
        public int intBankNameID { get; set; }
        public int intTransactionTypeID { get; set; }

        public int intYearInt { get; set; }
        public int intMonthInt { get; set; }
        public int intCompanyID { get; set; }
        public int intDepartmentID { get; set; }
        public int intDesignationID { get; set; }
        public int intEmployeeID { get; set; }
        public int BranchID { get; set; }

        public bool IncludeCompany { get; set; }


        public int WorkStatusID { get; set; }

        public int intCurrencyID { get; set; }
        public int intDivisionID { get; set; }
        public int intPaymentID { get; set; }
        public int intCompanyPayFlag { get; set; }
        public string strPayID { get; set; }
        public string strProcessDate { get; set; }
        public string strFromDate1 { get; set; }
        public string strToDate1 { get; set; }
        public string strHostName { get; set; }
       


      
    }
}
