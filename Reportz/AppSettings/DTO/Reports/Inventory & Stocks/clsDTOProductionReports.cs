﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyBooksERP
{
   public class clsDTOProductionReports
    {
       public long JobOrderID { get; set; }
       public long SalesPersonID { get; set; }
       public long DesignerID { get; set; }
       public int StatusID { get; set; }
       public DateTime Date { get; set; }
       public bool IsDate { get; set; }
       public int VendorID { get; set; }
    }
}
