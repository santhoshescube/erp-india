﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*******************************************************
 * Author       : Amal Raj
 * Created On   : 11 April 2011
 * Purpose      : DTO FOR JOB ORDER
 * *****************************************************/
namespace MyBooksERP
{  
    public class clsDTOProductionProcess
    {
        /// <summary>
        /// Unique ID of the Job Order. Primary Key.
        /// </summary>
        public Int64 intJobOrderID { get; set; }
        /// <summary>
        ///Gets Or Sets JobOrder Number
        /// </summary>
        public string strJobOrderNo { get; set; }
        /// <summary>
        ///Gets Or Sets JobOrder Start Date
        /// </summary>
        public DateTime dteJobStartDate { get; set;}
        /// <summary>
        ///Gets Or Sets JobOrder End Date
        /// </summary>
        public DateTime dteJobEndDate { get; set;}
        /// <summary>
        ///Gets Or Sets VendorID
        /// </summary>
        public int intVendorID { get;set; }
        /// <summary>
        ///Gets Or Sets StatusID
        /// </summary>
        public int intStatusID { get; set; }
        /// <summary>
        ///Gets Or Sets Expected Delivery Date
        /// </summary>
        public DateTime dteExpectedDeliveryDate { get; set; }
        /// <summary>
        ///Gets Or Sets Production Scheduled Date
        /// </summary>
        public DateTime dteProductionScheduledDate { get; set; }
        /// <summary>
        ///Gets Or Sets Created By
        /// </summary>
        public int intCreatedBy { get; set; }
        /// <summary>
        ///Gets Or Sets CreatedDate
        /// </summary>
        public DateTime dteCreatedDate { get; set; }
        /// <summary>
        ///Gets Or Sets CompanyID
        /// </summary>
        public int intCompanyID { get; set; }
        /// <summary>
        ///Gets Or Sets Remarks
        /// </summary>
        public string strRemarks { get; set; }
        /// <summary>
        ///Gets Or Sets EmployeeName
        /// </summary>
        public string strEmployeeName { get; set; }
        /// <summary>
        ///Gets Or Sets JobOrder TypeID
        /// </summary>
        public int intJobOrderTypeID { get; set; }
        /// <summary>
        /// Gets or sets IsCancelled.
        /// </summary>
        public bool IsCancelled { get; set; }
        /// <summary>
        ///Gets Or Sets Cancellation Date
        /// </summary>
        public string dteCancellationDate { get; set; }
        /// <summary>
        /// Gets or sets Cancel Remarks.
        /// </summary>
        public string strCancelRemarks { get; set; }
        /// <summary>
        /// Gets or sets Cancelled By.
        /// </summary>
        public int intCancelledBy { get; set; }
        /// <summary>
        /// Gets or sets Cancelled Employee
        /// </summary>
        public string strCancelledEmployee { get; set; }
        ///<summary>
        ///Gets or sets Is Progress Entry In Percentage
        /// </summary>
        public int intIsProgressEntryInPercentage { get; set; }
        ///<summary>
        ///Gets or sets Is Man Hours In Summary
        /// </summary>
        public int intIsManHoursInSummary { get; set; }
        /// <summary>
        ///Gets Or Sets class for Job Order Product
        /// </summary>
        /// 
        ///<summary>
        ///Gets Or Sets ProductionInChargeID
        /// </summary>
        public int intProductionInChargeID { get; set; }
        /// <summary>
        /// Gets or sets Cancel Remarks.
        /// </summary>
        public string strCompanyName { get; set; }
                
        public int intPrdItemID { get; set; }
        public decimal decPrdQuantity { get; set; }
        public int intPrdUOMID { get; set; }
        
        /// <summary>
        ///Gets Or Sets class for Job Order Production Stages
        /// </summary>
        public List<clsDTOJobOrderProductionStages> lstDTOJobOrderProductionStages { get; set; }


        public clsDTOProductionProcess()
        {
            this.intJobOrderID = clsConstants.NullInt;
            this.intStatusID = clsConstants.NullInt;
            this.intVendorID = clsConstants.NullInt;
            this.dteJobEndDate = clsConstants.NullDate;
            this.dteJobStartDate = clsConstants.NullDate;
            this.strJobOrderNo = clsConstants.NullString;
            this.dteExpectedDeliveryDate = clsConstants.NullDate;
            this.dteProductionScheduledDate = clsConstants.NullDate;
            this.intCreatedBy = clsConstants.NullInt;
            this.dteCreatedDate = clsConstants.NullDate;
            this.intCompanyID = clsConstants.NullInt;
            this.strRemarks = clsConstants.NullString;
            this.strEmployeeName = clsConstants.NullString;
        }
    }
    
    public class clsDTOJobOrderProductionStages
    { 
        public int intJobOrderID{ get; set; }
        public int intPSProductionStageID{ get; set; }
        public string strPSProductionStage{ get; set; }
        public DateTime dtePSStartDate{ get; set; }
        public DateTime dtePSEndDate{ get; set; }
        public decimal decPSCompletedPercentage{ get; set; }
        public decimal decPSCompletedQuantity{ get; set; }
        public int intPSUOMID{ get; set; }
        public string strPSUOMName{ get; set; }
        public int intPSCreatedBy{ get; set; }
        public DateTime dtePSCreatedDate { get; set; }

        public clsDTOJobOrderProductionStages()
        {
            this.intJobOrderID = clsConstants.NullInt;
            this.intPSProductionStageID = clsConstants.NullInt;
            this.strPSProductionStage = clsConstants.NullString;
            this.dtePSStartDate = clsConstants.NullDate;
            this.dtePSEndDate = clsConstants.NullDate;
            this.decPSCompletedPercentage = clsConstants.NullDecimal;
            this.decPSCompletedQuantity = clsConstants.NullDecimal;
            this.intPSUOMID = clsConstants.NullInt;
            this.strPSUOMName = clsConstants.NullString;
            this.intPSCreatedBy = clsConstants.NullInt;
            this.dtePSEndDate = clsConstants.NullDate;
        }

    }
}
