﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using MyBooksERP;
using System.ComponentModel;
using Microsoft.Reporting.WinForms;



public  class Report : Form
{

   /// <summary>
   /// Created By Rajesh.R
   /// Created On 13-Aug-2013
    /// </summary>

    #region Properties
    public DataTable dtCompany { get; set; }
    private clsMessage ObjUserMessage = null;

    private clsMessage UserMessage
    {
        get
        {
            if (this.ObjUserMessage == null)
                this.ObjUserMessage = new clsMessage(FormID.EmployeeProfileMISReport);
            return this.ObjUserMessage;
        }
    }
    #endregion

    public void SetReportHeader(int CompanyId, int BranchId, bool IncludeCompany)
    {
        dtCompany = clsBLLReportCommon.GetCompanyHeader(CompanyId, BranchId, IncludeCompany);
    }



    public static DataTable  GetReportHeader(int CompanyId, int BranchId, bool IncludeCompany)
    {
        return  clsBLLReportCommon.GetCompanyHeader(CompanyId, BranchId, IncludeCompany);
    }

    public void SetReportDisplay(ReportViewer Report)
    {
        Report.SetDisplayMode(DisplayMode.PrintLayout);
        Report.ZoomMode = ZoomMode.Percent;
        Report.ZoomPercent = 100;

    }

    public void SetDefaultBranch(ComboBox cb)
    {
        cb.SelectedValue = 0;
    }

    public void SetDefaultWorkStatus(ComboBox cb)
    {
        cb.SelectedValue = 6;
    }

  
}



 


//#endif