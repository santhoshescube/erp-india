﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyBooksERP
{
    /***********************************************************
    * Author       : Junaid
    * Created On   : 13 APRL 2012
    * Purpose      : Create Opening Balance for Accounts
    *                
    * **********************************************************/
    public class clsBLLAccountsFormReport
    {
        private clsDALAccountsFormReport objclsDALAccountsFormReport;
        private DataLayer objDataLayer;
        public clsDTOAccountsFormReport objclsDTOAccountsFormReport { get; set; }

        public clsBLLAccountsFormReport()
        {
            this.objDataLayer = new DataLayer();
            this.objclsDALAccountsFormReport = new clsDALAccountsFormReport();
            this.objclsDTOAccountsFormReport = new clsDTOAccountsFormReport();
            this.objclsDALAccountsFormReport.objclsDTOAccountsFormReport = objclsDTOAccountsFormReport;

        }
        public DataTable FillCombos(string[] saFields)
        {
            try
            {
                return this.objclsDALAccountsFormReport.FillCombos(saFields);
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayCompanyHeader()
        {
            try
            {
                return objclsDALAccountsFormReport.DisplayCompanyHeader();
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayBalanceSheet()
        {
            try
            {
                return objclsDALAccountsFormReport.DisplayBalanceSheet();
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayProfitAndLossAccounts()
        {
            try
            {
                return objclsDALAccountsFormReport.DisplayProfitAndLossAccounts();
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayIncomeStatement()
        {
            try
            {
                return objclsDALAccountsFormReport.DisplayIncomeStatement();
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayCashFlow()
        {
            try
            {
                return objclsDALAccountsFormReport.DisplayCashFlow();
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayFundFlow()
        {
            try
            {
                return objclsDALAccountsFormReport.DisplayFundFlow();
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayStockSummary()
        {
            try
            {
                return objclsDALAccountsFormReport.DisplayStockSummary();
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayStatementofAccounts()
        {
            try
            {
                return objclsDALAccountsFormReport.DisplayStatementofAccounts();
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayStockSummaryQtywise()
        {
            try
            {
                return objclsDALAccountsFormReport.DisplayStockSummaryQtywise();
            }
            catch (Exception ex) { throw ex; }
        }
        public DataSet DisplayStockSummaryItemwise()
        {
            try
            {
                return objclsDALAccountsFormReport.DisplayStockSummaryItemwise();
            }
            catch (Exception ex) { throw ex; }
        }

        public DataSet DisplayStockLedger()
        {
            try
            {
                return objclsDALAccountsFormReport.DisplayStockLedger();
            }
            catch (Exception ex) { throw ex; }
        }
    }
}