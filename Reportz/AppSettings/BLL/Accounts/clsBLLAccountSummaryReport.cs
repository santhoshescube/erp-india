﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public  class clsBLLAccountSummaryReport
    {
        private clsDALAccountSummaryReport MobjclsDALAccountSummary;
        private DataLayer MobjDataLayer;
        public clsDTOSummaryReport MobjclsDTOAccountSummary { get; set; }

        public clsBLLAccountSummaryReport()
        {
          this. MobjDataLayer = new DataLayer();
          this. MobjclsDALAccountSummary = new clsDALAccountSummaryReport(MobjDataLayer);
          this. MobjclsDTOAccountSummary = new clsDTOSummaryReport();
          this. MobjclsDALAccountSummary.objDTOAccountSummary = MobjclsDTOAccountSummary;
        }
        //~clsBLLAccountSummary()
        //{
        //    this.MobjclsDALAccountSummary = null;
        //    this.MobjDataLayer = null;
        //}
 
        public DataSet DisplayAccountsSummaryReoprt()
        {
            return MobjclsDALAccountSummary.DisplayAccountsSummaryReoprt();
        }

        public DataSet DisplayCompanyHeader()
        {
            return MobjclsDALAccountSummary.DisplayCompanyHeader();
        }  
    }
}
