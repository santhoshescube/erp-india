﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLRptHoliday
    {
        public static DataTable GetReport(int CompanyID, DateTime FromDate, DateTime ToDate)
        {
            return clsDALRptHoliday.GetReport(CompanyID, FromDate, ToDate);
        }
    }
}
