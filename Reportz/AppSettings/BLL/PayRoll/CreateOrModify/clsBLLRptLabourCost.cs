﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLRptLabourCost
    {
        public static DataSet GetReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int WorkStatusID, int EmployeeID, bool IsMonth, int Month, int Year, DateTime FinYearStartDate, DateTime FinYearEndDate, DateTime MonthEndDate)
        {
            return clsDALRptLabourCost.GetReport(CompanyID, BranchID, IncludeCompany, DepartmentID, WorkStatusID, EmployeeID, IsMonth, Month, Year, FinYearStartDate, FinYearEndDate, MonthEndDate);
        }
    }
}
