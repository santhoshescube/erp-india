﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;


public abstract class clsBLLReportCommon
{
    public static DataTable GetAllCompanies()
    {
        return clsDALReportCommon.GetAllCompanies();
    }

    public static DataTable GetAllBranchesByCompanyID(int CompanyID)
    {
        return clsDALReportCommon.GetAllBranchesByCompany(CompanyID);
    }
    public static DataTable GetAllCompaniesMulti()
    {
        return clsDALReportCommon.GetAllCompaniesMulti();
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DocumentTypeID,string  DocumentNumber)
    {
        return clsDALReportCommon.GetAllEmployees(CompanyID, BranchID, IncludeCompany, DocumentTypeID,DocumentNumber  );
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany)
    {
        return clsDALReportCommon.GetAllEmployees(CompanyID, BranchID, IncludeCompany);
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID)
    {
        return clsDALReportCommon.GetAllEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID);
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID)
    {
        return clsDALReportCommon.GetAllEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID);
    }


    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID,bool OnlySettledEmployees)
    {
        return clsDALReportCommon.GetAllEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID,true );
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID)
    {
        return clsDALReportCommon.GetAllEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID);
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID,int WorkLocationID)
    {
        return clsDALReportCommon.GetAllEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID,WorkLocationID);
    }

    public static DataTable GetAllEmployeesArb(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID, int WorkLocationID)
    {
        return clsDALReportCommon.GetAllEmployeesArb(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID, WorkLocationID);
    }

    public static DataTable GetEmployeesByLocation(int CompanyID, int BranchID, bool IncludeCompany, int WorkLocationID)
    {
        return clsDALReportCommon.GetAllEmployees(CompanyID, BranchID, IncludeCompany,WorkLocationID);
    }

    public static DataTable GetAllDepartments()
    {
        return clsDALReportCommon.GetAllDepartments();
    }

    public static DataTable GetAllDesignation()
    {
        return clsDALReportCommon.GetAllDesignation();
    }

    public static DataTable GetAllEmploymentType()
    {
        return clsDALReportCommon.GetAllEmploymentTypes();
    }


    public static DataTable GetAllWorkStatus()
    {
        return clsDALReportCommon.GetAllWorkStatus();
    }
    public static DataTable GetNationality()
    {
        return clsDALReportCommon.GetNationality();
    }

    public static DataTable GetReligion()
    {
        return clsDALReportCommon.GetReligion();
    }
    public static DataTable GetAllWorkLocations(int CompanyID, int BranchID, bool IncludeCompany)
    {
        return clsDALReportCommon.GetAllWorkLocations(CompanyID,BranchID,IncludeCompany);
    }


    public static DataTable GetCompanyHeader(int CompanyID,int BranchID,bool IncludeCompany)
    {
        return clsDALReportCommon.GetCompanyHeader(CompanyID,BranchID,IncludeCompany);
    }


    public static DataTable GetAllDocumentTypes()
    {
        return clsDALReportCommon.GetAllDocumentTypes();
    }

    public static DataTable GetAllDocumentNumbers(int CompanyID,int BranchID,bool IncludeCompany, int DocumentTypeID)
    {
        return clsDALReportCommon.GetAllDocumentNumbers(CompanyID,BranchID,IncludeCompany, DocumentTypeID); 
    }

    public static DataTable GetAllDeductions()
    {
        return clsDALReportCommon.GetAllDeductions();
    }
    public static DataTable GetAllAssetTypes()
    {
        return clsDALReportCommon.GetAllAssetTypes();
    }
    public static DataTable GetAllAssets(int AssetTypeID, int CompanyID, int BranchID, bool IncludeCompany)
    {
        return clsDALReportCommon.GetAllAssets(AssetTypeID,CompanyID,BranchID,IncludeCompany);
    }
    public static DataTable GetAllFinancialYears(int intCompanyID, int intBranchID, bool blnIncludeCompany)
    {
        return clsDALReportCommon.GetAllFinancialYears(intCompanyID, intBranchID, blnIncludeCompany);
    }
    public static DataTable GetPaymentMonthYear(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID)
    {
        return clsDALReportCommon.GetPaymentMonthYear(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, WorkStatusID, EmployeeID);
    }
    public static DataTable GetAllExpenseTypes()
    {
        return clsDALReportCommon.GetAllExpenseTypes();
    }
    public static DataTable GetAllSettlementTypes()
    {
        return clsDALReportCommon.GetAllSettlementTypes();
    }

    public static DataTable GetMonthForAttendance(int CompanyID, int BranchID, bool IncludeCompany,int DepartmentID,int DesignationID, int WorkStatusID, int EmployeeID)
    {
        return clsDALReportCommon.GetMonthForAttendance(CompanyID, BranchID, IncludeCompany,DepartmentID,DesignationID, WorkStatusID, EmployeeID);
    }
    public static DataTable GetAllAssetStatus(int AssetSearchType)
    {
        return clsDALReportCommon.GetAllAssetStatus(AssetSearchType);
    }
    public static DataTable GetCompaniesandBranches()
    {
        return clsDALReportCommon.GetCompaniesandBranches();
    }
    public static DataTable GetEmployeeForTransfer()
    {
        return clsDALReportCommon.GetEmployeeForTransfer();
    }
    public static bool IsCompanyPermissionExists(int CompanyID)
    {
        return clsDALReportCommon.IsCompanyPermissionExists(CompanyID);
    }
    public static DataTable GetAllEmployeesInCurrentCompany(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID)
    {
        return clsDALReportCommon.GetAllEmployeesInCurrentCompany(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID);
    }
    public static DataTable GetTransactionType()
    {
        return clsDALReportCommon.GetTransactionType();
    }
    public static DataTable GetProject()
    {
        return clsDALReportCommon.GetProject();

    }
}


