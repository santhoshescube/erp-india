﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace MyBooksERP
{

    /*****************************************************  
* Created By       : Arun
* Creation Date    : 24 Apr 2012
* Description      : Handle Attendance Reports Summary

* ******************************************************/
    public class clsBLLAttendanceReport
    {
        clsDALAttendanceReport MobjDALAttendanceReport = null;

        public clsBLLAttendanceReport()
        {

        }

        private clsDALAttendanceReport DALAttendanceReport
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALAttendanceReport == null)
                    this.MobjDALAttendanceReport = new clsDALAttendanceReport();

                return this.MobjDALAttendanceReport;
            }
        }



        public static DataTable GetReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, bool IsMonth, DateTime AttendanceDate, int Month, int Year)
        {
            return clsDALAttendanceReport.GetReport(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, WorkStatusID, EmployeeID, IsMonth, AttendanceDate, Month, Year);
        }
        public static DataSet GetPunchingReport(int EmployeeID, DateTime AttendanceDate)
        {
            return clsDALAttendanceReport.GetPunchingReport(EmployeeID, AttendanceDate);
        }

        public static DataTable GetSummaryReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year)
        {
            return clsDALAttendanceReport.GetSummaryReport(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, WorkStatusID, EmployeeID, Month, Year);
        }
        public DataTable GetEmployeeDetailsAttendance(int intEmployeeID, int intDayID, string strDate, int intLocationID)
        {
            return clsDALAttendanceReport.GetEmployeeDetailsAttendance(intEmployeeID, intDayID, strDate, intLocationID);
        }
        public DataTable GetEmployeeDayEntryExitDetails(int intEmployeeID, string strDate, int intLocationID)
        {
            return clsDALAttendanceReport.GetEmployeeDayEntryExitDetails(intEmployeeID, strDate, intLocationID);
        }
        public DataTable GetEmployeeSummary(int intCompanyID, int intEmployeeID, int intBranchID, string strStartDate, string strEndDate, int intIncludeCompany, int intLocationID)
        {
            return clsDALAttendanceReport.GetEmployeeSummary(intCompanyID, intEmployeeID, intBranchID, strStartDate, strEndDate, intIncludeCompany, intLocationID);
        }
        public DataTable GetEmployeeAttendnaceAllEmployee(int intMode, int intCompanyID, int intBranchID, int intEmployeeID, string strDate, int intIncludeCompany, int intLocationID)
        {
            return clsDALAttendanceReport.GetEmployeeAttendnaceAllEmployee(intMode, intCompanyID, intBranchID, intEmployeeID, strDate, intIncludeCompany, intLocationID);
        }






    }
}
