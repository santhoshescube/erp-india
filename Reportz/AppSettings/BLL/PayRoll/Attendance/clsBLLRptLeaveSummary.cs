﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLRptLeaveSummary
    {

        /// <summary>
        /// Constructor
        /// </summary>
        public clsBLLRptLeaveSummary() { }

        public static DataTable GetLeaveReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int WorkStatusID, int EmployeeID, DateTime FromDate, DateTime ToDate)
        {
            return clsDALRptLeaveSummary.GetLeaveReport(CompanyID, BranchID, IncludeCompany, DepartmentID, WorkStatusID, EmployeeID, FromDate, ToDate);
        }

        public static DataTable GetLeaveSummaryReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int WorkStatusID, int EmployeeID, string FinYearStartDate)
        {
            return clsDALRptLeaveSummary.GetLeaveSummaryReport(CompanyID, BranchID, IncludeCompany, DepartmentID, WorkStatusID, EmployeeID, FinYearStartDate);
        }


    }
}
