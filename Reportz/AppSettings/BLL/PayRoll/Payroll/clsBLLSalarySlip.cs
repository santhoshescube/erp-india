﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;


namespace MyBooksERP
{
   public  class clsBLLSalarySlip
    {
       
        clsDALSalarySlip  MobjclsDALSalarySlip;
        clsDTOSalarySlip MobjclsDTOSalarySlip;

        private DataLayer MobjDataLayer;

        public clsBLLSalarySlip()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALSalarySlip = new clsDALSalarySlip(MobjDataLayer);
            MobjclsDTOSalarySlip = new clsDTOSalarySlip();
            MobjclsDALSalarySlip.PobjclsDTOSalarySlip = MobjclsDTOSalarySlip;
        }

        public clsDTOSalarySlip clsDTOSalarySlip
        {
            get { return MobjclsDTOSalarySlip; }
            set { MobjclsDTOSalarySlip = value; }
        }


        public DataTable datDetail()
        {
            return MobjclsDALSalarySlip.datDetail();
        }
        public DataTable datDetailDed()
        {
            return MobjclsDALSalarySlip.datDetailDed();
        }

        public DataTable datMaster()
        {
            return MobjclsDALSalarySlip.datMaster();
        }
        public DataTable datMasterSum()
        {
            return MobjclsDALSalarySlip.datMaster();
        }

        public DataTable MdatLeaveDed()
        {
            return MobjclsDALSalarySlip.MdatLeaveDed();
        }

        public DataTable MdatOTDed()
        {
            return MobjclsDALSalarySlip.MdatOTDed();
        }
        public DataTable MdatHourDetail()
        {
            return MobjclsDALSalarySlip.MdatHourDetail();
        }
        public DataTable MdatSalarySlipWorkInfo()
        {
            return MobjclsDALSalarySlip.MdatSalarySlipWorkInfo();
        }
        public DataTable CurrencySubType()
        {
            return MobjclsDALSalarySlip.CurrencySubType();
        }
        public string GetPaymentIDList()
        {
            return MobjclsDALSalarySlip.GetPaymentIDList();
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalarySlip.FillCombos(saFieldValues);
        }

        public DataTable FillCombos(string sQuery)
        {
            //function for getting datatable for filling combo
            return MobjclsDALSalarySlip.FillCombos(sQuery);
        }

        public DataTable GetCompanyDetail()
        {
            return MobjclsDALSalarySlip.GetCompanyDetail();
        }

        public DataTable GetPayment()
        {
            return MobjclsDALSalarySlip.GetPayment();
        }

    }
}
