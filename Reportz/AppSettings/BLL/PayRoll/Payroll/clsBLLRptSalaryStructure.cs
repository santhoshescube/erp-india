﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLRptSalaryStructure
    {

        clsDALRptSalaryStructure MobjDALRptSalaryStructure = null;

        public clsBLLRptSalaryStructure()
        {

        }

        private clsDALRptSalaryStructure DALRptSalaryStructure  //clsDALAttendanceReport DALAttendanceReport
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALRptSalaryStructure == null)
                    this.MobjDALRptSalaryStructure = new clsDALRptSalaryStructure();

                return this.MobjDALRptSalaryStructure;
            }
        }
        public static DataTable GetSalaryStructureSummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {

            return clsDALRptSalaryStructure.GetSalaryStructureSummary(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmploymentTypeID, IncludeComp, EmployeeID);
        }

        public static DataTable GetSalaryStructureHistorySummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {

            return clsDALRptSalaryStructure.GetSalaryStructureHistorySummary(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmploymentTypeID, IncludeComp, EmployeeID);
        }


    }
}
