﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLRptPayments
    {
        clsDALRptPayments MobjDALRptEmolyeeProfile = null;
        public clsBLLRptPayments() { }

        private clsDALRptPayments DALRptPayments  
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALRptEmolyeeProfile == null)
                    this.MobjDALRptEmolyeeProfile = new clsDALRptPayments();

                return this.MobjDALRptEmolyeeProfile;
            }
        }

        public static DataTable GetMonthlyPayments(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly)
        {
            return clsDALRptPayments.GetMonthlyPayments(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmployeeID, Month, Year, IncludeCompany, TransactionTypeID, IsPayStructureOnly);
        }

        public static DataTable GetSummaryPayments(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, DateTime FromDate, DateTime ToDate, bool IncludeCompany, int TransactionTypeID)
        {
            return clsDALRptPayments.GetSummaryPayments(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmployeeID, FromDate, ToDate, IncludeCompany, TransactionTypeID);
        }


    }
}
