﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace MyBooksERP
{/*****************************************************
* Created By       : Arun
* Creation Date    : 05 May 2012
* Description      : Handle Salary Advance Summary Report

* ******************************************************/

   public class clsBLLSalaryAdvanceReport
    {

        public clsBLLSalaryAdvanceReport() { }
        public static DataTable GetSalaryAdvanceOrLoanSummary(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID, int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate, int IsSalaryAdvanceReport)
        {
            return clsDALSalaryAdvanceReport.GetSalaryAdvanceOrLoanSummary(CompanyID, BranchID, DepartmentID, WorkStatusID, EmployeeID, IncludeCompany, FromDate, ToDate, IsSalaryAdvanceReport);
        }
        public static DataTable GetLoanDetails(int EmployeeID, DateTime FromDate, DateTime ToDate)
        {
            return clsDALSalaryAdvanceReport.GetLoanDetails(EmployeeID, FromDate, ToDate);

        }

    }
}
