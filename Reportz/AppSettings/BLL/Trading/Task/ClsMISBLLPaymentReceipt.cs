﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,11 July 2011>
Description:	<Description,,BLL for Payment/Receipt>
================================================
*/
namespace MyBooksERP
{
   public class ClsMISBLLPaymentReceipt
    {

          ClsMISDALPaymentReceipt objMISDALPaymentReceipt;
            private DataLayer objClsConnection;

            public ClsMISBLLPaymentReceipt()
            {

            objClsConnection = new DataLayer();
            objMISDALPaymentReceipt = new ClsMISDALPaymentReceipt();

             }
            public DataTable FillCombos(string[] sarFieldValues)
            {
                objMISDALPaymentReceipt.objclsConnection = objClsConnection;
                return objMISDALPaymentReceipt.FillCombos(sarFieldValues);
            }
            public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
            {
                objMISDALPaymentReceipt.objclsConnection = objClsConnection;
                return objMISDALPaymentReceipt.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
            }
            public DataTable DisplayPaymentReceiptReport(int intCompany, int intType, int intVendor, string strFrom, string strTo, int intTransactionType, int intPaymentNo, int intNo, int DateType, int chkFrom, int chkTo, string strPermittedCompany) //  PaymentReceipt Report
            {
                objMISDALPaymentReceipt.objclsConnection = objClsConnection;
                return objMISDALPaymentReceipt.DisplayPaymentReceiptReport(intCompany, intType, intVendor, strFrom, strTo, intTransactionType, intPaymentNo, intNo, DateType, chkFrom, chkTo, strPermittedCompany);
            }

            public DataTable DisplayCompanyHeaderReport(int CompanyID)
            {

                objMISDALPaymentReceipt.objclsConnection = objClsConnection;
                return objMISDALPaymentReceipt.DisplayCompanyHeaderReport(CompanyID);
            }


            public DataTable DisplayALLCompanyHeaderReport()
            {
                objMISDALPaymentReceipt.objclsConnection = objClsConnection;
                return objMISDALPaymentReceipt.DisplayALLCompanyHeaderReport();
            }

            public bool GenearlCustomerCheck()
            {
                objMISDALPaymentReceipt.objclsConnection = objClsConnection;
                return objMISDALPaymentReceipt.GenearlCustomerCheck();
            }

    }
}
