﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace MyBooksERP
{
    class clsBLLRptExpense
    {
        clsDALRptExpense MobjDALRptExpense = null;

        public clsBLLRptExpense()
        {

        }

        private clsDALRptExpense DALRptExpense  //clsDALAttendanceReport DALAttendanceReport
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALRptExpense == null)
                    this.MobjDALRptExpense = new clsDALRptExpense();

                return this.MobjDALRptExpense;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALRptExpense.FillCombos(saFieldValues);
        }

        public DataTable DisplayCompanyReportHeader(int intCompanyID)  //Company  Header
        {
            return DALRptExpense.DisplayCompanyReportHeader(intCompanyID);
        }


        public DataTable GetExpenseDetails(int intType, int intCompanyID, int intExpenseTypeID, int intDateType, DateTime dteFromDate, DateTime dteToDate)
        {
            return DALRptExpense.GetExpenseDetails(intType, intCompanyID, intExpenseTypeID, intDateType, dteFromDate, dteToDate);
        }
        public DataTable GetExpenseSummary(int intType, int intCompanyID, int intDateType, DateTime dteFromDate, DateTime dteToDate)
        {
            return DALRptExpense.GetExpenseSummary(intType,intCompanyID,intDateType, dteFromDate, dteToDate);
        }




    }
}
