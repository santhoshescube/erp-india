﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLRptProductGroup
    {
        clsDALRptProductGroup objclsDALRptProductGroup;
        private DataLayer objClsConnection;

        public clsBLLRptProductGroup()
        {
            objClsConnection = new DataLayer();
            objclsDALRptProductGroup = new clsDALRptProductGroup();
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            objclsDALRptProductGroup.objclsConnection = objClsConnection;
            return objclsDALRptProductGroup.FillCombos(sarFieldValues);
        }

        public DataTable GetCompanyHeader(int intCompanyID)
        {
            objclsDALRptProductGroup.objclsConnection = objClsConnection;
            return objclsDALRptProductGroup.GetCompanyHeader(intCompanyID);
        }

        public DataTable GetReport(int intCompanyID, int intItemGroupID, DateTime dtFromDate, DateTime dtToDate, int intchkFrmTo)
        {
            objclsDALRptProductGroup.objclsConnection = objClsConnection;
            return objclsDALRptProductGroup.GetReport(intCompanyID,intItemGroupID,dtFromDate,dtToDate,intchkFrmTo);
        }
    }
}
