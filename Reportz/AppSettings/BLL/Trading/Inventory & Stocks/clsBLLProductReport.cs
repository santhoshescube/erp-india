﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,27 April 2011>
Description:	<Description,,BLL for Product>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLProductReport
    {
       clsDALProductReport objclsDALProduct;
       private DataLayer objClsConnection;
        
       public clsBLLProductReport()
       {
         objClsConnection = new DataLayer();
         objclsDALProduct = new clsDALProductReport();
       }
       public DataTable FillCombos(string[] sarFieldValues)
       {
         objclsDALProduct.objclsConnection = objClsConnection;
         return objclsDALProduct.FillCombos(sarFieldValues);
       }
       public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
       {
           objclsDALProduct.objclsConnection = objClsConnection;
           return objclsDALProduct.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
       }
       public DataTable GetPermittedCompany(int intRoleID, int intCompanyID, int intControlID)
       {
           objclsDALProduct.objclsConnection = objClsConnection;
           return objclsDALProduct.GetPermittedCompany(intRoleID, intCompanyID, intControlID);
       }
       public DataTable DisplayALLProductReport(int intCompanyID, int intCategoryID, int intManufactureID, int intStatusID, int intWarehouseID,int intSubCategory, string strExpiryDate, string strPermittedCompany,int intFeature,int intFeatureValue) //ALL Product Report
       {
          objclsDALProduct.objclsConnection = objClsConnection;
          return objclsDALProduct.DisplayALLProductReport(intCompanyID, intCategoryID, intManufactureID, intStatusID, intWarehouseID, intSubCategory, strExpiryDate, strPermittedCompany,intFeature,intFeatureValue);
       }
       public DataTable DisplayALLPriceHistory(int intCompanyID, int intWarehouseID, int intCategoryID,int intSubCategory ,int intManufactureID, int intStatusID, int intchkFrom, int intchkTo, string strFrom, string strTo, string strPermittedCompany)
       { 
           objclsDALProduct.objclsConnection = objClsConnection;
           return objclsDALProduct.DisplayALLPriceHistory(intCompanyID, intWarehouseID, intCategoryID, intSubCategory, intManufactureID, intStatusID, intchkFrom, intchkTo, strFrom, strTo, strPermittedCompany);
       }
       public DataTable DisplayALLCompanyHeaderReport()
       {
           objclsDALProduct.objclsConnection = objClsConnection;
           return objclsDALProduct.DisplayALLCompanyHeaderReport();
       }
       public DataTable DisplayCompanyHeaderReport(int CompanyID)
       {
           objclsDALProduct.objclsConnection = objClsConnection;
           return objclsDALProduct.DisplayCompanyHeaderReport(CompanyID);
       }
    }
}
