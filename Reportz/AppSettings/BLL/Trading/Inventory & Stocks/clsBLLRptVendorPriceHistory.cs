﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLRptVendorPriceHistory
    {
        clsDALRptVendorPriceHistory objclsDALRptVendorPriceHistory;
        private DataLayer objClsConnection;

        public clsBLLRptVendorPriceHistory()
        {
            objclsDALRptVendorPriceHistory = new clsDALRptVendorPriceHistory();
            objClsConnection = new DataLayer();
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            objclsDALRptVendorPriceHistory.MobjclsConnection = objClsConnection;
            return objclsDALRptVendorPriceHistory.FillCombos(sarFieldValues);
        }

        public DataTable LoadItems(int intType)
        {
            objclsDALRptVendorPriceHistory.MobjclsConnection = objClsConnection;
            return objclsDALRptVendorPriceHistory.LoadItems(intType);
        }

        public DataTable GetReport(int intTypeID,int intItemID,bool blnIsGroup, DateTime dtFromDate,DateTime dtToDate, int intchkFrmTo)
        {
            objclsDALRptVendorPriceHistory.MobjclsConnection = objClsConnection;
            return objclsDALRptVendorPriceHistory.GetReport(intTypeID, intItemID, blnIsGroup, dtFromDate, dtToDate, intchkFrmTo);
        }
    }
}
