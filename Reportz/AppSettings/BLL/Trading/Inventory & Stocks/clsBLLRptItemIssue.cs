﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/* 
=================================================
Author:		    Sanju
Create date:    07 May 2012
Description:	Delivery Note,Material Issue, Material Return Reports BLL
================================================
*/
namespace MyBooksERP
{
    public class clsBLLRptItemIssue
    {
        clsDALRptItemIssue objclsDALRptItemIssue;
        private DataLayer objClsConnection;

        public clsBLLRptItemIssue()
        {
            objClsConnection = new DataLayer();
            objclsDALRptItemIssue = new clsDALRptItemIssue();
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            objclsDALRptItemIssue.objclsConnection = objClsConnection;
            return objclsDALRptItemIssue.FillCombos(sarFieldValues);
        }
        public DataTable GetReportDetails(int intType, string strFilterCondition)
        {
            objclsDALRptItemIssue.objclsConnection = objClsConnection;
            return objclsDALRptItemIssue.GetReportDetails(intType, strFilterCondition);
        }

        public DataTable DisplayCompanyHeaderReport(int CompanyId) // for company report
        {

            objclsDALRptItemIssue.objclsConnection = objClsConnection;
            return objclsDALRptItemIssue.DisplayCompanyHeaderReport(CompanyId);
        }
    }
}