﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsBLLRptProductMovement
    {

         clsDALRptProductMovement objDALRptProductMovement;

        public clsBLLRptProductMovement()
        {
            objDALRptProductMovement = new clsDALRptProductMovement();
        }

        public DataSet GetReport(int CompanyID, DateTime FromDate, DateTime ToDate, int intCategoryID, int intSubCategoryID, int intItemID)
        {
            return objDALRptProductMovement.GetReport(CompanyID, FromDate, ToDate, intCategoryID, intSubCategoryID, intItemID);
        }

        public DataTable GetCompany()
        {
            return objDALRptProductMovement.GetCompany();
        }

        public DataTable GetWareHouseByCompany(int intCompanyID)
        {
            return this.objDALRptProductMovement.GetWareHouseByCompany(intCompanyID);
        }

        public DataTable GetCategory()
        {
             return objDALRptProductMovement.GetCategory();
        }

        public DataTable GetSubCategoryByCategory(int intCategoryID)
        {
            return objDALRptProductMovement.GetSubCategoryByCategory(intCategoryID);
        }

        public SqlDataReader GetDetailsForChart(int CompanyID, DateTime FromDate, DateTime ToDate, int intCategoryID, int intSubCategoryID, int intItemID)
        {
            return objDALRptProductMovement.GetDetailsForChart(CompanyID, FromDate, ToDate, intCategoryID, intSubCategoryID, intItemID);
        }

        public DataTable GetItems(int intCompanyID, int intCategoryID, int intSubCategoryID)
        {
            return objDALRptProductMovement.GetItems(intCompanyID, intCategoryID, intSubCategoryID);
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            return objDALRptProductMovement.FillCombos(sarFieldValues);
        }
        public DataSet GetPurchaseSalesReport(int CompanyID, DateTime FromDate, DateTime ToDate, int intType, long lngReferenceID, int intReportType)
        {
            return objDALRptProductMovement.GetPurchaseSalesReport(CompanyID, FromDate, ToDate, intType, lngReferenceID, intReportType);
        }

        public DataTable GetReferenceNo(int intCompanyID, int intOperationTypeID, DateTime dtFromDate, DateTime dtToDate)
        {
            return objDALRptProductMovement.GetReferenceNo(intCompanyID,intOperationTypeID,dtFromDate,dtToDate);
        }
    }
}
