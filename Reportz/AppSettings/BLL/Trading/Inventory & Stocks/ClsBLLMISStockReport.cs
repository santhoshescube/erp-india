﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
/* 
================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,18 May 2011>
Description:	<Description,,BLL for Stock>
================================================
*/
namespace MyBooksERP
{
    public class ClsBLLMISStockReport
    {
         ClsDALMISStockReport objClsDALMISStock;
            private DataLayer objClsConnection;

            public ClsBLLMISStockReport()
            {

            objClsConnection = new DataLayer();
            objClsDALMISStock = new ClsDALMISStockReport();

             }
            public DataTable FillCombos(string[] sarFieldValues)
            {
                objClsDALMISStock.objclsConnection = objClsConnection;
                return objClsDALMISStock.FillCombos(sarFieldValues);
            }

            public DataTable DisplayCompanyHeaderReport(int CompanyID)
            {
                objClsDALMISStock.objclsConnection = objClsConnection;
                return objClsDALMISStock.DisplayCompanyHeaderReport(CompanyID);
            }

            public DataTable DisplayALLCompanyHeaderReport()
            {
                objClsDALMISStock.objclsConnection = objClsConnection;
                return objClsDALMISStock.DisplayALLCompanyHeaderReport();
            }

            public DataTable DisplayNormalStockReport(int intCompany, int intWarehouse,  int intCategory, int intSubCategory, int intItemName, string strBatch)
            {
                objClsDALMISStock.objclsConnection = objClsConnection;
                return objClsDALMISStock.DisplayNormalStockReport(intCompany,  intWarehouse, intCategory,  intSubCategory,  intItemName,  strBatch);
            }

            public DataTable DisplayZeroStock(int intCompany, int intWarehouse, int intCategory, int intSubCategory, int intItemName, string strBatch)
            {
                objClsDALMISStock.objclsConnection = objClsConnection;
                return objClsDALMISStock.DisplayZeroReport(intCompany, intWarehouse, intCategory, intSubCategory, intItemName, strBatch);
            }

            public DataTable DisplayAllReorderLevelStockReport(int intCompany, int intWarehouse, int intCategory, int intSubCategory, int intItemName, string strBatch)
            {
                objClsDALMISStock.objclsConnection = objClsConnection;
                return objClsDALMISStock.DisplayAllReorderLevelStockReport(intCompany, intWarehouse, intCategory, intSubCategory, intItemName, strBatch);
            }

            public DataTable DisplayReorderLevelStockReport(int intCompany, int intWarehouse, int intCategory, int intSubCategory, int intItemName, string strBatch)
            {
                objClsDALMISStock.objclsConnection = objClsConnection;
                return objClsDALMISStock.DisplayReorderLevelStockReport(intCompany, intWarehouse, intCategory, intSubCategory, intItemName, strBatch);
            }
            public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
            {
                objClsDALMISStock.objclsConnection = objClsConnection;
                return objClsDALMISStock.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
            }
            public DataTable DisplayLocationwiseStockReport(int intCompany, int intWarehouse, int intCategory, int intSubCategory, int intItemName, string strBatch)
            {
                objClsDALMISStock.objclsConnection = objClsConnection;
                return objClsDALMISStock.DisplayLocationwiseStockReport(intCompany, intWarehouse, intCategory, intSubCategory, intItemName, strBatch);
            }

        
    }
}
