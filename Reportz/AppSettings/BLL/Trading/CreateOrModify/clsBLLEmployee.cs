﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,28 April 2011>
Description:	<Description,,BLL for Employee>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLEmployee
    {

        clsDALEmployee objclsDALEmployee;
            private DataLayer objClsConnection;

            public clsBLLEmployee()
            {

            objClsConnection = new DataLayer();
            objclsDALEmployee = new clsDALEmployee();

             }
            public DataTable FillCombos(string[] sarFieldValues)
            {
                objclsDALEmployee.objclsConnection = objClsConnection;
                return objclsDALEmployee.FillCombos(sarFieldValues);
            }
            public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
            {
                objclsDALEmployee.objclsConnection = objClsConnection;
                return objclsDALEmployee.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
            }
            public DataTable DisplayALLEmployeeReport(int intCompany, int intDesignation, int intDepartment, int intWorkStatus, string strPermittedCompany) //ALL Employee Report
            {
                objclsDALEmployee.objclsConnection = objClsConnection;
                return objclsDALEmployee.DisplayALLEmployeeReport(intCompany, intDesignation, intDepartment, intWorkStatus, strPermittedCompany);
            }

            public DataTable DisplayCompanyHeaderReport(int CompanyID)
            {
                objclsDALEmployee.objclsConnection = objClsConnection;
                return objclsDALEmployee.DisplayCompanyHeaderReport(CompanyID);
            }

            public DataTable DisplayALLCompanyHeaderReport()
            {
                objclsDALEmployee.objclsConnection = objClsConnection;
                return objclsDALEmployee.DisplayALLCompanyHeaderReport();
            }

    }
}
