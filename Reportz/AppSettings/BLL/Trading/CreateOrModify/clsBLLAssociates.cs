﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,17 May 2011>
Description:	<Description,,BLL for Associative>
================================================
*/

namespace MyBooksERP 
{
    public class clsBLLAssociates
    {
        clsDALAssociates objclsDALAssociates;
            private DataLayer objClsConnection;

            public clsBLLAssociates()
            {

            objClsConnection = new DataLayer();
            objclsDALAssociates = new clsDALAssociates();
            }

            public DataTable FillCombos(string[] sarFieldValues)
            {
                objclsDALAssociates.objclsConnection = objClsConnection;
                return objclsDALAssociates.FillCombos(sarFieldValues);
            }

            public AutoCompleteStringCollection FillStringCollection(string[] sarFieldValues)
            {
                objclsDALAssociates.objclsConnection = objClsConnection;
                return objclsDALAssociates.FillStringCollection(sarFieldValues);
            }

            public DataTable DisplayAssociativeReport(int intType, string strType, int intTypeselected) //ALL Associative Report
            {
                objclsDALAssociates.objclsConnection = objClsConnection;
                return objclsDALAssociates.DisplayAssociativeReport(intType, strType, intTypeselected);
            }

          public DataTable DisplayALLCompanyHeaderReport()
            {
                objclsDALAssociates.objclsConnection = objClsConnection;
                return objclsDALAssociates.DisplayALLCompanyHeaderReport();
            }
 
    }
}
