﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLRptEmployeeProfile
    {
        public static DataTable GetEmployeeInformation(int intCompanyID, int intBranchID, int intDepartmentID, int intDesignationID, int intEmploymentTypeID, int intEmployeeID, bool intIncludeComp, int intLocation, int intWorkStatus, int intNationalityID, int intReligionID, string sDateOfJoining, string DateOfJoiningTo)
        {
            return clsDALRptEmolyeeProfile.GetEmployeeInformation(intCompanyID, intBranchID, intDepartmentID, intDesignationID, intEmploymentTypeID, intEmployeeID, intIncludeComp, intLocation, intWorkStatus, intNationalityID, intReligionID, sDateOfJoining, DateOfJoiningTo);
        }

        public static DataTable DisplaySingleEmployeeReport(int intEmployeeID)
        {
            return clsDALRptEmolyeeProfile.DisplaySingleEmployeeReport(intEmployeeID);
        }
    }
}
