﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace MyBooksERP
{
    class ClsBLLRptAging
    {
        ClsDALRptAging objClsDALRptAging;
        private DataLayer objClsConnection;
        public ClsBLLRptAging()
        {
            objClsConnection = new DataLayer();
            objClsDALRptAging = new ClsDALRptAging();
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            objClsDALRptAging.objclsConnection = objClsConnection;
            return objClsDALRptAging.FillCombos(sarFieldValues);
        }
        public DataTable DisplayCompanyHeaderReport(int CompanyID)
        {
            objClsDALRptAging.objclsConnection = objClsConnection;
            return objClsDALRptAging.DisplayCompanyHeaderReport(CompanyID);
        }

        public DataTable DisplayALLCompanyHeaderReport()
        {
            objClsDALRptAging.objclsConnection = objClsConnection;
            return objClsDALRptAging.DisplayALLCompanyHeaderReport();
        }
        public DataTable GetAgeingDetails(int CompanyID, DateTime ReportDate, int CustomerID)
        {
            objClsDALRptAging.objclsConnection = objClsConnection;
            return objClsDALRptAging.GetAgeingDetails(CompanyID, ReportDate, CustomerID);
        }
    }
}
