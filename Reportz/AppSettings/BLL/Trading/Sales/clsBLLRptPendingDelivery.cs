﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLRptPendingDelivery
    {

        clsDALRptPendingDelivery objclsDALRptPendingDelivery;
        private DataLayer objClsConnection;

        public clsBLLRptPendingDelivery()
        {
            objClsConnection = new DataLayer();
            objclsDALRptPendingDelivery = new clsDALRptPendingDelivery();
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            objclsDALRptPendingDelivery.MobjclsConnection = objClsConnection;
            return objclsDALRptPendingDelivery.FillCombos(sarFieldValues);
        }

        public DataTable GetCompanyHeader(int intCompanyID)
        {
            objclsDALRptPendingDelivery.MobjclsConnection = objClsConnection;
            return objclsDALRptPendingDelivery.GetCompanyHeader(intCompanyID);
        }

        public DataSet GetReport(int intCompanyID,int intVendorID,long lngItemIssueID,int intStatusID,DateTime dtFromdate,DateTime dtTodate,int intchkFrmTo)
        {
            objclsDALRptPendingDelivery.MobjclsConnection = objClsConnection;
            return objclsDALRptPendingDelivery.GetReport(intCompanyID,intVendorID,lngItemIssueID,intStatusID,dtFromdate,dtTodate,intchkFrmTo);
        }
    }
}
