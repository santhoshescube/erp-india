﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLRptItemwiseProfit
    {
        clsDALRptItemwiseProfit objDALRptItemwiseProfit;

        public clsBLLRptItemwiseProfit()
        {
            objDALRptItemwiseProfit = new clsDALRptItemwiseProfit();
        }

        public DataSet LoadCombos(int intCompanyID, int intCategoryID, int intSubCategoryID)
        {
            return objDALRptItemwiseProfit.LoadCombos(intCompanyID, intCategoryID, intSubCategoryID);
        }

        public DataSet GetReport(int intItemID, int intCompanyID, int intCategoryID, int intSubCategoryID,int intTypeID,DateTime dtFromDate,DateTime dtToDate,bool IsGroup)
        {
            return objDALRptItemwiseProfit.GetReport(intItemID, intCompanyID, intCategoryID, intSubCategoryID, intTypeID, dtFromDate, dtToDate, IsGroup);
        }

    }
}
