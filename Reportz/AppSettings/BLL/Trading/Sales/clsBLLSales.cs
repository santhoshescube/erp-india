﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,,2 May 2011>
Description:	<Description,,BLL for Sales>
================================================
*/
namespace MyBooksERP
{
    public class clsBLLSales
    {
        clsDALSales objclsDALSales;
            private DataLayer objClsConnection;

            public clsBLLSales()
            {

            objClsConnection = new DataLayer();
            objclsDALSales = new clsDALSales();

             }
            public DataTable FillCombos(string[] sarFieldValues)
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.FillCombos(sarFieldValues);
            }
            public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
            }
            public DataTable DisplaySalesQuotationReport(int intCompany, int intCustomer, string strFrom, string strTo, int intstatus, int intNo,int intExecutive,int chkFrom,int chkTo,int intDateType, string strPermittedCompany) //  SalesQuotation Report
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.DisplaySalesQuotationReport(intCompany, intCustomer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);
            }
            public DataTable DisplaySalesOrderReport(int intCompany, int intCustomer, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) //  SalesQuotation Report
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.DisplaySalesOrderReport(intCompany, intCustomer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo,intDateType, strPermittedCompany);
            }
            public DataTable DisplaySalesInvoiceReport(int intCompany, int intCustomer, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) //  SalesQuotation Report
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.DisplaySalesInvoiceReport(intCompany, intCustomer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo,intDateType, strPermittedCompany);
            }

            public DataTable DisplaySalesReturnReport(int intCompany, int intCustomer, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) //  SalesQuotation Report
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.DisplaySalesReturnReport(intCompany, intCustomer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);
            }

        //a

            public DataTable DisplayPOSReport(int intCompany, int intCustomer, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) //  POS MIS 
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.DisplayPOSReport(intCompany, intCustomer, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDateType, strPermittedCompany);
            }

            public DataSet DisplayPOS1(int intCompany, int intCustomer, string strFrom, string strTo, int Type, int intstatus, int intNo, int chkFrom, int chkTo, int intDateType, string strPermittedCompany)
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.DisplayPOS1(intCompany, intCustomer, strFrom, strTo, Type, intstatus, intNo, chkFrom, chkTo, intDateType, strPermittedCompany); 
            }


            public DataSet DisplaySalesQuotation(int intCompany, int intCustomer, string strFrom, string strTo, int Type, int intstatus, int intNo, int chkFrom, int chkTo, int intDateType, string strPermittedCompany) 
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.DisplaySalesQuotation(intCompany, intCustomer, strFrom, strTo, Type, intstatus, intNo, chkFrom, chkTo, intDateType, strPermittedCompany);

            }


            public DataSet DisplaySalesOrder(int intCompany, int intCustomer, string strFrom, string strTo, int Type, int intstatus, int intNo, int chkFrom, int chkTo, int intDateType, string strPermittedCompany)
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.DisplaySalesOrder(intCompany, intCustomer, strFrom, strTo, Type, intstatus, intNo, chkFrom, chkTo, intDateType, strPermittedCompany);

            }
            public DataSet DisplaySalesInvoice(int intCompany, int intCustomer, string strFrom, string strTo, int Type, int intstatus, int intNo, int SalesOrderid, int chkFrom, int chkTo, int intDateType, string strPermittedCompany)
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.DisplaySalesInvoice(intCompany, intCustomer, strFrom, strTo, Type, intstatus, intNo, SalesOrderid, chkFrom, chkTo, intDateType, strPermittedCompany);

            }

            public DataSet DisplaySalesReturn(int intCompany, int intCustomer, string strFrom, string strTo, int intstatus, int intNo, int chkFrom, int chkTo, int intDateType, int intExecutive, string strPermittedCompany)
            {
                objclsDALSales.objclsConnection = objClsConnection;
                return objclsDALSales.DisplaySalesReturn(intCompany, intCustomer, strFrom, strTo, intstatus, intNo, chkFrom, chkTo, intDateType, intExecutive,  strPermittedCompany);
            }

             public DataTable DisplayCompanyHeaderReport(int CompanyID)
             {

                objclsDALSales.objclsConnection = objClsConnection;
             return objclsDALSales.DisplayCompanyHeaderReport(CompanyID);
             }
         

             public DataTable DisplayALLCompanyHeaderReport()
             {
                 objclsDALSales.objclsConnection = objClsConnection;
                 return objclsDALSales.DisplayALLCompanyHeaderReport();
             }

             public DataTable DisplayExpenseOrderId(int intType)
             {
                 objclsDALSales.objclsConnection = objClsConnection;
                 return objclsDALSales.DisplayExpenseOrderId(intType);
             }

             public bool GenearlCustomerCheck()
             {
                 objclsDALSales.objclsConnection = objClsConnection;
                 return objclsDALSales.GenearlCustomerCheck();
             }
   }
}
