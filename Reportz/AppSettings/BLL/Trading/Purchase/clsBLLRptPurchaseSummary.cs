﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyBooksERP
{
    public class clsBLLRptPurchaseSummary
    {
        clsDALRptPurchaseSummary MobjDALRptPurchaseSummary;

        public clsBLLRptPurchaseSummary()
        {
            MobjDALRptPurchaseSummary = new clsDALRptPurchaseSummary();

        }
        public DataSet LoadCombo(int CompanyID)
        {
           return  MobjDALRptPurchaseSummary.LoadCombo(CompanyID);
        }

        public DataSet GetReport(int ItemID, int CompanyID, int VendorID,DateTime dtFromDate,DateTime dtToDate,int intFilterType)
        {
            return MobjDALRptPurchaseSummary.GetReport(ItemID,CompanyID,VendorID,dtFromDate,dtToDate,intFilterType);
        }
    }
}
