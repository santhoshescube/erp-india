﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;

namespace MyBooksERP
{
    public class clsBLLDirectGRNReport
    {
        private DataLayer MobjDataLayer;
        private clsDALDirectGRNReport MobjClsDALDirectGRN;

        public clsDTOGRNReport PobjclsDTOGRN { get; set; }
        public clsBLLDirectGRNReport()
        {
            MobjDataLayer = new DataLayer();
            MobjClsDALDirectGRN = new clsDALDirectGRNReport(MobjDataLayer);
            PobjclsDTOGRN = new clsDTOGRNReport();
            MobjClsDALDirectGRN.PobjClsDTOGRN = PobjclsDTOGRN;
        }

        public DataSet FillCombo(int intCompanyID, int intVendorID, long lngGRNID, int intStatusID)
        {
            return MobjClsDALDirectGRN.FillCombo(intCompanyID, intVendorID, lngGRNID, intStatusID);
        }

        public DataSet GetReport(int intCompanyID, int intVendorID, long lngGRNID, int intStatusID, DateTime? dtFrmDate, DateTime? dtToDate, int intChkFrmTo)
        {
            return MobjClsDALDirectGRN.GetReport(intCompanyID, intVendorID, lngGRNID, intStatusID, dtFrmDate, dtToDate, intChkFrmTo);
        }
    }
}