﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
/* 
=================================================
Author:		    <Author,,Sawmya>
Create date:    <Create Date,,2 May 2011>
Description:    <Description,,BLL for Sales>
Modified By:    <Author,,Amal>
Modified date:  <Modified Date,,18 Oct 2011>
Description:    <Description,,FINE TUNING OF REPORTS>
================================================
*/
namespace MyBooksERP
{
   public class clsBLLMISPurchaseReport
    {
       clsDALMISPurchaseReport objclsDALMISPurchase;
       private DataLayer objClsConnection;
       public clsBLLMISPurchaseReport()
       {
            objClsConnection = new DataLayer();
            objclsDALMISPurchase = new clsDALMISPurchaseReport();
       }
       public DataTable FillCombos(string[] sarFieldValues)
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.FillCombos(sarFieldValues);
       }
       public DataTable DisplayCompanyHeaderReport(int CompanyID)
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayCompanyHeaderReport(CompanyID);
       }
       public DataTable DisplayALLCompanyHeaderReport()
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayALLCompanyHeaderReport();
       }
       public DataTable GetCompanyByPermission(int intRoleID, int intCompanyID, int intControlID)
       {
           objclsDALMISPurchase.objclsConnection = objClsConnection;
           return objclsDALMISPurchase.GetCompanyByPermission(intRoleID, intCompanyID, intControlID);
       }
       public DataTable DisplayPurchaseindentReport(int intCompany, int intDepartment, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayPurchaseindentReport(intCompany, intDepartment, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate,  strPermittedCompany);
       }
       public DataTable DisplayPurchaseQutationReport(int intCompany, int intVendor, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayPurchaseQutationReport(intCompany, intVendor, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate, strPermittedCompany);
       }
       public DataTable DisplayRFQ(int intCompany, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany)
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayRFQ(intCompany, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate, strPermittedCompany);
       }
       public DataTable DisplayPurchaseOrderReport(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany)
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayPurchaseOrderReport(intCompany, intVendor, strFrom, strTo, intOrderType, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate, strPermittedCompany);
       }
       public DataTable DisplayGRNReport(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intWarehouse, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayGRNReport(intCompany, intVendor, strFrom, strTo, intOrderType, intWarehouse, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate, strPermittedCompany);
       }
       public DataTable DisplayPurchaseInvoiceReport(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intWarehouse, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayPurchaseInvoiceReport(intCompany, intVendor, strFrom, strTo, intOrderType, intWarehouse, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate,  strPermittedCompany);
       }

       public DataTable DisplayDebitNoteReport(int intCompany, int intVendor, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayDebitNoteReport(intCompany, intVendor, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate, strPermittedCompany);
       }
       public DataSet DisplayPurchaseindent(int intCompany, int intDepartment, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayPurchaseindent(intCompany, intDepartment, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate,  strPermittedCompany);
       }
       public DataSet DisplayPurchaseQutation(int intCompany, int intVendor, string strFrom, string strTo, int intType, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayPurchaseQutation(intCompany, intVendor, strFrom, strTo, intType, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate, strPermittedCompany);
       }
       public DataSet DisplayGRN(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intWarehouse, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayGRN(intCompany, intVendor, strFrom, strTo, intOrderType, intWarehouse, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate,  strPermittedCompany);
       }
       public DataSet DisplayPurchaseOrder(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intType, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayPurchaseOrder(intCompany, intVendor, strFrom, strTo, intOrderType, intType, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate,  strPermittedCompany);
       }
       public DataSet DisplayPurchaseInvoice(int intCompany, int intVendor, string strFrom, string strTo, int intOrderType, int intType, int intWarehouse, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
             objclsDALMISPurchase.objclsConnection = objClsConnection;
             return objclsDALMISPurchase.DisplayPurchaseInvoice(intCompany, intVendor, strFrom, strTo, intOrderType, intType, intWarehouse, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate, strPermittedCompany);
       }
       public DataSet DisplayRFQReport(int intCompany, string strFrom, string strTo, int intType, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
            objclsDALMISPurchase.objclsConnection = objClsConnection;
            return objclsDALMISPurchase.DisplayRFQReport(intCompany, strFrom, strTo, intType, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate,  strPermittedCompany);
       }
       public DataSet DisplayDebitNote(int intCompany, int intVendor, string strFrom, string strTo, int intstatus, int intNo, int intExecutive, int chkFrom, int chkTo, int intDate, string strPermittedCompany) 
       {
             objclsDALMISPurchase.objclsConnection = objClsConnection;
             return objclsDALMISPurchase.DisplayDebitNote(intCompany, intVendor, strFrom, strTo, intstatus, intNo, intExecutive, chkFrom, chkTo, intDate, strPermittedCompany);
       }
    }
}
