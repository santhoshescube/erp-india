﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,25 Feb 2011>
Description:	<Description,BLL for ReportViewer>
================================================
*/
namespace MyBooksERP
{
     public class clsBLLReportViewer:IDisposable
    {
         clsDTOReportviewer objclsDTOReportviewer;
         clsDALReportViewer objclsDALReportViewer;
         private DataLayer objclsconnection;

         public clsBLLReportViewer()
         {

             objclsconnection = new DataLayer();
             objclsDALReportViewer = new clsDALReportViewer();
             objclsDTOReportviewer = new clsDTOReportviewer();
             objclsDALReportViewer.objclsDTOReportviewer=objclsDTOReportviewer;
         }

         public clsDTOReportviewer clsDTOReportviewer
         {
             get {return objclsDTOReportviewer;}
             set { objclsDTOReportviewer = value; }   
         }

         public DataTable DisplayCompanyReport()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayCompanyReport();
         }

         public DataTable DisplayCompanyBankReport()   //company Bank Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayCompanyBankReport();
         }

         public DataTable DisplayBankReport()  //Bank Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayBankReport();
         }

         public DataSet DisplayEmployeeLeaveEntry() // Leave Entry
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayEmployeeLeaveEntry();
         }
        public DataSet DisplayCompanyReportHeader(int intCompanyID)  //Company  Header
        {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayCompanyReportHeader(intCompanyID);
        }
        public DataTable EmpSettlement() //
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.EmpSettlement();
        }
        public DataSet EmpGratuityReport() //
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.EmpGratuityReport();

        }

       public DataTable DisplayCompanyDefaultHeader()  //Company Default Header
       {
           objclsDALReportViewer.objclsconnection = objclsconnection;
           return objclsDALReportViewer.DisplayCompanyDefaultHeader();
       }
       public DataTable EmpSettlementDetails() //
       {
           objclsDALReportViewer.objclsconnection = objclsconnection;
           return objclsDALReportViewer.EmpSettlementDetails();
       }
       public DataTable DisplayLeaveExtension(int EmployeeID, int CompanyID)//Leave Extension
       {
           objclsDALReportViewer.objclsconnection = objclsconnection;
           return objclsDALReportViewer.DisplayLeaveExtension(EmployeeID, CompanyID);
       }
       public DataTable DisplayEmployeeReport()  //Employee  Report
       {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayEmployeeReport();
             
       }
         public DataTable DisplayVendorReport()  //Vendor  Report
          {
              objclsDALReportViewer.objclsconnection = objclsconnection;
              return objclsDALReportViewer.DisplayVendorReport();             

         }
         public DataSet DisplayDeductionPolicy() // Shift Policy
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayDeductionPolicy();
         }
         public DataTable DisplayVendorAddressReport() //VendorAddress  Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayVendorAddressReport();
            
         }
         public DataTable DisplayCustomerSalesReport() //Customer  Sales History
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayCustomerSalesReport();

         }
         public DataTable DisplayCustomerPaymentReport() //Customer  Payment History
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayCustomerPaymentReport();

         }
         public DataTable DisplayCustomerItemsReport() //Customer  Sales item  History
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayCustomerItemsReport();

         }
         public DataTable DisplaySupplierPurchaseReport() //Supplier Purchase  History
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplaySupplierPurchaseReport();

         }
         public DataTable DisplaySupplierPaymentReport() //Supplier Payment  History
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplaySupplierPaymentReport();

         }
         public DataTable DisplaySupplierItemsReport() //Supplier Items  History
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplaySupplierItemsReport();

         }
         public DataSet DisplayLeaveStructure(int intEmployeeID, int intCompanyID, string date1, string CurrentFinYear) // 
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayLeaveStructure(intEmployeeID, intCompanyID, date1, CurrentFinYear);
         }
         public DataTable DisplaySupplierNearestNeighbourReport() //Nearest NeighbourReport
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplaySupplierNearestNeighbourReport(); 
         }

         public DataTable DisplayWareHouseReport() //WareHouse Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayWareHouseReport();
            
         }

         public DataSet DisplayCompanySettingsReport() //  CompanySettings Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer. DisplayCompanySettingsReport();            
         }

         public DataSet DisplayExpenseReport()   //Delivery Expense Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayExpenseReport(); 
         }

         public DataTable DisplayWareHouseDetailsReport() //WareHouseDetails  Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayWareHouseDetailsReport();             
         }
         public DataSet DisplayProductReport() //Product Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayProductReport();
         }

         public DataSet DisplayExchangeCurrencyReport() //ExchangeCurrency Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayExchangeCurrencyReport();
         }

           //DisplayDeliveryNoteReport() 

         public DataSet DisplayDeliveryNoteReport()// Delivery Note Report         
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayDeliveryNoteReport();
         }
         public DataSet DisplayCustomerWiseDeliveryNoteReport()//Customerwise Delivery Note report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayCustomerWiseDeliveryNoteReport();
         }
         public DataSet DisplayMaterialIssueReport()// Material Issue Report         
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayMaterialIssueReport();
         }

         public DataSet DisplayMaterialReturnReport()// Material Return Report         
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayMaterialReturnReport();
         }


         public DataSet DisplayItemGroupReport()//Item Group Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayItemGroupReport();
         }

         public DataSet DisplayAlertSettingsReport()// AlertSettings Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayAlertSettingsReport();
         }

         public DataTable DisplayUOMReport() //UOM  Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayUOMReport();
         }

         public DataTable DisplayPaymentTermsReport() //UOM  Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayPaymentTermsReport();
         }

         public DataSet DisplayRoleSettingsReport() //Role Settings Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayRoleSettingsReport();
         }

         public DataTable DisplaySTDiscountTypeReferenceReport() //STDiscountTypeReference Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplaySTDiscountTypeReferenceReport();
             
         }

         public DataTable DisplaySTDiscountItemWiseDetailsReport() //STDiscountItemWiseDetails  Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplaySTDiscountItemWiseDetailsReport();
         }

         public DataTable DisplaySTDiscountCustomerWiseDetailsReport() //STDiscountCustomerWiseDetails  Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplaySTDiscountCustomerWiseDetailsReport();
            
         }

         public DataSet DisplayPaymentDetailsReport() //Payment Details Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayPaymentDetailsReport();
         }

         public DataSet DisplayPaymentDetailsReportForAlMajdal() //Payment Details Report AlMajadal
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayPaymentDetailsReportForAlMajdal();
         }

         public DataSet DisplayPurchaseDetailsReport(int intFormType)
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayPurchaseDetailsReport(intFormType);
         }
         public DataSet VacationProcessReport()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.VacationProcessReport();
         }
         public DataSet DisplayPILocationReport()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayPILocationReport();
         }
         public DataTable DisplaySalesInvoice1Report() //sales invoice detail report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplaySalesInvoice1Report();
         }
        
        
         public DataSet DisplaySalesDetailReport(int intFormType)
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplaySalesDetailReport(intFormType);
         }

         public DataSet DisplayOpeningStockReport() //Opening Stock Details Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayOpeningStockReport();
         }

         public DataSet DisplayAccountSettingsReport() //AccountSettings Details Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayAccountSettingsReport();
         }
         public DataSet DisplayRecurrentSetupReport() //RecurrentSetup Details Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayRecurrentSetupReport();
         }
   
         public DataSet DisplayRFQReport()// RFQ REport
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayRFQReport();
         }

         public DataSet DisplayUOMConversionReport(int intUomClassID, int intUomBaseID) //UOM  Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayUOMConversionReport(intUomClassID, intUomBaseID);
         }
         public DataSet DisplayStockAdjustmentReport() //Stock Adjustment Details Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayStockAdjustmentReport();
         }
         public DataTable DisplayTermsandconditions(int OperationType,int intCompanyID) //Terms and Conditions Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayTermsAndConditionReport(OperationType, intCompanyID);
         }

         public DataSet DisplayGeneralReceiptsAndPayements()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayGeneralReceiptsAndPayements();             
         }

         public DataSet DisplayGroupJV()// Display report for Group JV
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayGroupJV();  
         }

         public DataSet DisplayAccountsOpeningBalance()// Display AccoutsOpeninig Balance 
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayAccountsOpeningBalance();

         }
         public DataTable FillCombos(string[] saFieldValues)
         {
             // function for getting datatable for filling combo
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.FillCombos(saFieldValues);
         }

         public DataSet DisplayProjectReport()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayProjectReport();
         }

         public DataSet DisplayStockTransfer()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayStockTransfer();
         }

         public DataSet DisplayDebitNoteReport()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayDebitNoteReport();
         }

         public DataSet DisplayDebitNoteLocationReport()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayDebitNoteLocationReport();
         }

         public DataSet DisplayItemIssueLocationReport()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayItemIssueLocationReport();
         }
         public DataSet DisplayPricingSchemeReport()   //Delivery Expense Report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayPricingSchemeReport();
         }
         public DataSet DisplayGRNLocationReport()//Customerwise Delivery Note report
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayGRNLocationReport();
         }

         public DataSet DisplayPOSReport()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayPOSReport();
         }
         public DataSet DisplayLeavePolicy() // LEave Policy
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayLeavePolicy();
         }
         public DataSet DisplayShiftPolicy() // Shift Policy
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayShiftPolicy();
         }
         public DataSet DisplayWorkPolicy() // Shift Policy
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayWorkPolicy();
         }

         public DataSet DisplayOvertimePolicy() // 
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayOvertimePolicy();
         }
         public DataSet DisplayReceiptIssue() // Documentreceiptissue
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayReceiptIssue();
         }
         public DataSet DisplayAccRecurringJournalSetup()//AccRecurringJournalSetup
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayAccRecurringJournalSetup();
         }
         public DataSet DisplaySalaryStructure() // 
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplaySalaryStructure();
         }

         public DataSet DisplaySalaryRelease()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplaySalaryRelease();
         }

         public DataSet DisplayCompanyHeader()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayCompanyHeader();
         }

         public DataSet GetJobOrderDetails()
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.GetJobOrderDetails();
         }
         public DataSet PrintExtraCharges(long ExtraChargeID)
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.PrintExtraCharges(ExtraChargeID);
         }
         public DataSet DisplayExtraChargePayment(long ExtraChargePaymentID)
         {
             objclsDALReportViewer.objclsconnection = objclsconnection;
             return objclsDALReportViewer.DisplayExtraChargePayment(ExtraChargePaymentID);
         }
         #region IDisposable Members

         public void Dispose()
         {

             if (objclsDALReportViewer != null)
                 objclsDALReportViewer = null;

             if (objclsDTOReportviewer != null)
                 objclsDTOReportviewer = null;
         }

         #endregion

    }
}
