﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;

namespace MyBooksERP
{
    public class clsBLLPermissionSettingsReports
    {
        clsDALPermissionSettingsReport MobjClsDALPermissionSettings;
        
        public clsBLLPermissionSettingsReports()
        {
            DataLayer objDataLayer = new DataLayer();
            MobjClsDALPermissionSettings = new clsDALPermissionSettingsReport(objDataLayer);
        }
        public void GetPermissions(int intRoleID, int intCompanyID, int intModuleID, int intMenuID, out bool blnPrintEmailPermission, out bool blnCreatePermission, out bool blnUpdatePermission, out bool blnDeletePermission)
        {
            MobjClsDALPermissionSettings.GetPermissions(intRoleID, intCompanyID, intModuleID, intMenuID, out blnPrintEmailPermission, out blnCreatePermission, out blnUpdatePermission, out blnDeletePermission);
        }

        public DataTable GetMenuPermissions(int intRoleID, int intCompanyID)
        {
            return MobjClsDALPermissionSettings.GetMenuPermissions(intRoleID, intCompanyID);
        }
        public DataTable GetPermittedCompany(int intRoleID, int intCompanyID, int intControlID)
        {
            return MobjClsDALPermissionSettings.GetPermittedCompany(intRoleID, intCompanyID, intControlID);
        }
    }
}
